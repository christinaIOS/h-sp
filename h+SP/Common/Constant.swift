//
//  Constant.swift
//  NeuIndustries
//
//  Created by Balakrishnan on 07/07/16.
//  Copyright © 2016 Balakrishnan. All rights reserved.
//

import UIKit

var accet = false
//var docCategory = ""
//let strBaseURL = "http://trydemo.xyz/zero_delay/public/api/"
let strBaseURL = "https://www.zero-delay.com/api/"
let loginURL = strBaseURL + "login"
let registerURL = strBaseURL + "register"
let counsilStateURL = strBaseURL + "councilstates"
let otURL = strBaseURL + "otpverify"
let resendOtURL = strBaseURL + "regenerateotp"
let forgotURL = strBaseURL + "forgotpassword"
let profileURL = strBaseURL + "myprofile"
let degreeURL = strBaseURL + "degrees"
let specialityURL = strBaseURL + "speciality"
let profileUpdateURL = strBaseURL + "profileupdate"
let clinicListingURL = strBaseURL + "clinic"
let timingURL = strBaseURL + "timings"
let nurserTimingURL = strBaseURL + "nursetiming"
let absentURL = strBaseURL + "absentdate"
let blockSlotListURL = strBaseURL + "blockslot"
let blockSlotTimesURL = strBaseURL + "listslot"
let changepassURL = strBaseURL + "changepassword"
let feedURL = strBaseURL + "listblog"
let mailUs = strBaseURL + "sendenquiry"
let subUser = strBaseURL + "createsubuser"
let subUserListing = strBaseURL + "listsubuser"
let editSubUser = strBaseURL + "editsubuser/"
let dylTiming = strBaseURL + "providertiming"
let settings = strBaseURL + "settings"
let pharmacyTiming = strBaseURL + "pharmacytiming"
let banner = strBaseURL + "listads"
let liveStatus = strBaseURL + "livestatusupdate"
let adscount = strBaseURL + "adscount"
let bankName = strBaseURL + "banklist"

let consClinicListArr = [NSDictionary]()

class Constant: NSObject {
   
    static let viewActivity = UIView()
    static let activityView = UIActivityIndicatorView(style: .gray)
    
    static func showLoader(view : UIView) {
        viewActivity.frame = view.frame
        viewActivity.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        activityView.transform = CGAffineTransform(scaleX: 2, y: 2)
        activityView.center = view.center
        activityView.color = UIColor.white
        activityView.startAnimating()
        view.addSubview(viewActivity)
        view.addSubview(activityView)
    }
    
    static func hideLoader(view : UIView)  {
        activityView.removeFromSuperview()
        viewActivity.removeFromSuperview()
    }
     static func viewShadowSetup(view: UIView) {
            view.layer.borderWidth = 0.3
            view.layer.borderColor = UIColor.lightGray.cgColor
   
        }
        
    static func viewShadowSetup1(view: UIView) {
            view.layer.borderWidth = 1.0
            view.layer.borderColor = UIColor.clear.cgColor
            
            view.layer.masksToBounds = false
            view.layer.shadowColor = UIColor.lightGray.cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 3.0
        }
    
    static func addStatusBar(view : UIView){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 249/255, green: 232/255, blue: 0/255, alpha: 1.0)
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 249/255, green: 232/255, blue: 0/255, alpha: 1.0)
        }
    }
        
}


