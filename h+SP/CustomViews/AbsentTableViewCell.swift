//
//  AbsentTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 12/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class AbsentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var multiView: UIView!
    @IBOutlet weak var slotView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var centerLbl: UILabel!
    @IBOutlet weak var tolbl: UILabel!
    @IBOutlet weak var todatelbl: UILabel!
    @IBOutlet weak var fromDatelbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
     @IBOutlet weak var frmTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
