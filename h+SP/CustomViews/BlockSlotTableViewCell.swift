//
//  BlockSlotTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 21/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class BlockSlotTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var multiView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var frmlbl: UILabel!
    @IBOutlet weak var todatelbl: UILabel!
    @IBOutlet weak var tolbl: UILabel!
    @IBOutlet weak var morView: UIView!
    @IBOutlet weak var noonView: UIView!
    @IBOutlet weak var eveView: UIView!
    @IBOutlet weak var mlbl: UILabel!
    @IBOutlet weak var nlbl: UILabel!
    @IBOutlet weak var elbl: UILabel!
    @IBOutlet weak var fromdatelbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
