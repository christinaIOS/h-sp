//
//  ClinicListingHeaderTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 21/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class ClinicListingHeaderTableViewCell: UITableViewCell{

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var adrsLbl: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var morSlotView: UIView!
    @IBOutlet weak var noonSlotView: UIView!
    @IBOutlet weak var eveSlotView: UIView!
    @IBOutlet weak var vistingHourBtn: UIButton!

    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var slotView: UIView!

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!

     @IBOutlet weak var monBtn: UIButton!
     @IBOutlet weak var tueBtn: UIButton!
     @IBOutlet weak var wedBtn: UIButton!
     @IBOutlet weak var thuBtn: UIButton!
     @IBOutlet weak var friBtn: UIButton!
     @IBOutlet weak var satBtn: UIButton!
     @IBOutlet weak var sunBtn: UIButton!

    @IBOutlet weak var monImg: UIImageView!
    @IBOutlet weak var tueImg: UIImageView!
    @IBOutlet weak var wedImg: UIImageView!
    @IBOutlet weak var thuImg: UIImageView!
    @IBOutlet weak var friImg: UIImageView!
    @IBOutlet weak var satImg: UIImageView!
    @IBOutlet weak var sunImg: UIImageView!

    @IBOutlet weak var btnSubmit: UIButton!

    var monBool = false
    var tueBool = false
    var wedBool = false
    var thuBool = false
    var friBool = false
    var satBool = false
    var sunBool = false

    @IBOutlet weak var submitBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
