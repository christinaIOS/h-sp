//
//  subregisterTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 12/06/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit

class subregisterTableViewCell: UITableViewCell {

    @IBOutlet weak var namelbl : UILabel!
    @IBOutlet weak var mailLbl : UILabel!
    @IBOutlet weak var mobileLbl : UILabel!
    @IBOutlet weak var mySwitch : UISwitch!
    @IBOutlet weak var viewBg : UIView!
     @IBOutlet weak var viewEdit : UIView!
     @IBOutlet weak var btnedit : UIButton!
    @IBOutlet weak var btnSelect : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
