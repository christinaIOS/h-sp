//
//  timeSlotView.swift
//  h+SP
//
//  Created by mirrorminds on 20/02/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit

class timeSlotView: UIView {

    @IBOutlet weak var lbl: UILabel!
     @IBOutlet weak var imgView: UIImageView!
     @IBOutlet weak var btn: UIButton!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: (UIDevice.current.userInterfaceIdiom == .phone ? "timeSlotView" : "timeSlotView_ipad"), bundle: Bundle(for: timeSlotView.self)).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

    
}
