//
//  BlockSlotTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 21/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class BlockSlotTableViewCell_ipad: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var multiView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
