//
//  FeedTableViewCell.swift
//  h+SP
//
//  Created by mirrorminds on 21/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class FeedTableViewCell_ipad: UITableViewCell {
    
  @IBOutlet weak var lbl: UILabel!
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
