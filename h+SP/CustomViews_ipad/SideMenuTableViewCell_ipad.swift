//
//  SideMenuTableViewCell.swift
//  MydayliveryDriver
//
//  Created by mirrorminds on 25/02/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class SideMenuTableViewCell_ipad: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
