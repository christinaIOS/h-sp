//
//  AbsentViewController.swift
//  h+SP
//
//  Created by mirrorminds on 12/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import DatePickerDialog
import Alamofire
import AlamofireImage
import KVSpinnerView
import LoadingShimmer

class slotCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
     @IBOutlet weak var btnSelect: UIButton!
}

class AbsentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate  {
    
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var toastLbl: UILabel!
    @IBOutlet weak var singleImgView: UIImageView!
    @IBOutlet weak var multiImgView: UIImageView!
    @IBOutlet weak var fromDateTopCons: NSLayoutConstraint!
    @IBOutlet weak var centerHeightCons: NSLayoutConstraint!
    @IBOutlet weak var toDateView: UIView!
    @IBOutlet weak var toDateTf: UITextField!
    @IBOutlet weak var fromDateTf: UITextField!
    
    var fromView = "dash"
    var select = "SD"
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var MenuBtn: UIButton!
    var listArr = [NSDictionary]()
    var isEditAbsent = false
    var editID = 0
    var fromdate = Date()
    var toDate = Date()
    
    var datePicker : UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       viewSetu()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        AbsentAPICall()
      
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/1.5
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func viewSetu() {
        Constant.addStatusBar(view: self.view)
          centerView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60))
         heightSetu(selection: "single")
               MenuBtn.isHidden = false
                         backBtn.isHidden = true
                       gestur()
    }
    func gestur(){
                     let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     left.direction = .left
                     self.view.addGestureRecognizer(left)
             
                     let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     right.direction = .right
                     self.view.addGestureRecognizer(right)
                 }
             
             
                 @objc func Swipe(){
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
                 }
    func heightSetu(selection:String) {
        if selection == "SD" {
            singleImgView.image = UIImage(named: "filledCircle")
            multiImgView.image = UIImage(named: "circle")
            fromDateTf.placeholder = "Date"
            toDateView.isHidden = true
            centerHeightCons.constant = 20
            fromDateTopCons.constant = 20
        }else if selection == "MD" {
            multiImgView.image = UIImage(named: "filledCircle")
            singleImgView.image = UIImage(named: "circle")
            fromDateTf.placeholder = "From Date"
            toDateView.isHidden = false
            centerHeightCons.constant = 57
            fromDateTopCons.constant = 5
        }
    }
    
    // MARK: - tf delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = 12
         let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        let threeDaysFromNow =  Calendar.current.date(byAdding: .day, value: 3, to: Date())
        let fourDaysFromNow =  Calendar.current.date(byAdding: .day, value: 4, to: Date())
        DatePickerDialog().show("Date", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: textField == self.fromDateTf ? threeDaysFromNow : fourDaysFromNow ,maximumDate: threeMonthAgo,datePickerMode: .date) { (date) in
           //  DatePickerDialog().show("Date", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: textField == self.fromDateTf ? threeMonthAgo : threeDaysFromNow ,maximumDate: threeMonthAgo,datePickerMode: .date) { (date) in
         if let dt = date {
             let formatter = DateFormatter()
             formatter.dateFormat = "dd-MM-yyyy"
              if textField == self.fromDateTf {
                 self.fromDateTf.text = formatter.string(from: dt)
                self.fromdate = dt
              }else if textField == self.toDateTf {
                 self.toDateTf.text = formatter.string(from: dt)
                self.toDate = dt
              }
            
            }
          }
         return false
    }
    @IBAction func datePickerChanged(_ sender: Any) {
        let date = datePicker.date
        debugPrint(date)
    }
    //MARK:- date formatter
    func convertDate(dateStr:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"

       let date: Date? = dateFormatterGet.date(from: dateStr)
        print(dateFormatterPrint.string(from: date!))
        return dateFormatterPrint.string(from: date!)
        
    }
    
    // MARK: - btnActions
    @IBAction func menuAction(_ sender: UIButton) {
//        if sender.tag == 0 {
//
//        }else if sender.tag == 1 {
//            self.dismiss(animated: true, completion: nil)
//        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    @IBAction func addAbsentAction(_ sender: UIButton) {
        if select == "SD" {
            if fromDateTf.text != "" {
                if isEditAbsent == true {
                    editAbsentAPICall(id: editID, type: "SD")
                }else{
                    AddAbsentAPICall(type: select)
                }
            }else{
                self.view.makeToast("Please select your date", duration: 2.0, position: .center)
            }
        }else{
           if fromDateTf.text != "" {
            if toDateTf.text != "" {
                
                if fromdate.compare(toDate) == ComparisonResult.orderedDescending
                {
                    self.view.makeToast("Please check your todate", duration: 2.0, position: .center)
                } else if fromdate.compare(toDate) == ComparisonResult.orderedAscending
                {
                    print("date1 before date2")
                    if isEditAbsent == true {
                        editAbsentAPICall(id: editID, type: "MD")
                    }else{
                        AddAbsentAPICall(type: select)
                    }
                } else
                {
                    self.view.makeToast("Fromdate and todate are same", duration: 2.0, position: .center)
                }
                
            }else{
                self.view.makeToast("Please select your to date", duration: 2.0, position: .center)
            }
            }else{
                self.view.makeToast("Please select your from date", duration: 2.0, position: .center)
            }
        }
    }
    
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    
    @IBAction func hideBtnAction(_ sender: Any) {
        hideView.isHidden = true
    }
    
    @IBAction func selectAction(_ sender: UIButton) {
        if sender.tag == 0 {
           heightSetu(selection: "SD")
            select = "SD"
        }else if sender.tag == 1 {
            heightSetu(selection: "MD")
             select = "MD"
        }
    }
     @IBAction func AddAction(_ sender: UIButton) {
        fromDateTf.text = ""
        toDateTf.text = ""
        hideView.isHidden = false
    }
    
    
   // MARK: - tblView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            return listArr.count
        }else {
            return 7
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblView {
            return (UIDevice.current.userInterfaceIdiom == .phone ? 100 : 150)
        }else{
             return 35
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            var  cell: AbsentTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "AbsentTableViewCell") as? AbsentTableViewCell)
        let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "AbsentTableViewCell" : "AbsentTableViewCell_ipad" ), owner: self, options: nil)!
        
                   cell = (nib[0]  as? AbsentTableViewCell)!
                   cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                   cell?.bgView.layer.shadowOpacity = 1
                   cell?.bgView.layer.shadowOffset = CGSize.zero
                   cell?.bgView.layer.shadowRadius = 2
                   cell?.bgView.layer.cornerRadius = 5
        
        if listArr[indexPath.row].value(forKey: "adtype") as! String == "SD" {
            cell?.centerLbl.isHidden = true
            cell?.todatelbl.isHidden = true
            cell?.tolbl.isHidden = true
            cell?.lbl.text = "SINGLE"
            cell?.frmTitle.text = "Leave date"
        }else{
            cell?.centerLbl.isHidden = false
            cell?.todatelbl.isHidden = false
            cell?.tolbl.isHidden = false
            cell?.lbl.text = "MULTIPLE"
            cell?.frmTitle.text = "From"
        }
        
        if listArr[indexPath.row].value(forKey: "created") as? String != nil {
            cell?.datelbl.text = listArr[indexPath.row].value(forKey: "created") as? String
        }
        if listArr[indexPath.row].value(forKey: "fromdate") as? String != nil {
            cell?.fromDatelbl.text = listArr[indexPath.row].value(forKey: "fromdate") as? String
        }
        if listArr[indexPath.row].value(forKey: "todate") as? String != nil {
            cell?.todatelbl.text = listArr[indexPath.row].value(forKey: "todate") as? String
        }
        
        cell?.editBtn.tag = indexPath.row
            cell?.editBtn.addTarget(self, action: #selector(editAction(sender:)), for: .touchUpInside)
            cell?.deleteBtn.tag = indexPath.row
            cell?.deleteBtn.addTarget(self, action: #selector(deleteAction(sender:)), for: .touchUpInside)
                   return cell!
        
    }
    
    @objc func editAction(sender:UIButton){
        isEditAbsent = true
        editID = listArr[sender.tag].value(forKey: "adid") as! Int
        if listArr[sender.tag].value(forKey: "adtype") as! String == "SD" {
            heightSetu(selection: "SD")
               if listArr[sender.tag].value(forKey: "fromdate") as? String != nil {
                  fromDateTf.text = listArr[sender.tag].value(forKey: "fromdate") as? String
                }
               }else{
                 heightSetu(selection: "MD")
                  if listArr[sender.tag].value(forKey: "fromdate") as? String != nil {
                     fromDateTf.text = listArr[sender.tag].value(forKey: "fromdate") as? String
                   }
                  if listArr[sender.tag].value(forKey: "todate") as? String != nil {
                    toDateTf.text = listArr[sender.tag].value(forKey: "todate") as? String
                  }
               }
               
        hideView.isHidden = false
      
    }
    @objc func deleteAction(sender:UIButton){
        // Create the alert controller
          let alertController = UIAlertController(title: "", message: "Are you sure want to delete?", preferredStyle: .alert)

                 // Create the actions
          let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                     UIAlertAction in
                     NSLog("OK Pressed")
            self.deleteAbsentAPICall(id: self.listArr[sender.tag].value(forKey: "adid") as! Int)
                 }
          let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                     UIAlertAction in
                     NSLog("Cancel Pressed")
                 }

                 // Add the actions
                 alertController.addAction(okAction)
                 alertController.addAction(cancelAction)

                 // Present the controller
          self.present(alertController, animated: true, completion: nil)
    }
    //MARK:- Ai call
    func AbsentAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = absentURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.listArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                                if self.listArr.count == 0 {
                                    self.toastLbl.isHidden = false
                                }else{
                                   self.toastLbl.isHidden = true
                                }
                                self.tblView.reloadData()
                            }else{
                                 if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                                  self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                    UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                      self.logout()
                                                                  })
                                                              }else {
                                                                   self.toastLbl.isHidden = false
                                                              }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.toastLbl.isHidden = false
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func AddAbsentAPICall(type:String) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = absentURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        print(strURL)
        
        let params = ["adtype":"\(type)","fromdate":"\(convertDate(dateStr: fromDateTf.text!))","todate": toDateTf.text != "" ? "\(convertDate(dateStr: toDateTf.text!))" : "","fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.hideView.isHidden = true
                                self.AbsentAPICall()
                           }else{
                               
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                   self.hideView.isHidden = true
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                           }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func deleteAbsentAPICall(id:Int) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = absentURL + "/\(id)"
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.delete, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                              self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                              self.AbsentAPICall()
                            }else{
                               if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func editAbsentAPICall(id:Int,type:String) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = absentURL + "/\(id)"
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        print(strURL)
        let params = ["adtype":"\(type)","fromdate":"\(fromDateTf.text!)","todate":"\(toDateTf.text!)"]
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.hideView.isHidden = true
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                 self.AbsentAPICall()
                            }else{
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        self.logout()
                                    })
                                }else {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }

}
