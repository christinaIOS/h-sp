//
//  AppDelegate.swift
//  h+SP
//
//  Created by mirrorminds on 09/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//


import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import FirebaseMessaging
import Alamofire
import FirebaseAnalytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.setValue(0, forKey: "adCount")
        var storyboard: UIStoryboard
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
               // It's an iPhone
            storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.window!.rootViewController = storyboard.instantiateInitialViewController() as! InitialViewController
            break
         case .pad:
               // It's an iPad
            storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            self.window!.rootViewController = storyboard.instantiateInitialViewController()
                as! InitialViewController
            break
         case .unspecified: break
               // Uh, oh! What could it be?
         case .tv: break
            // It's an iPhone
         case .carPlay: break
           // It's an iPhone
         @unknown default: break
            // It's an iPhone
        }
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }

        GMSServices.provideAPIKey("AIzaSyA7npFt2iCMFw4fACs-_8ifD5GKVdQF96E")
        GMSPlacesClient.provideAPIKey("AIzaSyA7npFt2iCMFw4fACs-_8ifD5GKVdQF96E")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(_:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
            
                    registerForPushNotification(application)
                    Messaging.messaging().delegate = self
                    FirebaseApp.configure()
        
        return true
    }
    
    
    
    func registerForPushNotification(_ application: UIApplication)
    {

        if #available(iOS 10.0, *) {

            let id = "1"
            let catid = "newCategory"
            let likeAction = UNNotificationAction(identifier: "Like" + id,
                                                  title: "Like" + id, options: [.foreground])
            let newCategory = UNNotificationCategory(identifier: "newCategory",
                                                     actions: [likeAction],
                                                     intentIdentifiers: [catid], options: [])
            let center = UNUserNotificationCenter.current()

            center.setNotificationCategories([newCategory])
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            //Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0


    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
       {
           print("device token %@",deviceToken)

           var deviceTokenString = String(format: "%@", deviceToken as CVarArg)
           deviceTokenString = deviceTokenString.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
           deviceTokenString = deviceTokenString.replacingOccurrences(of: " ", with: "")

           Messaging.messaging().setAPNSToken(deviceToken, type:MessagingAPNSTokenType.sandbox)
           Messaging.messaging().setAPNSToken(deviceToken, type:MessagingAPNSTokenType.prod)
       }


       func application(_ application: UIApplication,
                        didFailToRegisterForRemoteNotificationsWithError error: Error) {
           print("Failed to register: \(error)")
       }

       func application(received remoteMessage: MessagingRemoteMessage) {
           print(remoteMessage.appData)

       }

       @available(iOS 10.0, *)
       func userNotificationCenter(_ center: UNUserNotificationCenter,
                                   willPresent notification: UNNotification,
                                   withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           let userInfo = notification.request.content.userInfo
           // Print message ID.
          // print(userInfo)
           if let messageID = userInfo[gcmMessageIDKey] {
               print("Message ID: \(messageID)")

           }
          // print(userInfo)

           let state = UIApplication.shared.applicationState
           if state == .active {
               print("App in active")
           }

           // Change this to your preferred presentation option
           completionHandler([.alert, .badge, .sound])
       }
    
      @objc func tokenRefreshNotification(_ notification: NSNotification) {
           InstanceID.instanceID().instanceID { (result, error) in
                    if let error = error {
                        print("Error fetching remote instange ID: \(error)")
                    } else if let result = result {
                        print("Remote instance ID token: \(result.token)")
                    }
                    print("InstanceID token: \(result!.token)")
                    UserDefaults.standard.setValue("\(result!.token)", forKey: "FCMTOKEN")
                    UserDefaults.standard.setValue("", forKey: "REGISTERDFCMTOKEN")
                }
                connectToFcm()
            }


        func connectToFcm() {
            
            Messaging.messaging().isAutoInitEnabled = true
            // Won't connect since there is no token
    
        }
        // [START refresh_token]

        //MARK: FCM Token Refreshed
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            // FCM token updated, update it on Backend Server
            print("Firebase registration token: \(fcmToken)")

            UserDefaults.standard.setValue(fcmToken, forKey: "FCMTOKEN")
        }

        @available(iOS 10.0, *)
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            print("remoteMessage: \(remoteMessage)")
            // Convert to pretty-print JSON, just to show the message for testing
            guard let data = try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
                let prettyPrinted = String(data: data, encoding: .utf8) else {
                    return

            }
            print("Received direct channel message rm :\n\(prettyPrinted)")

            let dict = remoteMessage.appData[AnyHashable("notification")] as! NSDictionary
            let id = ""

            let body = dict.value(forKey: "body") as! String
            let title = dict.value(forKey: "title") as! String
            let images = ""
            //  let type = userInfo[AnyHashable("gcm.notification.type")] as? String

            //creating the notification content
            let content = UNMutableNotificationContent()

            //adding title, subtitle, body and badge
            content.title = title
            //        content.subtitle = ""
            content.body = body
            // content.badge =  3
            //   content.sound = UNNotificationSound(named: "falling.mp3")
            if images != nil && images != "" {
                let imagePush:UIImageView = UIImageView(frame: CGRect(x:0, y:0, width:50, height:50))
            

                imagePush.af_setImage(
                    withURL: URL(string: ((images.replacingOccurrences(of: "\"" , with: ""))))!,
                    placeholderImage: nil,
                    filter: nil,
                    imageTransition: .crossDissolve(0.5),
                    completion: { response in
                        print(response.value!)
                        print(response.error as Any)


                        let strFilename = id + ".png"
                        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFilename)

                        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                        let fileURL = documentsDirectoryURL.appendingPathComponent(strFilename)

                        if !FileManager.default.fileExists(atPath: fileURL.path) {
                            do {
                                try (imagePush.image)?.pngData()!.write(to: fileURL)
                                print("Image Added Successfully")
                                let fileURL = URL(fileURLWithPath: fileURL.path )
                                let attachement = try? UNNotificationAttachment.init(identifier: "newCategory" , url: fileURL, options: nil)
                                content.attachments = [attachement!]
                            } catch {
                                print(error)
                            }
                        }
                        let identifier = ProcessInfo.processInfo.globallyUniqueString
                        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.3, repeats: false)
                        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
                        UNUserNotificationCenter.current().add(request) { (error) in
                        }
                }
                )

            }
            else {
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
                let request = UNNotificationRequest(identifier: id , content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }


        }


        func getDirectoryPath() -> String {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            return documentsDirectory
        }

        func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
            // Print notification payload data
            print("Push notification received: \(data)")
               guard
           
                let body = data["body"] as? String,
                let title = data["title"] as? String

                else {
                    // handle any error here
                    return
            }

            print("Title: \(title) \nBody:\(body)")
        }


        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            print("didReceiveRemoteNotification (withCompletionHandeler)")

            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            guard let data = try? JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted),
                let prettyPrinted = String(data: data, encoding: .utf8) else {
                    return

            }
            print("Received direct channel message rn :\n\(prettyPrinted)")
            print(userInfo)
            completionHandler(UIBackgroundFetchResult.newData)
        }

        @available(iOS 10.0, *)
        internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
          //  UserDefaults.standard.setValue("Called", forKey: "Notification")

            print("Userinfo \(response.notification.request.content.userInfo)")

            let userInfo = (response.notification.request.content.userInfo) as NSDictionary

            let images = userInfo[AnyHashable("gcm.notification.picture")] as? String
            let id = userInfo[AnyHashable("gcm.notification.id")] as? String
            let aps = userInfo[AnyHashable("aps")] as? NSDictionary

            let alert = aps!["alert"] as? NSDictionary
            let body = ""//alert!["body"] as? String
            let title = alert!["title"] as? String
            let content = UNMutableNotificationContent()
       content.title = title!
            //        content.subtitle = ""
            content.body = body//!
            if images != nil && images != "" {
                let imagePush:UIImageView = UIImageView(frame: CGRect(x:0, y:0, width:50, height:50))
             

                imagePush.af_setImage(
                    withURL: URL(string: ((images?.replacingOccurrences(of: "\"" , with: ""))!))!,
                    placeholderImage: nil,
                    filter: nil,
                    imageTransition: .crossDissolve(0.5),
                    completion: { response in
                        print(response.value!)
                        print(response.error as Any)


                        let strFilename = id! + ".png"
                        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFilename)

                        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                        let fileURL = documentsDirectoryURL.appendingPathComponent(strFilename)

                        if !FileManager.default.fileExists(atPath: fileURL.path) {
                            do {
                                try (imagePush.image)?.pngData()!.write(to: fileURL)
                                print("Image Added Successfully")
                                let fileURL = URL(fileURLWithPath: fileURL.path )
                                let attachement = try? UNNotificationAttachment.init(identifier: "newCategory" , url: fileURL, options: nil)
                                content.attachments = [attachement!]
                            } catch {
                                print(error)
                            }
                        }
                        let identifier = ProcessInfo.processInfo.globallyUniqueString
                        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.3, repeats: false)
                        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
                        UNUserNotificationCenter.current().add(request) { (error) in
                            // handle error
                        }
                }
                )

            }
            else {
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
                let request = UNNotificationRequest(identifier: id! , content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
            if userInfo.value(forKey: "aps") as? NSDictionary != nil {
                let aps = userInfo.value(forKey: "aps") as! NSDictionary
                let alert = aps.value(forKey: "alert") as! NSDictionary

                if alert.value(forKey: "body") as? String != nil {
                    let body = alert.value(forKey: "body") as! String
                    UserDefaults.standard.setValue(body, forKey: "body")
                    print( UserDefaults.standard.value(forKey: "body") as! String )
                }else{

                }

                if alert.value(forKey: "title") as? String != nil {
                    let title = alert.value(forKey: "title") as! String

                    UserDefaults.standard.setValue(title, forKey: "title")
                    print( UserDefaults.standard.value(forKey: "title") as! String )
                }else{

                }

            }

                if userInfo.value(forKey: "gcm.notification.type") as? String != nil {
                    if userInfo.value(forKey: "gcm.notification.type") as! String == "Chat" {
                         UserDefaults.standard.setValue("chat", forKey: "type")
                         UserDefaults.standard.setValue("Called", forKey: "Notification")
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Trigger"), object: nil)
                    }else {

                    }
                }else if userInfo.value(forKey: "gcm.notification.type") as? String == nil {
                    UserDefaults.standard.setValue("noti", forKey: "type")
                    UserDefaults.standard.setValue("Called", forKey: "Notification")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TriggerNoti"), object: nil)
            }


            completionHandler()
        }
    
      func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    //        // fetch data from internet now
    //        guard let data = fetchSomeData() else {
    //            // data download failed
    //            completionHandler(.failed)
    //            return
    //        }
    //
    //        if data.isNew {
    //            // data download succeeded and is new
    //            completionHandler(.newData)
    //        } else {
    //            // data downloaded succeeded and is not new
    //            completionHandler(.noData)
    //        }
        }
        
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
         Messaging.messaging().shouldEstablishDirectChannel = false
                  connectToFcm()
         //       Messaging.messaging().disconnect()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
      //  self.saveContext()
    }

//    // MARK: - Core Data stack
//
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//        */
//        let container = NSPersistentContainer(name: "h_SP")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }

}

public extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
