//
//  AppointmentsViewController.swift
//  h+SP
//
//  Created by mirrorminds on 12/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import KVSpinnerView

class AppointmentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var upcomBtn: UIButton!
    @IBOutlet weak var completedBtn: UIButton!
    @IBOutlet weak var canceBtn: UIButton!
    @IBOutlet weak var upcomLbl: UILabel!
    @IBOutlet weak var completedLbl: UILabel!
    @IBOutlet weak var cancelbl: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var MenuBtn: UIButton!
    @IBOutlet weak var errLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    var fromView = ""
    var listArr = [NSDictionary]()
    var dateArr = [String]()
    var intSelectedSec:Int = 0
    var selectedMenu = "HOME"
    var arrayMenu = ["HOME","TOUR","AGENT","DS","sd"]
    var arraySubMenu = [["1", "1", "Follow Up"], ["2",  "Follow Up", "Operation"], ["3", "3", "Follow Up"], ["Quotation", "Map", "Follow Up"],["s","s","xs"]] as [NSArray]
    var docCategory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetu()
       setDate()
       let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

            tap.cancelsTouchesInView = false
        
       docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
       
        if docCategory == "DOC" || docCategory == "DEN" || docCategory == "DYL" || docCategory == "PHY" {
            errLbl.text = "No Appointments Found"
            titleLbl.text = "Appointments"
        }else if docCategory == "PHA" {
            errLbl.text = "No Orders Found"
            titleLbl.text = "Orders"
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func setDate(){

      let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
      //  var days = [Int]()
       for i in 1 ... 5 {
            let day = cal.component(.day, from: date)
            //days.append(String(day))
            let formatter = DateFormatter.init()
            formatter.dateFormat = "dd-MMM-yyyy"
            // let date1 = Date.init() //or any date
            let dayName = formatter.string(from: date)
        
            date = cal.date(byAdding: .day, value: 1, to: date)!
        dateArr.append("\(date)".components(separatedBy: " ").first!)
        }
      //  print(dateArr)
        
        for i in 0..<dateArr.count {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"

            let date: NSDate? = dateFormatterGet.date(from: dateArr[i]) as NSDate?
            print(dateFormatterPrint.string(from: date! as Date))
            dateArr[i] = "  " + dateFormatterPrint.string(from: date! as Date)
        }
        print(dateArr)
        dateArr[0] = "  Today"
        dateArr[1] = "  Tomorrow"
    }
    
    func viewSetu(){
        Constant.addStatusBar(view: self.view)
          bottomView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60))
         confirmBtn.layer.shadowColor = UIColor.lightGray.cgColor
                confirmBtn.layer.shadowOpacity = 1
                confirmBtn.layer.shadowOffset = CGSize(width: 0, height: 3.0)
                confirmBtn.layer.shadowRadius = 3
        confirmBtn.layer.cornerRadius = 15
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
                     NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        if fromView == "dash" {
            MenuBtn.isHidden = true
            backBtn.isHidden = false
            
        }else {
            MenuBtn.isHidden = false
           backBtn.isHidden = true
            gestur()
        }
        
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
               KVSpinnerView.settings.statusTextColor = UIColor.darkGray
               KVSpinnerView.settings.tintColor = UIColor.darkGray
               
        
    }
    func gestur(){
                     let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     left.direction = .left
                     self.view.addGestureRecognizer(left)
             
                     let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     right.direction = .right
                     self.view.addGestureRecognizer(right)
                 }
             
             
                 @objc func Swipe(){
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
                 }
    @objc func keyboardWillShow(notification: NSNotification) {
          if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
              if self.view.frame.origin.y == 0{
                  self.view.frame.origin.y -= keyboardSize.height/1.5
              }
          }
      }
      
      @objc func keyboardWillHide(notification: NSNotification) {
          if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
              if self.view.frame.origin.y != 0{
                  self.view.frame.origin.y = 0
              }
          }
      }
    
    // MARK: - btnActions
    @IBAction func menuAction(_ sender: UIButton) {
        if sender.tag == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }else if sender.tag == 1 {
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    @IBAction func tabAction(_ sender: UIButton) {
        if sender.tag == 0 {
            upcomBtn.setTitleColor(UIColor.black, for: .normal)
             completedBtn.setTitleColor(UIColor.darkGray, for: .normal)
             canceBtn.setTitleColor(UIColor.darkGray, for: .normal)
            upcomLbl.isHidden = false
             completedLbl.isHidden = true
             cancelbl.isHidden = true
        }else if sender.tag == 1 {
            completedBtn.setTitleColor(UIColor.black, for: .normal)
                        upcomBtn.setTitleColor(UIColor.darkGray, for: .normal)
                        canceBtn.setTitleColor(UIColor.darkGray, for: .normal)
                       completedLbl.isHidden = false
                        upcomLbl.isHidden = true
                        cancelbl.isHidden = true
        }else if sender.tag == 2 {
            canceBtn.setTitleColor(UIColor.black, for: .normal)
                        completedBtn.setTitleColor(UIColor.darkGray, for: .normal)
                        upcomBtn.setTitleColor(UIColor.darkGray, for: .normal)
                       cancelbl.isHidden = false
                        upcomLbl.isHidden = true
                        completedLbl.isHidden = true
        }
    }
    @IBAction func addAction(_ sender: Any) {
        hideView.isHidden = false
    }
    @IBAction func hideAction(_ sender: Any) {
          hideView.isHidden = true
    }
   @IBAction func confirmAction(_ sender: Any) {
             hideView.isHidden = true
         }
    // MARK: - tblView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
           return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if intSelectedSec == section {
                   let arraySub = arraySubMenu[intSelectedSec]
                   return arraySub.count
               }
               return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if UIDevice.current.userInterfaceIdiom == .pad {
                 return 220
           }else {
                 return 190
           }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var  cell: appointmentHeaderTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "appointmentHeaderTableViewCell") as? appointmentHeaderTableViewCell)
           let nib: [Any] = Bundle.main.loadNibNamed("appointmentHeaderTableViewCell", owner: self, options: nil)!
           cell = (nib[0]  as? appointmentHeaderTableViewCell)!
           cell?.lbl.text = dateArr[section]
        cell!.btn.tag = section
        if intSelectedSec == section {
                   cell?.imgView.image = UIImage(named: "dd1")
               }
               else {
                   cell?.imgView.image = UIImage(named: "dd")
               }
         cell!.btn.addTarget(self, action: #selector(headerAction), for: .touchUpInside)
           return cell!
    }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if UIDevice.current.userInterfaceIdiom == .pad {
                       var  cell: AppointmentTableViewCell_ipad? = (tableView.dequeueReusableCell(withIdentifier: "AppointmentTableViewCell_ipad") as? AppointmentTableViewCell_ipad)
                           let nib: [Any] = Bundle.main.loadNibNamed("AppointmentTableViewCell_ipad", owner: self, options: nil)!
                           cell = (nib[0]  as? AppointmentTableViewCell_ipad)!
                           cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                           cell?.bgView.layer.shadowOpacity = 1
                           cell?.bgView.layer.shadowOffset = CGSize.zero
                           cell?.bgView.layer.shadowRadius = 2
                           cell?.bgView.layer.cornerRadius = 0
                        cell?.visitBtn.tag = indexPath.row
                        cell?.cancelBtn.tag = indexPath.row
                        cell?.visitBtn.addTarget(self, action: #selector(OTP(Sender:)), for: .touchUpInside)
                        cell?.cancelBtn.addTarget(self, action: #selector(cancelAction(Sender:)), for: .touchUpInside)
                           return cell!
                  }else {
                       var  cell: AppointmentTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "AppointmentTableViewCell") as? AppointmentTableViewCell)
                           let nib: [Any] = Bundle.main.loadNibNamed("AppointmentTableViewCell", owner: self, options: nil)!
                           cell = (nib[0]  as? AppointmentTableViewCell)!
                           cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                           cell?.bgView.layer.shadowOpacity = 1
                           cell?.bgView.layer.shadowOffset = CGSize.zero
                           cell?.bgView.layer.shadowRadius = 2
                           cell?.bgView.layer.cornerRadius = 0
                        cell?.visitBtn.tag = indexPath.row
                        cell?.cancelBtn.tag = indexPath.row
                        cell?.visitBtn.addTarget(self, action: #selector(OTP(Sender:)), for: .touchUpInside)
                        cell?.cancelBtn.addTarget(self, action: #selector(cancelAction(Sender:)), for: .touchUpInside)
                           return cell!
                  }
           
       }
    @objc func OTP(Sender:UIButton){
       // hideView.isHidden = false
    }
    @objc func headerAction(sender:UIButton){
        if arrayMenu[sender.tag] == "" {
            selectedMenu = arrayMenu[sender.tag]
        }
        else {
            intSelectedSec = ((intSelectedSec != sender.tag) ? sender.tag : arrayMenu.count)
            tblView.reloadData()
        }
    }
    @objc func cancelAction(Sender:UIButton){
             // Create the alert controller
        let alertController = UIAlertController(title: "", message: "Are you sure want to cancel this Appointment?", preferredStyle: .alert)

               // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                   UIAlertAction in
                   NSLog("OK Pressed")
               }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                   UIAlertAction in
                   NSLog("Cancel Pressed")
               }

               // Add the actions
               alertController.addAction(okAction)
               alertController.addAction(cancelAction)

               // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
       }
    //MARK:- textfield delegate
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          textField.resignFirstResponder()
          return true
      }
    

    
}

