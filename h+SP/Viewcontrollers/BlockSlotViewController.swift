//
//  BlockSlotViewController.swift
//  h+SP
//
//  Created by mirrorminds on 20/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import DatePickerDialog
import KVSpinnerView
import Alamofire
import AMShimmer
import LoadingShimmer

class BlockSlotViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var backBtn: UIButton!
       @IBOutlet weak var MenuBtn: UIButton!
    @IBOutlet weak var tblView: UITableView!
   
    
    //HIdeView
     @IBOutlet weak var hideHeightCon: NSLayoutConstraint!
     @IBOutlet weak var dateViewHeightCons: NSLayoutConstraint!
     @IBOutlet weak var slotViewHeightCons: NSLayoutConstraint!
     @IBOutlet weak var buutonTopHeightCons: NSLayoutConstraint!
     @IBOutlet weak var centerView: UIView!
     @IBOutlet weak var hideView: UIView!
     @IBOutlet weak var toDateTf: UITextField!
     @IBOutlet weak var fromDateTf: UITextField!
     @IBOutlet weak var mornTblView: UITableView!
     @IBOutlet weak var noonTblView: UITableView!
     @IBOutlet weak var eveTblView: UITableView!
     @IBOutlet weak var singleImgView: UIImageView!
     @IBOutlet weak var multiImgView: UIImageView!
     @IBOutlet weak var slotsImgView: UIImageView!
     @IBOutlet weak var slotView: UIView!
     @IBOutlet weak var toastLbl: UILabel!
    
     var fromView = ""
     var select = "SD"
    var listArray = [NSDictionary]()
    var timeSlotArray = [NSDictionary]()
    var morArr = [NSDictionary]()
    var noonArr = [NSDictionary]()
    var eveArr = [NSDictionary]()
    
    var morningIdArr = [Int]()
    var noonIdArr = [Int]()
    var eveIdArr = [Int]()
    
    var blockListIdArr = [Int]()
    
    var morListArr = [NSDictionary]()
    var noonListArr = [NSDictionary]()
    var eveListArr = [NSDictionary]()
    var fromdate = Date()
       var toDate = Date()
    var arrHeightval = 0
    var isEditBlockSlot = false
    var slotArr = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centerView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 60 : 80))
        Constant.addStatusBar(view: self.view)
        blockListAPICall()
    }
    
    func gestur(){
        let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
        left.direction = .left
        self.view.addGestureRecognizer(left)
        let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
        right.direction = .right
        self.view.addGestureRecognizer(right)
    }
             
    @objc func Swipe(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    
    // MARK: - btnActions
    
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    
    @IBAction func menuAction(_ sender: UIButton) {
       
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    
    @IBAction func hideBtnAction(_ sender: Any) {
        hideView.isHidden = true
    }
    
    @IBAction func AddAction(_ sender: UIButton) {
        arrHeightval = 0
        fromDateTf.text = ""
        toDateTf.text = ""
        blockSLOTSAPICall()
    }
    
    @IBAction func addBlockSlotAction(_ sender: UIButton) {
        
              morningIdArr.removeAll()
              noonIdArr.removeAll()
              eveIdArr.removeAll()
              
              for i in 0..<morArr.count {
                  let dict = morArr[i]
                  if dict.value(forKey: "isSelect") as! String == "1" {
                      morningIdArr.append(dict.value(forKey: "mtid") as! Int)
                  }
              }
              for i in 0..<noonArr.count {
                  let dict = noonArr[i]
                  if dict.value(forKey: "isSelect") as! String == "1" {
                      noonIdArr.append(dict.value(forKey: "mtid") as! Int)
                  }
              }
              for i in 0..<eveArr.count {
                  let dict = eveArr[i]
                  if dict.value(forKey: "isSelect") as! String == "1" {
                      eveIdArr.append(dict.value(forKey: "mtid") as! Int)
                  }
              }
              print(morningIdArr)
              print(noonIdArr)
              print(eveIdArr)
              
             blockListIdArr = morningIdArr + noonIdArr + eveIdArr
        
        
        if select == "SD" {
            if fromDateTf.text != "" {
                if isEditBlockSlot == true {
                   // editAbsentAPICall(id: editID, type: "SD")
                }else{
                    if blockListIdArr.count != 0 {
                        craeteBlockSLOTSAPICall(type: "SD")
                    }else{
                        self.view.makeToast("please select your slot to block", duration: 2.0, position: .center)
                    }
                }
            }else{
                self.view.makeToast("please select your date", duration: 2.0, position: .center)
            }
        }else{
           if fromDateTf.text != "" {
            if toDateTf.text != "" {
             
                    if blockListIdArr.count != 0 {
                          
                        if fromdate.compare(toDate) == ComparisonResult.orderedDescending
                                       {
                                           self.view.makeToast("Please check your todate", duration: 2.0, position: .center)
                                       } else if fromdate.compare(toDate) == ComparisonResult.orderedAscending
                                       {
                                           print("date1 before date2")
                                          craeteBlockSLOTSAPICall(type: "MD")
                                       } else
                                       {
                                           self.view.makeToast("Fromdate and todate are same", duration: 2.0, position: .center)
                                       }
                    }else{
                         self.view.makeToast("please select your slot to block", duration: 2.0, position: .center)
                    }
                
            }else{
                self.view.makeToast("please select your to date", duration: 2.0, position: .center)
            }
            }else{
                self.view.makeToast("please select your from date", duration: 2.0, position: .center)
            }
        }
        
    }
    
    @IBAction func selectAction(_ sender: UIButton) {
           if sender.tag == 0 {
              heightSetu(selection: "SD")
               select = "SD"
           }else if sender.tag == 1 {
               heightSetu(selection: "MD")
                select = "MD"
           }else if sender.tag == 2 {
               heightSetu(selection: "BS")
                select = "BS"
           }
       }
    
    func heightSetu(selection:String) {
           if selection == "SD" {
               singleImgView.image = UIImage(named: "filledCircle")
               multiImgView.image = UIImage(named: "circle")
               slotsImgView.image = UIImage(named: "circle")
               fromDateTf.placeholder = "Date"
               toDateTf.isHidden = true
               fromDateTf.placeholder = "From Date"
               toDateTf.isHidden = false
               slotView.isHidden = false
               dateViewHeightCons.constant = 40
              // slotViewHeightCons.constant = CGFloat(75 + arrHeightval)
               hideHeightCon.constant = 440//CGFloat(300 + arrHeightval)
              // buutonTopHeightCons.constant = CGFloat(105 + arrHeightval)
           }else if selection == "MD" {
               multiImgView.image = UIImage(named: "filledCircle")
               singleImgView.image = UIImage(named: "circle")
               slotsImgView.image = UIImage(named: "circle")
               fromDateTf.placeholder = "From Date"
               toDateTf.isHidden = false
               slotView.isHidden = false
               dateViewHeightCons.constant = 90
              // slotViewHeightCons.constant = CGFloat(75 + arrHeightval)
               hideHeightCon.constant = 490//CGFloat(350 + arrHeightval)
               //buutonTopHeightCons.constant = CGFloat(155 + arrHeightval)
           }else if selection == "BS" {
               slotsImgView.image = UIImage(named: "filledCircle")
               singleImgView.image = UIImage(named: "circle")
               multiImgView.image = UIImage(named: "circle")
               fromDateTf.placeholder = "From Date"
               toDateTf.isHidden = false
               slotView.isHidden = false
               slotViewHeightCons.constant = CGFloat(75 + arrHeightval)
               hideHeightCon.constant = CGFloat(350 + arrHeightval)
               buutonTopHeightCons.constant = CGFloat(155 + arrHeightval)
           }
       }
    
    //MARK:- date formatter
       func convertDate(dateStr:String) -> String {
           let dateFormatterGet = DateFormatter()
           dateFormatterGet.dateFormat = "dd-MM-yyyy"

           let dateFormatterPrint = DateFormatter()
           dateFormatterPrint.dateFormat = "yyyy-MM-dd"

          let date: Date? = dateFormatterGet.date(from: dateStr)
           print(dateFormatterPrint.string(from: date!))
           return dateFormatterPrint.string(from: date!)
           
       }
    
    // MARK: - tblView Delegates
       func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if tableView == tblView {
            return listArray.count
           }else if tableView == mornTblView {
                return self.morArr.count
           }else if tableView == noonTblView {
                return self.noonArr.count
           }else if tableView == eveTblView {
                return self.eveArr.count
           }
           return 0
       }
    
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          if tableView == tblView {
            self.slotArr = [NSDictionary]()
            if listArray[indexPath.row].value(forKey: "slot") as? [NSDictionary] != nil {
                self.slotArr = listArray[indexPath.row].value(forKey: "slot") as! [NSDictionary]
            }
            self.morListArr.removeAll()
                       self.noonListArr.removeAll()
                       self.eveListArr.removeAll()
                       
                        for i in 0..<slotArr.count {
                             let dict = slotArr[i]
                             if dict.value(forKey: "mttype") as! String == "M" {
                             self.morListArr.append(dict)
                         }else if dict.value(forKey: "mttype") as! String == "A" {
                            self.noonListArr.append(dict)
                         }else if dict.value(forKey: "mttype") as! String == "E" {
                              self.eveListArr.append(dict)
                         }
                        }
                       
                       print(self.morListArr.count)
                       print(self.noonListArr.count)
                       print(self.eveListArr.count)
             let maxArr = max(max(self.morListArr.count, self.noonListArr.count), self.eveListArr.count)
            return CGFloat((UIDevice.current.userInterfaceIdiom == .phone ? 120 : 160) + (maxArr * (UIDevice.current.userInterfaceIdiom == .phone ? 35 : 40)))
           }else if tableView == mornTblView || tableView == noonTblView || tableView == eveTblView {
                return (UIDevice.current.userInterfaceIdiom == .phone ? 35 : 40)
           }
        return 0
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
              if tableView == tblView {
                var  cell: BlockSlotTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "BlockSlotTableViewCell") as? BlockSlotTableViewCell)
                 let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "BlockSlotTableViewCell" : "BlockSlotTableViewCell_ipad" ), owner: self, options: nil)!
                cell = (nib[0]  as? BlockSlotTableViewCell)!
                          cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                          cell?.bgView.layer.shadowOpacity = 1
                          cell?.bgView.layer.shadowOffset = CGSize.zero
                          cell?.bgView.layer.shadowRadius = 2
                          cell?.bgView.layer.cornerRadius = 0
                       if indexPath.row == 0 {
                           cell?.multiView.isHidden = true
                       }else {
                            cell?.multiView.isHidden = false
                       }
                
                //inner cell
                self.slotArr = [NSDictionary]()
                if listArray[indexPath.row].value(forKey: "slot") as? [NSDictionary] != nil {
                   self.slotArr = listArray[indexPath.row].value(forKey: "slot") as! [NSDictionary]
                }
                
                               self.morListArr.removeAll()
                               self.noonListArr.removeAll()
                               self.eveListArr.removeAll()
                               
                                for i in 0..<slotArr.count {
                                     let dict = slotArr[i]
                                     if dict.value(forKey: "mttype") as! String == "M" {
                                     self.morListArr.append(dict)
                                 }else if dict.value(forKey: "mttype") as! String == "A" {
                                    self.noonListArr.append(dict)
                                 }else if dict.value(forKey: "mttype") as! String == "E" {
                                      self.eveListArr.append(dict)
                                 }
                                }
                               
                               print(self.morListArr.count)
                               print(self.noonListArr.count)
                               print(self.eveListArr.count)
                if morListArr.count == 0 {
                    cell?.mlbl.text = ""
                }
                if noonListArr.count == 0 {
                    cell?.nlbl.text = ""
                }
                if eveListArr.count == 0 {
                    cell?.elbl.text = ""
                }
                             
                               for i in 0..<morListArr.count {
                                  let morSlotview: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                                  morSlotview.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 30 : 40)
                                   morSlotview.frame.origin.x = ((cell?.morView.frame.origin.x)! )
                                   morSlotview.frame.size.width = ((cell?.morView.frame.size.width)! )
                                     morSlotview.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 35 : 40))
                                              
                                              let timeStr = morListArr[i].value(forKey: "mttiming") as! String
                                              let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
                                              var timeStr2 = ""
                                              if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                                              }else {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                                              }
                                              morSlotview.lbl.text = timeStr1 + "-" + timeStr2
                                   morSlotview.imgView.image = UIImage(named:"checkBox")
                                
                                if morListArr.count != 0 {
                                    cell?.morView.addSubview(morSlotview)
                                    cell?.mlbl.text = "Morning"
                                }
                                
           }
                               for i in 0..<noonListArr.count {
                                             let noonSlotview: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                                               noonSlotview.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 30 : 40)
                                               
                                              
                                               noonSlotview.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 35 : 40))
                                              
                                              let timeStr = noonListArr[i].value(forKey: "mttiming") as! String
                                              let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
                                              var timeStr2 = ""
                                              if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                                              }else {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                                              }
                                              noonSlotview.lbl.text = timeStr1 + "-" + timeStr2
                                   noonSlotview.imgView.image = UIImage(named:"checkBox")
                                
                                if morListArr.count != 0 && noonListArr.count != 0 {
                                   noonSlotview.frame.origin.x = 0
                                   cell?.noonView.addSubview(noonSlotview)
                                     noonSlotview.frame.size.width = (cell?.noonView.frame.size.width)!
                                   cell?.nlbl.text = "Afternoon"
                                }else if morListArr.count == 0 && noonListArr.count != 0 {
                                    noonSlotview.frame.origin.x = (cell?.morView.frame.origin.x)!
                                     noonSlotview.frame.size.width = (cell?.noonView.frame.size.width)!
                                    cell?.morView.addSubview(noonSlotview)
                                    cell?.mlbl.text = "Afternoon"
                                }
                                
                 }
                    for i in 0..<eveListArr.count {
                                               let eveSlotview: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                                               eveSlotview.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 30 : 40)
                                               eveSlotview.frame.origin.x = (cell?.eveView.frame.origin.x)!
                                               eveSlotview.frame.size.width = (cell?.eveView.frame.size.width)!
                                               eveSlotview.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 35 : 40))
                                              
                                              let timeStr = eveListArr[i].value(forKey: "mttiming") as! String
                                              let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
                                              var timeStr2 = ""
                                              if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                                              }else {
                                              timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                                              }
                                              eveSlotview.lbl.text = timeStr1 + "-" + timeStr2
                                   eveSlotview.imgView.image = UIImage(named:"checkBox")
                        
                        if morListArr.count != 0 && noonListArr.count != 0 && eveListArr.count != 0{
                            eveSlotview.frame.origin.x = 0
                            cell?.eveView.addSubview(eveSlotview)
                            cell?.elbl.text = "Evening"
                        }else if morListArr.count == 0 && noonListArr.count != 0 && eveListArr.count != 0 {
                            eveSlotview.frame.origin.x = 0
                            cell?.noonView.addSubview(eveSlotview)
                            cell?.nlbl.text = "Evening"
                        }else if morListArr.count == 0 && noonListArr.count == 0 && eveListArr.count != 0 {
                            eveSlotview.frame.origin.x = 0
                            cell?.morView.addSubview(eveSlotview)
                            cell?.mlbl.text = "Evening"
                        }
                        if eveListArr.count == 0 {
                            cell?.elbl.text = ""
                        }
                        
               }
                
                // cell
                if listArray[indexPath.row].value(forKey: "adsubtype") as? String != nil {
                    if listArray[indexPath.row].value(forKey: "adsubtype") as! String == "SD" {
                                       cell?.fromdatelbl.text = "\(listArray[indexPath.row].value(forKey: "fromdate") as! String)"
                                      cell?.multiView.isHidden = true
                                  }else{
                                       cell?.multiView.isHidden = false
                                       cell?.frmlbl.text = "\(listArray[indexPath.row].value(forKey: "fromdate") as! String)"
                                      cell?.todatelbl.text = "\(listArray[indexPath.row].value(forKey: "todate") as! String)"
                                  }
                }
              
                
                         cell?.datelbl.text = listArray[indexPath.row].value(forKey: "created") as? String
                
                         cell?.editBtn.tag = indexPath.row
                          cell?.editBtn.addTarget(self, action: #selector(editAction(sender:)), for: .touchUpInside)
                          cell?.deleteBtn.tag = indexPath.row
                          cell?.deleteBtn.addTarget(self, action: #selector(deleteAction(sender:)), for: .touchUpInside)
                
                
               return cell!
                        
              }else if tableView == mornTblView {
                  let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as? slotCell)
                let timeStr = morArr[indexPath.row].value(forKey: "mttiming") as! String
                 let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
                  var timeStr2 = ""
                  if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                       timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                 }else {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                 }
                 cell?.lbl.text = timeStr1 + "-" + timeStr2
                if morArr[indexPath.row].value(forKey: "isSelect") as! String == "0" {
                  cell?.imgView.image = UIImage(named: "unCheck")
                }else{
                    cell?.imgView.image = UIImage(named: "checkBox")
                }
                  return cell!
              }else if tableView == noonTblView {
                  let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell1") as? slotCell)
                let timeStr = noonArr[indexPath.row].value(forKey: "mttiming") as! String
                let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
                let timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                cell?.lbl.text = timeStr1 + "-" + timeStr2
                if noonArr[indexPath.row].value(forKey: "isSelect") as! String == "0" {
                  cell?.imgView.image = UIImage(named: "unCheck")
                }else{
                    cell?.imgView.image = UIImage(named: "checkBox")
                }
                  return cell!
              }else if tableView == eveTblView {
                  let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell2") as? slotCell)
                let timeStr = eveArr[indexPath.row].value(forKey: "mttiming") as! String
                let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
                var timeStr2 = ""
                if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                   timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                }else {
                   timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                 }
                 cell?.lbl.text = timeStr1 + "-" + timeStr2
                if eveArr[indexPath.row].value(forKey: "isSelect") as! String == "0" {
                  cell?.imgView.image = UIImage(named: "unCheck")
                }else{
                    cell?.imgView.image = UIImage(named: "checkBox")
                }
                  return cell!
              }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == mornTblView {
            let dict = self.morArr[indexPath.row].mutableCopy() as! NSMutableDictionary
                   if dict.value(forKey: "isSelect") as! String == "0" {
                       dict.setValue("1", forKey: "isSelect")
                       self.morArr[indexPath.row] = dict as NSDictionary
                   }else  if dict.value(forKey: "isSelect") as! String == "1" {
                       dict.setValue("0", forKey: "isSelect")
                       self.morArr[indexPath.row] = dict as NSDictionary
                   }
            mornTblView.reloadData()
        }else if tableView == noonTblView {
            let dict = self.noonArr[indexPath.row].mutableCopy() as! NSMutableDictionary
                   if dict.value(forKey: "isSelect") as! String == "0" {
                       dict.setValue("1", forKey: "isSelect")
                       self.noonArr[indexPath.row] = dict as NSDictionary
                   }else  if dict.value(forKey: "isSelect") as! String == "1" {
                       dict.setValue("0", forKey: "isSelect")
                       self.noonArr[indexPath.row] = dict as NSDictionary
                   }
            noonTblView.reloadData()
        }else if tableView == eveTblView {
            let dict = self.eveArr[indexPath.row].mutableCopy() as! NSMutableDictionary
                   if dict.value(forKey: "isSelect") as! String == "0" {
                       dict.setValue("1", forKey: "isSelect")
                       self.eveArr[indexPath.row] = dict as NSDictionary
                   }else  if dict.value(forKey: "isSelect") as! String == "1" {
                       dict.setValue("0", forKey: "isSelect")
                       self.eveArr[indexPath.row] = dict as NSDictionary
                   }
            eveTblView.reloadData()
        }
    }
    
  @objc func editAction(sender:UIButton){
     isEditBlockSlot = true
     arrHeightval = 0
     select = listArray[sender.tag].value(forKey: "adsubtype") as! String
    if select == "SD" {
        fromDateTf.text = listArray[sender.tag].value(forKey: "fromdate") as? String
    }else{
        fromDateTf.text = listArray[sender.tag].value(forKey: "fromdate") as? String
        toDateTf.text = listArray[sender.tag].value(forKey: "toDate") as? String
    }
     blockSLOTSAPICall()
 }
        
  @objc func deleteAction(sender:UIButton){
              // Create the alert controller
                let alertController = UIAlertController(title: "", message: "Are you sure want to delete?", preferredStyle: .alert)
                       // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                           UIAlertAction in
                    self.deleteBlockSLOTSAPICall(id: self.listArray[sender.tag].value(forKey: "adid") as! Int)
                           NSLog("OK Pressed")
                       }
                let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                           UIAlertAction in
                           NSLog("Cancel Pressed")
                       }
                       // Add the actions
                       alertController.addAction(okAction)
                       alertController.addAction(cancelAction)
                       // Present the controller
                self.present(alertController, animated: true, completion: nil)
          }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           let currentDate = Date()
           var dateComponents = DateComponents()
           dateComponents.month = 12
           let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
           let threeDaysFromNow =  Calendar.current.date(byAdding: .day, value: 0, to: Date())
           let fourDaysFromNow =  Calendar.current.date(byAdding: .day, value: 1, to: Date())
           DatePickerDialog().show("Date", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: textField == fromDateTf ? threeDaysFromNow : fourDaysFromNow,maximumDate: threeMonthAgo,datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
               if textField == self.fromDateTf {
                    self.fromDateTf.text = formatter.string(from: dt)
                self.fromdate = dt
               }else if textField == self.toDateTf {
                    self.toDateTf.text = formatter.string(from: dt)
                self.toDate = dt
               }
               }
             }
            return false
       }

    //MARK:- Ai call
    func blockListAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = blockSlotListURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.listArray = allServiceDict.value(forKey: "List") as! [NSDictionary]
                               
                                self.tblView.reloadData()
                             //   AMShimmer.stop(for: self.tblView)
                                if self.listArray.count == 0 {
                                  self.toastLbl.isHidden = false
                                }else{
                                    self.toastLbl.isHidden = true
                                }
                            }else{
                                
                                
                             if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                  self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                      self.logout()
                                  })
                              }else {
                                     self.toastLbl.isHidden = false
                              }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.toastLbl.isHidden = false
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func blockSLOTSAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = blockSlotTimesURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.timeSlotArray = allServiceDict.value(forKey: "List") as! [NSDictionary]
                                
                                self.morArr.removeAll()
                                self.noonArr.removeAll()
                                self.eveArr.removeAll()
                                
                                for i in 0..<self.timeSlotArray.count {
                                    let dict = self.timeSlotArray[i].mutableCopy() as! NSMutableDictionary
                                    if dict.value(forKey: "mttype") as! String == "M" {
                                            dict.setValue("0", forKey: "isSelect")
                                        self.morArr.append(dict as NSDictionary)
                                    }else if dict.value(forKey: "mttype") as! String == "A" {
                                        dict.setValue("0", forKey: "isSelect")
                                        self.noonArr.append(dict)
                                    }else if dict.value(forKey: "mttype") as! String == "E" {
                                        dict.setValue("0", forKey: "isSelect")
                                        self.eveArr.append(dict)
                                    }
                                }
                                
                                print(self.morArr.count)
                                print(self.noonArr.count)
                                print(self.eveArr.count)
                                let maxArr = max(max(self.morArr.count, self.noonArr.count), self.eveArr.count)
                                self.arrHeightval = maxArr * 35
                                self.tblView.reloadData()
                                if self.timeSlotArray.count != 0 {
                                    self.hideView.isHidden = false
                                }else{
                                    self.view.makeToast("Slots not available", duration: 2.0, position: .center)
                                    self.hideView.isHidden = true
                                }
                                
                                self.mornTblView.reloadData()
                                self.noonTblView.reloadData()
                                self.eveTblView.reloadData()
                                
                                
                                if self.isEditBlockSlot == true {
                                    if self.select == "SD" {
                                        self.heightSetu(selection: "MD")
                                    }else{
                                        self.heightSetu(selection: "MD")
                                    }
                                }else{
                                    self.heightSetu(selection: "SD")
                                }
                                
                            }else{
                               
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                                                                                                             self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                                             DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                                                 self.logout()
                                                                                                                                             })
                                                                                                                                         }else {
                                                                                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                                         }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    func editBlockSLOTSAPICall(id:Int,type:String) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = blockSlotListURL + "\(id)"
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        var slotId = ""
                      for i in 0..<blockListIdArr.count {
                          slotId = slotId + "\(blockListIdArr[i]),"
                      }
               let params = ["adtype":"BS","subtype":type,"fromdate":convertDate(dateStr: fromDateTf.text!),"todate": toDateTf.text != "" ? convertDate(dateStr: toDateTf.text!) : "","slots":blockListIdArr.count != 0 ? (slotId.dropLast()): ""] as [String : Any]
        print(strURL)
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                              
                            }else{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func craeteBlockSLOTSAPICall(type:String) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = blockSlotListURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        var slotId = ""
               for i in 0..<blockListIdArr.count {
                  if blockListIdArr[i] != 0 {
                     slotId = slotId + "\(blockListIdArr[i]),"
                 }
               }
        let params = ["adtype":"BS","subtype":type,"fromdate":convertDate(dateStr: fromDateTf.text!),"todate": toDateTf.text != "" ? convertDate(dateStr: toDateTf.text!) : "","slots":blockListIdArr.count != 0 ? (slotId.dropLast()): "",
                      "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")] as [String : Any]
        print(strURL)
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.hideView.isHidden = true
                                self.blockListAPICall()
                            }else{
                              
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                    self.logout()
                                                                                                                })
                                                                                                            }else {
                                                                  self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    self.hideView.isHidden = true
                                                                                                            }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func deleteBlockSLOTSAPICall(id:Int) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = blockSlotListURL + "/\(id)"
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        var slotId = ""
            for i in 0..<blockListIdArr.count {
               slotId = slotId + "\(blockListIdArr[i]),"
            }
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.delete, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.blockListAPICall()
                            }else{
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                    self.logout()
                                                                                                                })
                                                                                                            }else {
                                                                                                                  self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                            }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
}


   
  
