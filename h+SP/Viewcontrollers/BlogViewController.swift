//
//  BlogViewController.swift
//  h+SP
//
//  Created by mirrorminds on 20/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView

class BlogViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
 @IBOutlet weak var tblView: UITableView!
 @IBOutlet weak var backBtn: UIButton!
 @IBOutlet weak var MenuBtn: UIButton!
    var listArr = [NSDictionary]()
    
    var fromView = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//       if fromView == "dash" {
//            MenuBtn.isHidden = true
//            backBtn.isHidden = false
//
//        }else {
//            MenuBtn.isHidden = false
//           backBtn.isHidden = true
//         gestur()
//        }
        feedListAPICall()
    }
    func gestur(){
                     let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     left.direction = .left
                     self.view.addGestureRecognizer(left)
             
                     let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     right.direction = .right
                     self.view.addGestureRecognizer(right)
                 }
             
             
                 @objc func Swipe(){
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
                 }
    // MARK: - btnActions
       @IBAction func menuAction(_ sender: UIButton) {
//           if sender.tag == 0 {
//
//           }else if sender.tag == 1 {
//               self.dismiss(animated: true, completion: nil)
//           }
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
       }
    // MARK: - tblView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if UIDevice.current.userInterfaceIdiom == .pad {
            return UITableView.automaticDimension
        }else {
        return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if UIDevice.current.userInterfaceIdiom == .pad {
               var  cell: FeedTableViewCell_ipad? = (tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell_ipad") as? FeedTableViewCell_ipad)
                     let nib: [Any] = Bundle.main.loadNibNamed("FeedTableViewCell_ipad", owner: self, options: nil)!
                     cell = (nib[0]  as? FeedTableViewCell_ipad)!
                     cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                     cell?.bgView.layer.shadowOpacity = 1
                     cell?.bgView.layer.shadowOffset = CGSize.zero
                     cell?.bgView.layer.shadowRadius = 2
                     cell?.bgView.layer.cornerRadius = 0
            if listArr[indexPath.row].value(forKey: "image") as? String != "" && listArr[indexPath.row].value(forKey: "image") as? String != nil {
                cell?.imgView.af_setImage(withURL: URL(string: listArr[indexPath.row].value(forKey: "image") as! String)!)
            }
            cell?.lbl.text = listArr[indexPath.row].value(forKey: "title") as? String
                     return cell!
        }else {
             var  cell: FeedTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as? FeedTableViewCell)
                     let nib: [Any] = Bundle.main.loadNibNamed("FeedTableViewCell", owner: self, options: nil)!
                     cell = (nib[0]  as? FeedTableViewCell)!
                     cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                     cell?.bgView.layer.shadowOpacity = 1
                     cell?.bgView.layer.shadowOffset = CGSize.zero
                     cell?.bgView.layer.shadowRadius = 2
                     cell?.bgView.layer.cornerRadius = 0
            if listArr[indexPath.row].value(forKey: "image") as? String != "" && listArr[indexPath.row].value(forKey: "image") as? String != nil {
                cell?.imgView.af_setImage(withURL: URL(string: listArr[indexPath.row].value(forKey: "image") as! String)!)
            }
            cell?.lbl.text = listArr[indexPath.row].value(forKey: "title") as? String
                     return cell!
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "FeedDeatilViewController") as! FeedDeatilViewController
        controller.feedDict = listArr[indexPath.row]
               controller.modalPresentationStyle = .fullScreen
               controller.modalTransitionStyle = .coverVertical
               present(controller, animated: true, completion: nil)
       }
    }
    
    func feedListAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = feedURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.listArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                                self.tblView.reloadData()
                            }else{
                               
                                
                                                           
                                                        if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                             self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                             DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                 self.logout()
                                                             })
                                                         }else {
                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
}
