//
//  ChangepassViewController.swift
//  h+SP
//
//  Created by mirrorminds on 21/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView
class ChangepassViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var olfTf: UITextField!
    @IBOutlet weak var newTf: UITextField!
    @IBOutlet weak var confirmTf: UITextField!
    @IBOutlet weak var bottomView: UIView!
     @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 25 : 40))
        topView.roundCorners(corners: [.layerMaxXMaxYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 25 : 40))
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/1.5
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
   // MARK: - tf delegates
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
       return true
   }
    // MARK: - btnActions
       @IBAction func menuAction(_ sender: UIButton) {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
       }
    @IBAction func submitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        validation()
    }
    func validation(){
        if olfTf.text != "" {
            if newTf.text != "" {
                if confirmTf.text != "" {
                    if newTf.text == confirmTf.text {
                          changePasswordAPICall()
                        }else {
                            self.view.makeToast("Please check your Confirm Password", duration: 2.0, position: .center)
                        }
                }else {
                    self.view.makeToast("Please enter your Confirm Password", duration: 2.0, position: .center)
                }
            }else {
                self.view.makeToast("Please enter your New Password", duration: 2.0, position: .center)
            }
        }else {
            self.view.makeToast("Please enter your Old Password", duration: 2.0, position: .center)
        }
    }

//MARK:- API CALL
func changePasswordAPICall() {
  if Reachability.isConnectedToNetwork() {
    KVSpinnerView.show(saying: "")
    let strURL = changepassURL
     let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
      let headers = ["Content-Type": "application/x-www-form-urlencoded",
                              "Authorization":strAccessToken]
    let params = [
             "oldpassword" : "\(olfTf.text!)",
             "newpassword" : "\(newTf.text!)",
        "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")
         ]
  
    print(params)
    print(strURL)
    print(headers)
              Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                  {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.bottomView.isHidden = false
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                self.goHome()
                            })
                        }else{
                                    if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                    self.logout()
                                                                                })
                                                                            }else {
                                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                            }
                        }
                        
                       DispatchQueue.main.async {
                           KVSpinnerView.dismiss()
                       }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
              }
          }else {
           self.view.makeToast("NetWork error", duration: 3.0, position: .center)
         }
}
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
    func goHome(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        controller.selectedMenu = "Home"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
}
