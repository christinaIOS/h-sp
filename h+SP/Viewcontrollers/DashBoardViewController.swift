//
//  DashBoardViewController.swift
//  h+SP
//
//  Created by mirrorminds on 12/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView
import FSPagerView
import AlamofireImage

class DashBoardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FSPagerViewDataSource, FSPagerViewDelegate {
    @IBOutlet weak var feedCollectionView: UICollectionView!
    @IBOutlet weak var myInfo: UIView!
    @IBOutlet weak var absentView: UIView!
    @IBOutlet weak var toview: UIView!
    @IBOutlet weak var settlementView: UIView!
    @IBOutlet weak var bloclSlotView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var bookingLbl: UILabel!
    @IBOutlet weak var seg: UISegmentedControl!
    @IBOutlet weak var myinfoImg: UIImageView!
    @IBOutlet weak var myinfolbl: UILabel!
    @IBOutlet weak var menuImgView: UIImageView!
    @IBOutlet weak var settleBgView: UIView!
    @IBOutlet weak var settleView2: UIView!
    @IBOutlet weak var feedBtn: UIButton!
    @IBOutlet weak var feedLbl: UILabel!
    @IBOutlet weak var videocallswitch: UISwitch!
    @IBOutlet weak var houseVisitswitch: UISwitch!
    @IBOutlet weak var btnInfo: UIButton!
   // view setu
     @IBOutlet weak var bgView: UIView!
     @IBOutlet weak var view1: NSLayoutConstraint!
     @IBOutlet weak var view2: NSLayoutConstraint!
     @IBOutlet weak var view3: NSLayoutConstraint!
     @IBOutlet weak var bottomConts: NSLayoutConstraint!
     @IBOutlet weak var bannerHeight: NSLayoutConstraint!
     @IBOutlet weak var bannerBottom: NSLayoutConstraint!
     @IBOutlet weak var bannerTop: UIView!
    @IBOutlet weak var settlementTopBg: UIView!
    @IBOutlet weak var settlementTop: UIView!
    
    @IBOutlet weak var commentView: UIView!
        @IBOutlet weak var commentLbl: UILabel!
    
    
    var docCategory = ""
    var listArr = [NSDictionary]()
    var dictUser : NSDictionary!
    var arrBanner = [NSDictionary]()
    var comments = ""
   
     var versionNum = ""
    
    @IBOutlet weak var ageControl: UIPageControl!
       
       @IBOutlet weak var bannerView: FSPagerView! {
           didSet {
               self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.bannerView.transformer = FSPagerViewTransformer(type: .crossFading)
               self.bannerView.contentMode = .scaleAspectFit
            
            if UserDefaults.standard.value(forKey: "bannerTiming") as? Int != nil {
                self.bannerView.automaticSlidingInterval = CGFloat(UserDefaults.standard.value(forKey: "bannerTiming") as! Int)
            }else{
                self.bannerView.automaticSlidingInterval = 4.0
            }
               
               self.bannerView.isInfinite = true
               self.bannerView.dataSource = self
               self.bannerView.delegate = self
           }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setuView()
        settingsAPICall()
        getBannerAPICall()
        
       // feedListAPICall()
        
        if #available(iOS 13.0, *) {
            self.view.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        videocallswitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        houseVisitswitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
               KVSpinnerView.settings.statusTextColor = UIColor.darkGray
               KVSpinnerView.settings.tintColor = UIColor.darkGray
         
        
              getAppStoreInform()
        
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
       // let value = mySwitch.isOn
        // Do something
        if mySwitch == videocallswitch {
            statusAPICall()
        }
        
        if mySwitch == houseVisitswitch {
            statusAPICall()
        }
        
    }
    
    func setuView(){
        
        self.videocallswitch.superview?.superview?.isHidden = true
         self.bottomConts.constant = 40
                                           
       NotificationCenter.default.addObserver(self, selector: #selector(self.loadData), name: NSNotification.Name(rawValue: "loadData"), object: nil)
        
        docCategory = UserDefaults.standard.value(forKey: "docCat") as! String
        Constant.addStatusBar(view: self.view)
        feedCollectionView.register(UINib(nibName: "FeedCollectionViewCell", bundle: nil) , forCellWithReuseIdentifier: "FeedCollectionViewCell")
        feedCollectionView.register(UINib(nibName: "FeedCollectionViewCell_ipad", bundle: nil) , forCellWithReuseIdentifier: "FeedCollectionViewCell_ipad")
        
        AddShadow(shadView: myInfo)
        AddShadow(shadView: absentView)
       
        AddShadow(shadView: bloclSlotView)
        AddShadow(shadView: centerView)
       
        AddShadow(shadView: settlementView)
        
        if docCategory == "PHA" || docCategory == "MOR" || docCategory == "AMB"  {
             AddShadow(shadView: settleBgView)
            settleBgView.isHidden = false
            bookingLbl.text = "Orders"
        }else if docCategory == "DYL" {
            AddShadow(shadView: settleBgView)
                      settleBgView.isHidden = false
                      bookingLbl.text = "Appointments"
        }else{
            settleBgView.isHidden = true
            bookingLbl.text = "Appointments"
        }
        
        if docCategory == "NUR" {
             AddShadow(shadView: settleBgView)
            AddShadow(shadView: settlementTopBg)
            settlementTopBg.isHidden = false
            settleBgView.isHidden = false
        }
        
       centerView.layer.shadowColor = UIColor.lightGray.cgColor
       centerView.layer.shadowOpacity = 1
       centerView.layer.shadowOffset = CGSize.zero
       centerView.layer.shadowRadius = 2
                     //centerView.layer.cornerRadius = 8
       //toview.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 30)
      //  toview.roundCorners(corners: [.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 30 : 60))
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
               let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
       seg.setTitleTextAttributes(titleTextAttributes, for: .normal)
        seg.setTitleTextAttributes(titleTextAttributes1, for: .selected)
        gestur()
    }
    
    @objc func loadData(){
        
//       if UserDefaults.standard.value(forKey: "profileImg") as! String != "" {
//         menuImgView.layer.cornerRadius = menuImgView.frame.size.width/2
//           menuImgView.af_setImage(withURL: URL(string: UserDefaults.standard.value(forKey: "profileImg") as! String)!)
//       }
    }
    
    func AddShadow(shadView:UIView){
        shadView.layer.shadowColor = UIColor.lightGray.cgColor
        shadView.layer.shadowOpacity = 1
        shadView.layer.shadowOffset = CGSize.zero
        shadView.layer.shadowRadius = 2
        shadView.layer.cornerRadius = 8
    }
    func gestur(){
                     let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     left.direction = .left
                     self.view.addGestureRecognizer(left)
             
                     let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     right.direction = .right
                     self.view.addGestureRecognizer(right)
                 }
             
             
                 @objc func Swipe(){
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
                 }
   @IBAction func btnCloseComment(_ sender: Any) {
            commentView.isHidden = true
        }
        
        @IBAction func btnInfo(_ sender: Any) {
    //        let next = self.storyboard?.instantiateViewController(withIdentifier: "commentsViewController") as! commentsViewController
    //        next.commentTxt = comments
    //        next.modalPresentationStyle = .fullScreen
    //        self.present(next, animated: true, completion: nil)
            commentLbl.attributedText = comments.htmlToAttributedString
            commentView.isHidden = false
            
        }
    // MARK: - FSPagerView Delegate
       
       func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.arrBanner.count == 0 ? (1) : (self.arrBanner.count)
       }
       
       func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
           let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
          
        //cell.imageView?.image = UIImage(named: "in")
        
        var img = ""
        
        if self.arrBanner.count != 0 {
            img =  self.arrBanner[index].value(forKey: "image") as! String
        }
        if img != "" && img != nil {
             cell.imageView?.af_setImage(withURL: URL(string: img)!)
        }else{
           //  cell.imageView?.image = UIImage(named: "in")
        }
       
        
           cell.imageView?.layer.cornerRadius = 5
           cell.imageView?.contentMode = .scaleToFill
           cell.imageView?.clipsToBounds = true
          self.ageControl.currentPage = index
        
      
        
           return cell
       }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ZoomViewController") as! ZoomViewController
        controller.dict = self.arrBanner[index]
                           controller.modalPresentationStyle = .fullScreen
                           controller.modalTransitionStyle = .coverVertical
                           present(controller, animated: true, completion: nil)
    }

       
       func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let page = scrollView.contentOffset.x / scrollView.frame.width
           ageControl.currentPage = Int(page)
       }
    
    // MARK: - btnActions
    @IBAction func menuAction(_ sender: Any) {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
          let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
            controller.fromview = "O"
          controller.modalPresentationStyle = .fullScreen
          controller.modalTransitionStyle = .coverVertical
          present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func dashBoardAction(_ sender: UIButton) {
        
        if UserDefaults.standard.value(forKey: "assistLogin") as? String != nil {
            if UserDefaults.standard.value(forKey: "assistLogin") as! String == "Y" {
                
                
                if sender.tag == 0 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Appointments"
                    controller.modalPresentationStyle = .fullScreen
                                   controller.modalTransitionStyle = .coverVertical
                                   present(controller, animated: true, completion: nil)
                }else if sender.tag == 1 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Absent Dates"
                    controller.modalPresentationStyle = .fullScreen
                                   controller.modalTransitionStyle = .coverVertical
                                   present(controller, animated: true, completion: nil)
                }else if sender.tag == 5 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Feeds"
                    controller.modalPresentationStyle = .fullScreen
                                   controller.modalTransitionStyle = .coverVertical
                                   present(controller, animated: true, completion: nil)
                }else if sender.tag == 2 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Block Slots"
                    controller.modalPresentationStyle = .fullScreen
                                   controller.modalTransitionStyle = .coverVertical
                                   present(controller, animated: true, completion: nil)
                }else if sender.tag == 3 || sender.tag == 4 {
                    self.view.makeToast("Only main user can access.", duration: 2.0, position: .center)
                }
                
               
            }else{
                
                
                if sender.tag == 0 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Appointments"
                    controller.modalPresentationStyle = .fullScreen
                    controller.modalTransitionStyle = .coverVertical
                    present(controller, animated: true, completion: nil)
                }else if sender.tag == 1 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Absent Dates"
                    controller.modalPresentationStyle = .fullScreen
                    controller.modalTransitionStyle = .coverVertical
                    present(controller, animated: true, completion: nil)
                }else if sender.tag == 3 {
                    //controller.selectedMenu = "My Info"
                }else if sender.tag == 4 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Settlements"
                    controller.modalPresentationStyle = .fullScreen
                    controller.modalTransitionStyle = .coverVertical
                    present(controller, animated: true, completion: nil)
                }else if sender.tag == 5 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Feeds"
                    controller.modalPresentationStyle = .fullScreen
                    controller.modalTransitionStyle = .coverVertical
                    present(controller, animated: true, completion: nil)
                }else if sender.tag == 2 {
                    let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    controller.selectedMenu = "Block Slots"
                    controller.modalPresentationStyle = .fullScreen
                    controller.modalTransitionStyle = .coverVertical
                    present(controller, animated: true, completion: nil)
                }
                
               
            }
        }else{
            let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            if sender.tag == 0 {
                controller.selectedMenu = "Appointments"
            }else if sender.tag == 1 {
                controller.selectedMenu = "Absent Dates"
            }else if sender.tag == 3 {
                controller.selectedMenu = "My Info"
            }else if sender.tag == 4 {
                controller.selectedMenu = "Settlements"
            }else if sender.tag == 5 {
                controller.selectedMenu = "Feeds"
            }else if sender.tag == 2 {
                controller.selectedMenu = "Block Slots"
            }
            
            controller.modalPresentationStyle = .fullScreen
            controller.modalTransitionStyle = .coverVertical
            present(controller, animated: true, completion: nil)
        }
        
      
    }
  
    
    //MARK:- collectionView delegates
         func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
         }
       
          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return listArr.count
           }
       
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
                cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
                cell.bgView.layer.shadowOpacity = 1
                cell.bgView.layer.shadowOffset = CGSize.zero
                cell.bgView.layer.shadowRadius = 2
                if listArr[indexPath.row].value(forKey: "image") as? String != "" && listArr[indexPath.row].value(forKey: "image") as? String != nil {
                    cell.imgView.af_setImage(withURL: URL(string: listArr[indexPath.row].value(forKey: "image") as! String)!)
                }
                cell.lbl.text = listArr[indexPath.row].value(forKey: "title") as? String
                return cell
            } else {
               let cell   = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell_ipad", for: indexPath) as! FeedCollectionViewCell_ipad
                cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
                cell.bgView.layer.shadowOpacity = 1
                cell.bgView.layer.shadowOffset = CGSize.zero
                cell.bgView.layer.shadowRadius = 2
                if listArr[indexPath.row].value(forKey: "image") as? String != "" && listArr[indexPath.row].value(forKey: "image") as? String != nil {
                    cell.imgView.af_setImage(withURL: URL(string: listArr[indexPath.row].value(forKey: "image") as! String)!)
                }
                cell.lbl.text = listArr[indexPath.row].value(forKey: "title") as? String
                return cell
            }
             
       }
       
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if UIDevice.current.userInterfaceIdiom == .phone {
                return CGSize(width: ((feedCollectionView.frame.size.width - 10)/2.3), height: feedCollectionView.frame.size.height)
            }else{
                return CGSize(width: 300, height: feedCollectionView.frame.size.height)
            }
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
            let controller = storyboard?.instantiateViewController(withIdentifier: "FeedDeatilViewController") as! FeedDeatilViewController
            controller.feedDict = listArr[indexPath.row]
                   controller.modalPresentationStyle = .fullScreen
                   controller.modalTransitionStyle = .coverVertical
                   present(controller, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
       
   }
    
    func feedListAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = feedURL
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.listArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                                self.feedCollectionView.reloadData()
                                if self.listArr.count == 0 {
                                    self.feedBtn.isHidden = true
                                    self.feedLbl.text = "No Feeds Found"
                                }else if self.listArr.count <= 2 {
                                    self.feedBtn.isHidden = true
                                    self.feedLbl.text = "Latest Feeds"
                                }else{
                                    self.feedBtn.isHidden = false
                                    self.feedLbl.text = "Latest Feeds"
                                }
                            }else{
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                           // self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }

    
        func settingsAPICall() {
          if Reachability.isConnectedToNetwork() {
           
            let strURL = settings
               let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                               "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true"{
                                    
                                    let veriStatus = allServiceDict.value(forKey: "verifiedstatus") as! String
                                    UserDefaults.standard.setValue((allServiceDict.value(forKey: "bannertiming") as! NSString).integerValue, forKey: "bannerTiming")
                                    
                                    self.bannerView.automaticSlidingInterval = CGFloat((allServiceDict.value(forKey: "bannertiming") as! NSString).integerValue)
                                    
                                                                      self.comments = allServiceDict.value(forKey: "comments") as! String
                                                                      if veriStatus == "E" {
                                                                          self.btnInfo.isHidden = false
                                                                       }else{
                                                                          self.btnInfo.isHidden = true
                                                                       }

                                    UserDefaults.standard.setValue(allServiceDict.value(forKey: "supportno") as! String, forKey: "contactNo")
                                       if allServiceDict.value(forKey: "housevisitonline") as! String == "" && allServiceDict.value(forKey: "videoonline") as! String == "" {

                                        self.videocallswitch.superview?.superview?.isHidden = true
                                        self.bottomConts.constant = 40
                                       }else{

                                        self.videocallswitch.superview?.superview?.isHidden = false
                                        self.bottomConts.constant = 0
                                    }
                                    
                                    if allServiceDict.value(forKey: "videoonline") as! String == "Y" {
                                        self.videocallswitch.isOn = true
                                    }else if allServiceDict.value(forKey: "videoonline") as! String == "N" {
                                        self.videocallswitch.isOn = false
                                    }else{
                                        self.videocallswitch.superview?.isHidden = true
                                    }
                                    
                                    if allServiceDict.value(forKey: "housevisitonline") as! String == "Y" {
                                        self.houseVisitswitch.isOn = true
                                    }else if allServiceDict.value(forKey: "housevisitonline") as! String == "N" {
                                        self.houseVisitswitch.isOn = false
                                    }else{
                                        self.houseVisitswitch.superview?.isHidden = true
                                    }
                                    
                                    
                                    }else{
                                      self.videocallswitch.superview?.superview?.isHidden = true
                                      self.bottomConts.constant = 40
                                    }
                               DispatchQueue.main.async {
                                   KVSpinnerView.dismiss()
                               }
                            case .failure(let error):
                                print(error)
                                self.videocallswitch.superview?.superview?.isHidden = true
                                self.bottomConts.constant = 40
                             //   self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
    //                           DispatchQueue.main.async {
    //                                KVSpinnerView.dismiss()
    //                            }
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
  
        func getBannerAPICall() {
          if Reachability.isConnectedToNetwork() {
            
            if ((UserDefaults.standard.value(forKey: "adCount") as! Int) + 1) > 3 {
                UserDefaults.standard.setValue(0, forKey: "adCount")
            }
            
            let addCount = (UserDefaults.standard.value(forKey: "adCount") as! Int) + 1
            UserDefaults.standard.setValue(addCount, forKey: "adCount")
            let strURL = banner + "/\(addCount)"
             let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                      let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                     "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
                     print(strURL)
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true"{
                                    self.arrBanner = allServiceDict.value(forKey: "List") as! [NSDictionary]
                                    if self.arrBanner.count != 0 {
                                        self.bannerHeight.constant = 160
                                        self.bannerBottom.constant = 5
                                        self.bannerTop.backgroundColor = UIColor.clear
                                        self.bannerTop.roundCorners([.bottomLeft, .bottomRight], radius: 20)
                                        self.bannerView.reloadData()
                                    }else{
                                       self.bannerHeight.constant = 160
                                        self.bannerBottom.constant = -70
                                        self.bannerTop.backgroundColor = UIColor(red: 255/255, green: 239/255, blue: 0/255, alpha: 1.0)
                                        self.bannerTop.roundCorners([.bottomLeft, .bottomRight], radius: 0)
                                    }
                                    
                                    }else{
                                        if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                           self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                            UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                               self.logout()
                                                                           })
                                                                       }else {
                                                                           self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                       }
                                    }
                               DispatchQueue.main.async {
                                   KVSpinnerView.dismiss()
                               }
                            case .failure(let error):
                                print(error)
                                self.bannerHeight.constant = 160
                                self.bannerBottom.constant = -70
                                self.bannerTop.backgroundColor = UIColor(red: 255/255, green: 239/255, blue: 0/255, alpha: 1.0)
                                self.bannerTop.roundCorners([.bottomLeft, .bottomRight], radius: 0)
                             //   self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
    //                           DispatchQueue.main.async {
    //                                KVSpinnerView.dismiss()
    //                            }
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
    
    
    func statusAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = liveStatus
      let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
                 print(strURL)
              var params: [String: Any]
        
        params = [
            "videolivestatus" : videocallswitch.isOn == true ? ("Y") : ("N"),
                  "housevisitlivestatus" : houseVisitswitch.isOn == true ? ("Y") : ("N"),
             ]
       
      
        print(params)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }else{
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                    UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                       self.logout()
                                                                   })
                                                               }else {
                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               }
                            }
                            
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                          //  self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
        
    }
    
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
    
    func getAppStoreInform() {
        if Reachability.isConnectedToNetwork() {
            Constant.showLoader(view: self.view)
            let infoDictionary = Bundle.main.infoDictionary
            let appID = infoDictionary!["CFBundleIdentifier"] as! String
            let url = "http://itunes.apple.com/lookup?bundleId=\(appID)"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        UserDefaults.standard.setValue("1", forKey: "udate")
                        
                        let allServiceDict : NSDictionary = JSON as! NSDictionary
                        if allServiceDict.value(forKey: "resultCount") as! NSNumber == 1 {
                            let results:NSArray = (allServiceDict["results"] as? NSArray)!
                            let appstoreVersion = (results[0] as! NSDictionary).value(forKey: "version") as? String
                            let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                            
                            if appstoreVersion != currentVersion {
                              let alertController = UIAlertController(title: "UPDATE AVAILABLE", message: "A new version of ZERODELAY PRO is available.Please update the new version now.", preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "Update" , style: .default, handler: { action in
                                    let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id1522927770?mt=8"
                                    if #available(iOS 10.0, *) {
                                        UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                                    } else {
                                        UIApplication.shared.openURL(URL(string: urlStr)!)
                                    }
                                }))
                                alertController.addAction(UIAlertAction(title: "Not now" , style: .cancel, handler: { action in
                                    
                                }))
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    case .failure(let error):
                        self.view.makeToast("Try Again", duration: 3.0, position: .center)
                    }
                    Constant.hideLoader(view: self.view)
            }
        }
        else {
           // self.view.makeToast(((selectLanguage == ENGLISH) ? "No Internet Connection" : "لا يوجد اتصال بالإنترنت"), duration: 3.0, position: .center)
        }
    }
}
extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
      let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      self.layer.mask = mask
    }
}
//roundCorners(with: [.layerMinXMinYCorner], radius: 20)
//Top left
//
//roundCorners(with: [.layerMaxXMinYCorner], radius: 20)
//Bottom right
//
//roundCorners(with: [.layerMinXMaxYCorner], radius: 20)
//Bottom left
//
//roundCorners(with: [.layerMaxXMaxYCorner], radius: 20)
extension NSLayoutConstraint {
    /**
     Change multiplier constraint

     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
    */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {

        NSLayoutConstraint.deactivate([self])

        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}
