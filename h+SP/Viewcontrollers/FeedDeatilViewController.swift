//
//  FeedDeatilViewController.swift
//  h+SP
//
//  Created by mirrorminds on 19/03/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit
import WebKit
import KVSpinnerView

@available(iOS 13.0, *)
class FeedDeatilViewController: UIViewController,WKUIDelegate,WKNavigationDelegate,UIScrollViewDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
     @IBOutlet weak var webview: WKWebView!
     @IBOutlet weak var titleLbl: UILabel!
    var feedDict : NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
     // textView.attributedText = (feedDict.value(forKey: "description") as! String).htmlToAttributedString
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        let str: NSMutableString =  NSMutableString(string: feedDict.value(forKey: "description") as! String)
        str.insert(" style='color:#fff'", at: 2)
        print(str)
        titleLbl.text = feedDict.value(forKey: "title") as! String
        webview.loadHTMLString(headerString + "\(str)", baseURL: nil)
        self.webview.navigationDelegate = self
        self.webview!.isOpaque = false
      //  self.webview!.backgroundColor = UIColor.clear
        self.webview!.scrollView.backgroundColor = UIColor.clear
        imgView.af_setImage(withURL: URL(string: feedDict.value(forKey: "image") as! String )!)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        scrollView.delegate = self
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
  
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
          return imgView
    }
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
//
//    switch navigationAction.navigationType {
//    case .linkActivated:
//        if navigationAction.targetFrame == nil {
//            self.webview.load(navigationAction.request)// It will load that link in same WKWebView
//        }
//        default:
//            break
//        }
      

//    if let url = navigationAction.request.url {
//        print(url.absoluteString) // It will give the selected link URL
//
//    }
//    decisionHandler(.allow)
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
      return true
  }

  
}

