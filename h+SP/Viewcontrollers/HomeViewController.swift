//
//  HomeViewController.swift
//  MydayliveryDriver
//
//  Created by mirrorminds on 25/02/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
     @IBOutlet weak var imgView: UIImageView!
     @IBOutlet weak var namelbl: UILabel!
     @IBOutlet weak var mobilelbl: UILabel!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var btnRoot: UIView!
    @IBOutlet weak var verifyImg: UIImageView!
    @IBOutlet weak var btnInfo: UIButton!
     @IBOutlet weak var commentView: UIView!
     @IBOutlet weak var commentLbl: UILabel!
    
    
    var selectedMenu = ""
    var selectedCell = ""
    var accesskey = ""
    var type = ""
    var docCategory = ""
    var name = ""
    var mobileNo = ""
    
    var menuArray = [String]()
    var menuArray1 = [String]()
    var comments = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsAPICall()
        viewSetup()
        if #available(iOS 13.0, *) {
            self.view.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        loadData()
    }
    
    func viewSetup(){
        docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
        
       // selectedMenu = "Home"
        if UserDefaults.standard.value(forKey: "assistLogin") as? String != nil {
               if UserDefaults.standard.value(forKey: "assistLogin") as! String == "Y" {
                menuArray = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
               menuArray1 = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
               }else{
                menuArray = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Add Assistant","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
                menuArray1 = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Add Assistant","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
            }
        }else{
            menuArray = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Add Assistant","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
            menuArray1 = ["Home","Appointments","Settlements","Absent Dates","My Info","Block Slots","Notifications","Add Assistant","Mail Us","Call Us","Terms & Conditions","Privacy Policy","Change Password","Logout"]
        }
        
        if docCategory == "NUR"  {
            
             menuArray = ["Home","Appointments","Settlements","My Info","Notifications","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
            
            menuArray1 = ["Home","Appointments","Settlements","My Info","Notifications","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
        }
        
        if docCategory == "PHA"  {
                   menuArray = ["Home","Orders","Settlements","Absent Dates","My Info","Notifications","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
                   menuArray1 = ["Home","Orders","Settlements","Absent Dates","My Info","Notifications","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
               }
        
      if  docCategory == "DYL"  {
                        menuArray = ["Home","Appointments","Settlements","Absent Dates","My Info","Notifications","Add Assistant","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
                        menuArray1 = ["Home","Appointments","Settlements","Absent Dates","My Info","Notifications","Add Assistant","Mail Us","Call Us","Change Password","Terms & Conditions","Privacy Policy","Logout"]
                    }
        
        namelbl.text = name
        mobilelbl.text = mobileNo
        Constant.addStatusBar(view: self.view)
        self.viewMenu.frame = CGRect(x: -((UIScreen.main.bounds.width/3)*2), y: self.viewMenu.frame.origin.y, width: (UIScreen.main.bounds.width/3)*2, height: self.viewMenu.frame.size.height)
        self.btnRoot.frame.origin.x = self.viewMenu.frame.size.width
        self.btnRoot.isHidden = true
     
        NotificationCenter.default.addObserver(self, selector: #selector(self.sideMenu), name: NSNotification.Name(rawValue: "menuAction"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.loadData), name: NSNotification.Name(rawValue: "loadData"), object: nil)
        
        setselected()

        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
               KVSpinnerView.settings.statusTextColor = UIColor.darkGray
               KVSpinnerView.settings.tintColor = UIColor.darkGray
               
        
    }

    @IBAction func btnRoot(_ sender: Any) {
        sideMenu()
    }
    
    @IBAction func btnCloseComment(_ sender: Any) {
        commentView.isHidden = true
    }
    
    @IBAction func btnInfo(_ sender: Any) {
//        let next = self.storyboard?.instantiateViewController(withIdentifier: "commentsViewController") as! commentsViewController
//        next.commentTxt = comments
//        next.modalPresentationStyle = .fullScreen
//        self.present(next, animated: true, completion: nil)
        commentLbl.attributedText = comments.htmlToAttributedString
        commentView.isHidden = false
        
    }
    
    @objc func loadData(){
        if UserDefaults.standard.value(forKey: "profileImg") as? String != nil {
            if UserDefaults.standard.value(forKey: "profileImg") as! String != "" {
              imgView.af_setImage(withURL: URL(string: UserDefaults.standard.value(forKey: "profileImg") as! String)!)
           }
        }
        
        if UserDefaults.standard.value(forKey: "name") as? String != nil {
          if UserDefaults.standard.value(forKey: "name") as! String != "" {
            namelbl.text = UserDefaults.standard.value(forKey: "name") as? String
               }
        }
        
        if UserDefaults.standard.value(forKey: "mobile") as? String != nil {
         if UserDefaults.standard.value(forKey: "mobile") as! String != "" {
            mobilelbl.text = "+91 \(UserDefaults.standard.value(forKey: "mobile") as! String)"
               }
        }
        
        if UserDefaults.standard.value(forKey: "verify") as? String != nil {
                if UserDefaults.standard.value(forKey: "verify") as! String == "N" {
                    verifyImg.tintColor = .systemYellow
                }else if UserDefaults.standard.value(forKey: "verify") as! String == "Y" {
                    verifyImg.tintColor = .systemGreen
                }else{
                    verifyImg.tintColor = UIColor.red
               }
        }
        
        
    }
    @objc func sideMenu(){
        if self.viewMenu.frame.origin.x != 0 {
            UIView.animate(withDuration: 0.4, animations: {
                //  self.viewRoot.frame.origin.x = self.viewMenu.frame.size.width
                self.viewRoot.frame = CGRect(x: self.viewMenu.frame.size.width, y: 50, width: self.viewRoot.frame.size.width, height: self.viewMenu.frame.size.height-100)
                self.viewMenu.frame = CGRect(x: 0, y: self.viewMenu.frame.origin.y, width: (UIScreen.main.bounds.width/3)*2, height: self.viewMenu.frame.size.height)
                self.btnRoot.frame.origin.x = self.viewMenu.frame.size.width
                self.btnRoot.isHidden = false
                //  self.btnRoot.backgroundColor? = UIColor(white: 0, alpha: 0.5)
            }, completion: { (finished: Bool) -> Void in
                UIView.animate(withDuration: 0, animations: {
                    self.btnRoot.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                })
            })
        }
        else {
            self.viewMenu.backgroundColor = UIColor.clear
            self.btnRoot.backgroundColor = UIColor.clear
            UIView.animate(withDuration: 0.4, animations: {
                self.viewMenu.frame = CGRect(x: -((UIScreen.main.bounds.width/3)*2), y: self.viewMenu.frame.origin.y, width: (UIScreen.main.bounds.width/3)*2, height: self.viewMenu.frame.size.height)
                self.viewRoot.frame = CGRect(x: 0, y: 0, width: self.viewRoot.frame.size.width, height: self.viewMenu.frame.size.height)
                //self.viewRoot.frame.origin.x = 0
                self.btnRoot.frame.origin.x = 0
                
                self.btnRoot.isHidden = true
            })
        }
        
    }
    
        func gestur(){
            let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
            left.direction = .left
            self.view.addGestureRecognizer(left)
    
            let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
            right.direction = .right
            self.view.addGestureRecognizer(right)
        }
    
    
        @objc func Swipe(){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }
    
    
    //MARK:- tblview delgates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if UIDevice.current.userInterfaceIdiom == .pad {
              return 75
        }else {
              return 45
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if docCategory == "PHA" || docCategory == "MOR" || docCategory == "AMB" || docCategory == "DYL" {
            return menuArray1.count
        }else{
           return menuArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if UIDevice.current.userInterfaceIdiom == .pad {
            var cell: SideMenuTableViewCell_ipad? = (tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as? SideMenuTableViewCell_ipad)
                            let nib: [Any] = Bundle.main.loadNibNamed("SideMenuTableViewCell_ipad", owner: self, options: nil)!
            cell = (nib[0]  as? SideMenuTableViewCell_ipad)!
            
            if docCategory == "PHA" || docCategory == "MOR" || docCategory == "AMB" || docCategory == "DYL" {
               cell?.imgView.image = UIImage(named: menuArray1[indexPath.row])
                cell?.lbl.text = menuArray1[indexPath.row]
            }else{
               cell?.imgView.image = UIImage(named: menuArray[indexPath.row])
               cell?.lbl.text = menuArray[indexPath.row]
            }
            
                  cell?.bgView.layer.cornerRadius = 7
                  return cell!
               }else {
            var cell: SideMenuTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? SideMenuTableViewCell)
                            let nib: [Any] = Bundle.main.loadNibNamed("SideMenuTableViewCell", owner: self, options: nil)!
            cell = (nib[0]  as? SideMenuTableViewCell)!
          if docCategory == "PHA" || docCategory == "MOR" || docCategory == "AMB" || docCategory == "DYL" {
               cell?.imgView.image = UIImage(named: menuArray1[indexPath.row])
                cell?.lbl.text = menuArray1[indexPath.row]
            }else{
               cell?.imgView.image = UIImage(named: menuArray[indexPath.row])
               cell?.lbl.text = menuArray[indexPath.row]
            }
                  cell?.bgView.layer.cornerRadius = 7
                  return cell!
               }
      
    }
   
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedCell = "1" + "/" + String(indexPath.row)
        tblView.reloadData()
        
        
        if docCategory == "PHA" || docCategory == "MOR" || docCategory == "AMB" || docCategory == "DYL" {
         selectedMenu = menuArray1[indexPath.row]
        }else{
           selectedMenu = menuArray[indexPath.row]
        }
        setselected()
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
            if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                //self.view.makeToast("Please update your profile.", duration: 1.0, position: .center)
                for subview in self.viewRoot.subviews {
                              subview.removeFromSuperview()
                            }
                          var nextView = UIViewController()
                             if docCategory == "DOC" || docCategory == "DIE" || docCategory == "PHY" || docCategory == "DEN"{
                                                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "myInfoViewController") as! myInfoViewController
                             }else if docCategory == "DYL" {
                                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo3ViewController") as! MyInfo3ViewController
                             }else if docCategory == "PHA" {
                                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo4ViewController") as! MyInfo4ViewController
                             }else if docCategory == "NUR" {
                                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo5ViewController") as! MyInfo5ViewController
                             }
                                     else {
                                                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo2ViewController") as! MyInfo2ViewController
                                                                       }
                            addChild(nextView)
                                nextView.view.frame = viewRoot.bounds
                                viewRoot.addSubview(nextView.view)
                                nextView.didMove(toParent: self)
                            
                                if self.viewMenu.frame.origin.x == 0 {
                                  self.sideMenu()
                               }
                           
              return nil
            }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "C" || UserDefaults.standard.value(forKey: "profileEdit") as! String == "V"{
                //self.view.makeToast("Please update your clinic Details.", duration: 1.0, position: .center)
                for subview in self.viewRoot.subviews {
                                            subview.removeFromSuperview()
                                          }
                                        var nextView = UIViewController()
                                           if docCategory == "DOC" || docCategory == "DIE" ||  docCategory == "PHY" || docCategory == "DEN"{
                                                                                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "myInfoViewController") as! myInfoViewController
                                           }else if docCategory == "DYL" {
                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo3ViewController") as! MyInfo3ViewController
                                           }else if docCategory == "PHA" {
                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo4ViewController") as! MyInfo4ViewController
                                           }else if docCategory == "NUR" {
                                                              nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo5ViewController") as! MyInfo5ViewController
                                           } else {
                                                                                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo2ViewController") as! MyInfo2ViewController
                                                                                     }
                                          addChild(nextView)
                                              nextView.view.frame = viewRoot.bounds
                                              viewRoot.addSubview(nextView.view)
                                              nextView.didMove(toParent: self)
                                          
                                              if self.viewMenu.frame.origin.x == 0 {
                                                self.sideMenu()
                                             }
              return nil
            }else{
                return indexPath
            }
        }
        return indexPath
    }
    
    func setselected() {
      
        if UserDefaults.standard.value(forKey: "assistLogin") as? String != nil {
        if UserDefaults.standard.value(forKey: "assistLogin") as! String == "Y" {
            if selectedMenu != "Settlements" && selectedMenu != "My Info" {
                for subview in self.viewRoot.subviews {
                                         subview.removeFromSuperview()
                                       }
                                     var nextView = UIViewController()
                                      if selectedMenu == "Home" {
                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                      }else if selectedMenu == "Appointments" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                                     }else if selectedMenu == "Orders" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                                     }else if selectedMenu == "Absent Dates" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "AbsentViewController") as! AbsentViewController
                                     }else if selectedMenu == "Notifications" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
                                     }else if selectedMenu == "Block Slots" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlockSlotViewController") as! BlockSlotViewController
                                     }else if selectedMenu == "Feeds" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
                                     }else if selectedMenu == "Change Password" {
                                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "ChangepassViewController") as! ChangepassViewController
                                     }else if selectedMenu == "Logout" {
                                           nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                         // Create the alert controller
                                         let alertController = UIAlertController(title: "", message: "Are you sure want to Logout?", preferredStyle: .alert)

                                         let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                                                    UIAlertAction in
                                                    NSLog("OK Pressed")
                                           UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                          self.logout()
                                                }
                                         let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                                                    UIAlertAction in
                                                    NSLog("Cancel Pressed")
                                                }

                                                alertController.addAction(okAction)
                                                alertController.addAction(cancelAction)

                                         self.present(alertController, animated: true, completion: nil)
                                      }else if selectedMenu == "Terms & Conditions" {
                                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                            if let url = URL(string: "https://www.zero-delay.com/terms_condition"), UIApplication.shared.canOpenURL(url) {
                                             if #available(iOS 10.0, *) {
                                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                             } else {
                                                UIApplication.shared.openURL(url)
                                             }
                                          }
                                      }else if selectedMenu == "Privacy Policy" {
                                        nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                                                  if let url = URL(string: "http://zero-delay.com/privacy-policy"), UIApplication.shared.canOpenURL(url) {
                                                                                   if #available(iOS 10.0, *) {
                                                                                      UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                                                   } else {
                                                                                      UIApplication.shared.openURL(url)
                                                                                   }
                                                                                }
                                      }else if selectedMenu == "Call Us" {
                                        nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                        
                                          var no = ""
                                                                          no = UserDefaults.standard.value(forKey: "contactNo") as? String != nil ? (UserDefaults.standard.value(forKey: "contactNo") as! String) : ("")
                                                                          if let url = URL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url) {
                                                                              if #available(iOS 10, *) {
                                                                                  UIApplication.shared.open(url)
                                                                              } else {
                                                                                  UIApplication.shared.openURL(url)
                                                                              }
                                                                          }
                                      }else if selectedMenu == "Mail Us" {
                                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "mailUsViewController") as! mailUsViewController
                                         //openMailApp()
                                      }else if selectedMenu == "Add Assistant" {
                                           nextView = self.storyboard?.instantiateViewController(withIdentifier: "SubRegisterViewController") as! SubRegisterViewController
                                    }
                                      addChild(nextView)
                                      nextView.view.frame = viewRoot.bounds
                                      viewRoot.addSubview(nextView.view)
                                      nextView.didMove(toParent: self)
                                  
                                      if self.viewMenu.frame.origin.x == 0 {
                                        self.sideMenu()
                                     }
            }else{
                self.view.makeToast("Only main user can access.", duration: 2.0, position: .center)
                if self.viewMenu.frame.origin.x == 0 {
                                                       self.sideMenu()
                                                    }
            }
           
        }else{
            for subview in self.viewRoot.subviews {
                          subview.removeFromSuperview()
                        }
                      var nextView = UIViewController()
                       if selectedMenu == "Home" {
                             nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                       }else if selectedMenu == "Appointments" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                      }else if selectedMenu == "Orders" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                      }else if selectedMenu == "Absent Dates" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AbsentViewController") as! AbsentViewController
                      }else if selectedMenu == "Notifications" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
                      }else if selectedMenu == "My Info" {
                           if docCategory == "DOC" || docCategory == "DIE" ||  docCategory == "PHY" || docCategory == "DEN"{
                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "myInfoViewController") as! myInfoViewController
                           }else if docCategory == "PHY"{
                               nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo4ViewController") as! MyInfo4ViewController
                           }else if docCategory == "DYL"{
                               nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo3ViewController") as! MyInfo3ViewController
                           }else if docCategory == "PHA"{
                               nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo4ViewController") as! MyInfo4ViewController
                           }else if docCategory == "NUR" {
                                              nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo5ViewController") as! MyInfo5ViewController
                           }else {
                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo2ViewController") as! MyInfo2ViewController
                           }
                      }else if selectedMenu == "Settlements" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "SettlementViewController") as! SettlementViewController
                      }else if selectedMenu == "Block Slots" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlockSlotViewController") as! BlockSlotViewController
                      }else if selectedMenu == "Feeds" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
                      }else if selectedMenu == "Change Password" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "ChangepassViewController") as! ChangepassViewController
                      }else if selectedMenu == "Logout" {
                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                          // Create the alert controller
                          let alertController = UIAlertController(title: "", message: "Are you sure want to Logout?", preferredStyle: .alert)

                          let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                                     UIAlertAction in
                                     NSLog("OK Pressed")
                            UserDefaults.standard.setValue(false, forKey: "loginStatus")
                           self.logout()
                                 }
                          let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                                     UIAlertAction in
                                     NSLog("Cancel Pressed")
                                 }

                                 alertController.addAction(okAction)
                                 alertController.addAction(cancelAction)

                          self.present(alertController, animated: true, completion: nil)
                       }else if selectedMenu == "Terms & Conditions" {
                             nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                             if let url = URL(string: "https://www.zero-delay.com/terms_condition"), UIApplication.shared.canOpenURL(url) {
                              if #available(iOS 10.0, *) {
                                 UIApplication.shared.open(url, options: [:], completionHandler: nil)
                              } else {
                                 UIApplication.shared.openURL(url)
                              }
                           }
                       }else if selectedMenu == "Privacy Policy" {
                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                                   if let url = URL(string: "http://zero-delay.com/privacy-policy"), UIApplication.shared.canOpenURL(url) {
                                                                    if #available(iOS 10.0, *) {
                                                                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                                    } else {
                                                                       UIApplication.shared.openURL(url)
                                                                    }
                                                                 }
                       }else if selectedMenu == "Call Us" {
                        nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                              
                            var no = ""
                                 no = UserDefaults.standard.value(forKey: "contactNo") as? String != nil ? (UserDefaults.standard.value(forKey: "contactNo") as! String) : ("")
                                 if let url = URL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url) {
                                     if #available(iOS 10, *) {
                                         UIApplication.shared.open(url)
                                     } else {
                                         UIApplication.shared.openURL(url)
                                     }
                                 }
                       }else if selectedMenu == "Mail Us" {
                           nextView = self.storyboard?.instantiateViewController(withIdentifier: "mailUsViewController") as! mailUsViewController
                          //openMailApp()
                       }else if selectedMenu == "Add Assistant" {
                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "SubRegisterViewController") as! SubRegisterViewController
                     }
                       addChild(nextView)
                       nextView.view.frame = viewRoot.bounds
                       viewRoot.addSubview(nextView.view)
                       nextView.didMove(toParent: self)
                   
                       if self.viewMenu.frame.origin.x == 0 {
                         self.sideMenu()
                      }
            }
        }else{
            for subview in self.viewRoot.subviews {
                          subview.removeFromSuperview()
                        }
                      var nextView = UIViewController()
                       if selectedMenu == "Home" {
                             nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                       }else if selectedMenu == "Appointments" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                      }else if selectedMenu == "Orders" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsViewController") as! AppointmentsViewController
                      }else if selectedMenu == "Absent Dates" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "AbsentViewController") as! AbsentViewController
                      }else if selectedMenu == "Notifications" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
                      }else if selectedMenu == "My Info" {
                           if docCategory == "DOC" || docCategory == "DIE" || docCategory == "PHY" || docCategory == "DEN"{
                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "myInfoViewController") as! myInfoViewController
                           }else if docCategory == "DYL"{
                               nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo3ViewController") as! MyInfo3ViewController
                           }else if docCategory == "PHA"{
                               nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo4ViewController") as! MyInfo4ViewController
                           }else if docCategory == "NUR" {
                                              nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo5ViewController") as! MyInfo5ViewController
                           }else {
                                nextView = self.storyboard?.instantiateViewController(withIdentifier: "MyInfo2ViewController") as! MyInfo2ViewController
                           }
                      }else if selectedMenu == "Settlements" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "SettlementViewController") as! SettlementViewController
                      }else if selectedMenu == "Block Slots" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlockSlotViewController") as! BlockSlotViewController
                      }else if selectedMenu == "Feeds" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "BlogViewController") as! BlogViewController
                      }else if selectedMenu == "Change Password" {
                          nextView = self.storyboard?.instantiateViewController(withIdentifier: "ChangepassViewController") as! ChangepassViewController
                      }else if selectedMenu == "Logout" {
                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                          // Create the alert controller
                          let alertController = UIAlertController(title: "", message: "Are you sure want to Logout?", preferredStyle: .alert)

                          let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                                     UIAlertAction in
                                     NSLog("OK Pressed")
                            UserDefaults.standard.setValue(false, forKey: "loginStatus")
                           self.logout()
                                 }
                          let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                                     UIAlertAction in
                                     NSLog("Cancel Pressed")
                                 }

                                 alertController.addAction(okAction)
                                 alertController.addAction(cancelAction)

                          self.present(alertController, animated: true, completion: nil)
                       }else if selectedMenu == "Terms & Conditions" {
                             nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                             if let url = URL(string: "https://www.zero-delay.com/terms_condition"), UIApplication.shared.canOpenURL(url) {
                              if #available(iOS 10.0, *) {
                                 UIApplication.shared.open(url, options: [:], completionHandler: nil)
                              } else {
                                 UIApplication.shared.openURL(url)
                              }
                           }
                       }else if selectedMenu == "Privacy Policy" {
                         nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                                   if let url = URL(string: "http://zero-delay.com/privacy-policy"), UIApplication.shared.canOpenURL(url) {
                                                                    if #available(iOS 10.0, *) {
                                                                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                                    } else {
                                                                       UIApplication.shared.openURL(url)
                                                                    }
                                                                 }
                       }else if selectedMenu == "Call Us" {
                        nextView = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                              
                           var no = ""
                                                           no = UserDefaults.standard.value(forKey: "contactNo") as? String != nil ? (UserDefaults.standard.value(forKey: "contactNo") as! String) : ("")
                                                           if let url = URL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url) {
                                                               if #available(iOS 10, *) {
                                                                   UIApplication.shared.open(url)
                                                               } else {
                                                                   UIApplication.shared.openURL(url)
                                                               }
                                                           }
                       }else if selectedMenu == "Mail Us" {
                           nextView = self.storyboard?.instantiateViewController(withIdentifier: "mailUsViewController") as! mailUsViewController
                          //openMailApp()
                       }else if selectedMenu == "Add Assistant" {
                            nextView = self.storyboard?.instantiateViewController(withIdentifier: "SubRegisterViewController") as! SubRegisterViewController
                     }
                       addChild(nextView)
                       nextView.view.frame = viewRoot.bounds
                       viewRoot.addSubview(nextView.view)
                       nextView.didMove(toParent: self)
                   
                       if self.viewMenu.frame.origin.x == 0 {
                         self.sideMenu()
                      }
        }
        
        
        
           
        
    }
    
    @objc func updateprofile() {
        tblView.reloadData()
    }
        func settingsAPICall() {
          if Reachability.isConnectedToNetwork() {
           
            let strURL = settings
               let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                               "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true"{
                                    let veriStatus = allServiceDict.value(forKey: "verifiedstatus") as! String
                                    self.comments = allServiceDict.value(forKey: "comments") as! String
                                    UserDefaults.standard.setValue((allServiceDict.value(forKey: "bannertiming") as! NSString).integerValue, forKey: "bannerTiming")
                                    
                                    
                                    if veriStatus == "E" {
                                        self.verifyImg.tintColor = UIColor.red
                                        self.btnInfo.isHidden = false
                                        UserDefaults.standard.setValue(self.comments, forKey: "comments")
                                     }else{
                                        self.btnInfo.isHidden = true
                                        UserDefaults.standard.setValue("", forKey: "comments")
                                     }
                                    }else{
                                      
                                    }
                               DispatchQueue.main.async {
                                 //  KVSpinnerView.dismiss()
                               }
                            case .failure(let error):
                                print(error)
                             //   self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
    //                           DispatchQueue.main.async {
    //                                KVSpinnerView.dismiss()
    //                            }
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
    
    func openMailApp() {

        let recipients = "someone@gmail.com"
        let url = NSURL(string: "mailto:\(recipients)")
        UIApplication.shared.open(url! as URL)
    }
}




    
