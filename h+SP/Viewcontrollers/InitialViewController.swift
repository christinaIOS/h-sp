//
//  InitialViewController.swift
//  h+SP
//
//  Created by mirrorminds on 19/07/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    

    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "loginStatus") as? Bool != nil {
                 if UserDefaults.standard.value(forKey: "loginStatus") as! Bool == true {
                     
                     if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                        if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                             UserDefaults.standard.setValue("", forKey: "profileImg")
                             UserDefaults.standard.setValue("", forKey: "name")
                             UserDefaults.standard.setValue("", forKey: "mobile")
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            next.modalPresentationStyle = .fullScreen
                            next.selectedMenu = "My Info"
                            self.present(next, animated: true, completion: nil)
                         }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "C" {
                             UserDefaults.standard.setValue("", forKey: "profileImg")
                             UserDefaults.standard.setValue("", forKey: "name")
                             UserDefaults.standard.setValue("", forKey: "mobile")
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            next.modalPresentationStyle = .fullScreen
                            next.selectedMenu = "My Info"
                            self.present(next, animated: true, completion: nil)
                         } else{
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                             next.modalPresentationStyle = .fullScreen
                             next.selectedMenu = "Home"
                             self.present(next, animated: true, completion: nil)
                         }
                     }else{
                        let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                         next.modalPresentationStyle = .fullScreen
                         next.selectedMenu = "Home"
                         self.present(next, animated: true, completion: nil)
                     }
                 }else{
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    next.modalPresentationStyle = .fullScreen
                                    self.present(next, animated: true, completion: nil)
               }
             }else{
                 let next = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                 next.modalPresentationStyle = .fullScreen
                 self.present(next, animated: true, completion: nil)
             }
    }
    


}
