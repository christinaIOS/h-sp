//
//  LoginViewController.swift
//  h+SP
//
//  Created by mirrorminds on 18/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//
import UIKit
import Alamofire
import KVSpinnerView

class whoCell: UITableViewCell {
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var imgBtn: UIButton!
 //   @IBOutlet weak var btn: UIButton!
     @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgBtn: UIButton!
}

@available(iOS 11.0, *)
@available(iOS 11.0, *)
@available(iOS 11.0, *)
class LoginViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var mobileTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var mailTf: UITextField!
     @IBOutlet weak var regHideView: UIView!
    @IBOutlet weak var centeView: UIView!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var whoTf: UITextField!
 
    @IBOutlet weak var whoView: UIView!
    @IBOutlet weak var downView: UIView!
   var docCategory = ""
    
//    var whoArray = ["Doctor","Dentist","Physiotherapist","Nurse","Dietitian","Dialysis Centre","Ambulance","Mortuary","Pharmacy"]
//    var duArray = ["Doctor","Dentist","Physiotherapist","Nurse","Dietician","Dialysis Centre","Ambulance","Mortuary","Pharmacy"]
    
   // var duArray1 = ["DOC","DEN","PHY","NUR","DIE","DYL","AMB","MOR","PHA"]
     var duArray1 = ["DOC","DEN","PHY","NUR","DIE","DYL","PHA"]
    var whoArray = ["Doctor","Dentist","Physiotherapist","Nurse","Dietitian","Dialysis Centre","Pharmacy"]
       var duArray = ["Doctor","Dentist","Physiotherapist","Nurse","Dietician","Dialysis Centre","Pharmacy"]
    
    var strReply:String = ""
    var intReply:Int = 0
    var selectedIndex = ""
    var who = ""
   // @available(iOS 11.0, *)
    
//    @available(iOS 11.0, *)
    override func viewDidLoad() {
        super.viewDidLoad()
       viewSetu()
        
    }

    @available(iOS 11.0, *)
    func viewSetu(){
        //loader
        settingsAPICall()
        centeView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60))
        
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
        KVSpinnerView.settings.statusTextColor = UIColor.darkGray
        KVSpinnerView.settings.tintColor = UIColor.darkGray
        
        Constant.addStatusBar(view: self.view)
        mobileTf.attributedPlaceholder = NSAttributedString(string: "Mobile Number",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        passwordTf.attributedPlaceholder = NSAttributedString(string: "Password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        whoView.layer.shadowColor = UIColor.lightGray.cgColor
              whoView.layer.shadowOpacity = 1
              whoView.layer.shadowOffset = CGSize.zero
              whoView.layer.shadowRadius = 2
              whoView.layer.cornerRadius = 2
              
              downView.layer.shadowColor = UIColor.lightGray.cgColor
              downView.layer.shadowOpacity = 1
              downView.layer.shadowOffset = CGSize.zero
              downView.layer.shadowRadius = 2
              downView.layer.cornerRadius = 2
                
       
             
          //    centeView.roundCorners(corners: [.topLeft], radius: 60)
        
        if UserDefaults.standard.value(forKey: "docCat") as? String != nil {
            docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
    }
    
//    //MARK:- tf delegates
//       @objc func searchPredicate(textField: UITextField) {
//           if textField.text?.count == 0 {
//               self.duArray = self.whoArray
//               intReply = self.duArray.count
//               strReply = ""
//               tblview.frame.size.height = 280
//               downView.frame.size.height = 280
//               tblview.reloadData()
//               return
//           }
//           else {
//              let searchString = "\(whoTf.text!)"
//             //  duArray = whoArray.filter({ $0.hasPrefix(searchString.firstCapitalized) })
//             duArray = whoArray.filter { $0.contains(searchString) }
//               print(duArray)
//               tblview.frame.size.height = CGFloat(duArray.count * 45)
//               downView.frame.size.height = CGFloat(duArray.count * 45)
//               tblview.reloadData()
//           }
//       }
    @objc func keyboardWillShow(notification: NSNotification) {
          if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
              if self.view.frame.origin.y == 0{
                  self.view.frame.origin.y -= keyboardSize.height/1.5
              }
          }
      }
      
      @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
              if self.view.frame.origin.y != 0{
                  self.view.frame.origin.y = 0
              }
          }
      }
   
          
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //MARK:- btn actions
    @available(iOS 11.0, *)
    @IBAction func submitAction(_ sender: Any) {
        self.view.endEditing(true)
        validation()
    }
    @IBAction func registerAction(_ sender: Any) {

        self.regHideView.isHidden = false
    }
    @IBAction func forgotAction(_ sender: Any) {
        hideView.isHidden = false
    }
    @IBAction func hideBtnAction(_ sender: Any) {
        hideView.isHidden = true
    }
    @IBAction func forgotSubmitAction(_ sender: Any) {
        if mailTf.text != "" {
            if mailTf.text?.count == 10 {
                forgotAPICall()
            }else{
                self.view.makeToast("Please check your mobile number", duration: 2.0, position: .center)
            }
        }else{
            self.view.makeToast("Please enter your mobile number", duration: 2.0, position: .center)
        }
    }
    @IBAction func closeAction(_ sender: Any) {
             regHideView.isHidden = true
        }
  
      @IBAction func regSubmitAction(_ sender: Any) {
          if who != "" {
              let controller = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
              controller.modalPresentationStyle = .fullScreen
              controller.modalTransitionStyle = .coverVertical
//              controller.docType = who
              present(controller, animated: true, completion: nil)
          }else {
              self.view.makeToast("Please select who are you ..", duration: 2.0, position: .center)
          }
      }
    
    //MARK:- validation
    @available(iOS 11.0, *)
    func validation(){
        if mobileTf.text != "" {
            if passwordTf.text != "" {
              
                   loginAPICall()
              
            }else{
                self.view.makeToast("Please enter your password", duration: 2.0, position: .bottom)
            }
        }else{
            self.view.makeToast("Please enter your mobile number", duration: 2.0, position: .bottom)
        }
    }
    
    //MARK:- textfield delegate
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          textField.resignFirstResponder()
          return true
      }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == whoTf {
            return false
        }
           if textField == mailTf {
               let maxLength = 10
               let currentString: NSString = mailTf.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
        if textField == mobileTf {
            let maxLength = 10
            let currentString: NSString = mobileTf.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
           return true
       }
    
    
      // MARK: - tblView delegates
      func numberOfSections(in tableView: UITableView) -> Int {
          return 1
      }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return duArray.count
      }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60)
      }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! whoCell
          cell.lbl.text = duArray[indexPath.row]
          if selectedIndex == "\(indexPath.row)" {
            //  cell.btn.isHidden = false
              cell.bgView.backgroundColor = UIColor(red: 249/255, green: 232/255, blue: 0/255, alpha: 1.0)
          }else{
              cell.bgView.backgroundColor = UIColor.white
          }
      
        cell.imgBtn.isHidden = true
        cell.bgBtn.tag = indexPath.row
        cell.bgBtn.addTarget(self, action: #selector(didSelect(sender:)), for: .touchUpInside)
          return cell
      }

    @objc func didSelect(sender:UIButton){
        selectedIndex = "\(sender.tag)"
      //  whoTf.text = duArray[sender.tag]
        who = whoArray[sender.tag]
        docCategory = duArray1[sender.tag]
        UserDefaults.standard.setValue(docCategory, forKey: "docCat")
        tblview.reloadData()
    }
    
    @available(iOS 11.0, *)
    
    //MARK :- AI CALL
    @available(iOS 11.0, *)
    @available(iOS 11.0, *)
    
    
    func settingsAPICall() {
      if Reachability.isConnectedToNetwork() {
       
        let strURL = settings
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
                 print(strURL)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true"{
                                UserDefaults.standard.setValue(allServiceDict.value(forKey: "supportno") as! String, forKey: "contactNo")
                                }else{
                                  
                                }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
//                           DispatchQueue.main.async {
//                                KVSpinnerView.dismiss()
//                            }
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    
    func loginAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = loginURL
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
                 print(strURL)
              let params: [String: Any] = [
                "mobile" : "\(mobileTf.text!)",
                "password" : "\(passwordTf.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")
                ]
        print(params)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true"{
                               self.saveUserData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                UserDefaults.standard.setValue(allServiceDict.value(forKey: "assistant") as! String, forKey: "assistLogin")
                                let userDict = allServiceDict.value(forKey: "user") as! NSDictionary
                                if userDict.value(forKey: "otp_verify") as! String == "N" {
                                    if allServiceDict.value(forKey: "user") as? NSDictionary != nil {
                                                                       self.saveUserData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                                                   }
                                      let next = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                                    next.otStr = ((allServiceDict.value(forKey: "user") as! NSDictionary).value(forKey: "otp") as! NSNumber).stringValue
                                      next.modalPresentationStyle = .fullScreen
                                       self.present(next, animated: true, completion: nil)
                                }else{
                                    UserDefaults.standard.setValue(true, forKey: "loginStatus")
                                    let next =  self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                     next.modalPresentationStyle = .fullScreen
                                     next.name = userDict.value(forKey: "name") as! String
                                      next.name = userDict.value(forKey: "mobile_no") as! String
                                     if allServiceDict.value(forKey: "profile") as? String != nil {
                                        UserDefaults.standard.setValue(allServiceDict.value(forKey: "profile") as! String, forKey: "profileEdit")
                                         if allServiceDict.value(forKey: "profile") as! String == "N" {
                                                 next.selectedMenu = "My Info"
                                              }else {
                                                 next.selectedMenu = "Home"
                                           }
                                     }else{
                                        next.selectedMenu = "Home"
                                     }
                                    self.present(next, animated: true, completion: nil)
                                }
                                
                            }else if allServiceDict.value(forKey: "Message") as? String == "INVALID LOGIN CREDENTIALS"{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }else if allServiceDict.value(forKey: "Message") as? String == "USER BLOCKED"{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                           

                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                           DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func forgotAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = forgotURL
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
                 print(strURL)
              let params: [String: Any] = [
                             "mobile" : "\(mailTf.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")
                             ]
        print(strURL)
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true"{
                                  self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.hideView.isHidden = true
                            }else{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }

                          DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    @available(iOS 11.0, *)
    private func saveUserData(dict:NSDictionary) {
      do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
           UserDefaults.standard.setValue(data, forKey: "userDict")
            UserDefaults.standard.setValue(dict.value(forKey: "token") as! String, forKey: "token")
         UserDefaults.standard.setValue(dict.value(forKey: "user_type") as! String, forKey: "docCat")
        } catch {
           
        }
    }
    
}


    
extension UIView {

    @available(iOS 11.0, *)
    func roundCorners(corners:CACornerMask, radius: CGFloat) {
      self.layer.cornerRadius = radius
      self.layer.maskedCorners = corners
   }
}
