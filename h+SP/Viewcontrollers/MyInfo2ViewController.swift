//
//  MyInfo2ViewController.swift
//  h+SP
//
//  Created by mirrorminds on 14/02/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//



    import UIKit
    import DatePickerDialog
    import DropDown
    import Alamofire
    import KVSpinnerView
    import AlamofireImage
    import GooglePlaces

    class MyInfo2ViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate {
        
        @IBOutlet weak var tblView: UITableView!
        @IBOutlet weak var scrollView: UIScrollView!
        @IBOutlet weak var backBtn: UIButton!
        @IBOutlet weak var MenuBtn: UIButton!
        @IBOutlet weak var genralBtn: UIButton!
        
        //genral
        @IBOutlet weak var regnoLbl: UILabel!
        @IBOutlet weak var mailTf: UITextField!
        @IBOutlet weak var nametf: MyTextField!
        @IBOutlet weak var propertynameTf: MyTextField!
        @IBOutlet weak var mobilenoTf: MyTextField!
        @IBOutlet weak var addressTf: MyTextField!
        @IBOutlet weak var degreeTf: MyTextField!
        @IBOutlet weak var timeTf: MyTextField!
        @IBOutlet weak var closeTimeTf: MyTextField!
        @IBOutlet weak var alternateTf: MyTextField!
        @IBOutlet weak var aadharNoTf: MyTextField!
        @IBOutlet weak var priceperKMTf: MyTextField!
        @IBOutlet weak var priceperKMViewHeight : NSLayoutConstraint!
        @IBOutlet weak var priceperKMTFHeight : NSLayoutConstraint!
        @IBOutlet weak var priceperKMTf1: MyTextField!
        @IBOutlet weak var priceperKMTf2: MyTextField!
        @IBOutlet weak var licenseTf: MyTextField!
        @IBOutlet weak var userImg: UIImageView!
        @IBOutlet weak var degreeImg: UIImageView!
        @IBOutlet weak var aadharImgView: UIImageView!
        @IBOutlet weak var regCerImgView: UIImageView!
        @IBOutlet weak var homedelImg: UIImageView!
        @IBOutlet weak var pickupImg: UIImageView!
        @IBOutlet weak var priceLbl: UILabel!
        @IBOutlet weak var pharmacistTf: UITextField!
        @IBOutlet weak var degCertView: UIView!
        @IBOutlet weak var degCertHeightCons: NSLayoutConstraint!
        @IBOutlet weak var homedeliGeightCons: NSLayoutConstraint!
        @IBOutlet weak var pickupHeightCons: NSLayoutConstraint!
        @IBOutlet weak var degreeHeightCons: NSLayoutConstraint!
        @IBOutlet weak var propertyTopCons: NSLayoutConstraint!
        @IBOutlet weak var attachTopCons: NSLayoutConstraint!
        @IBOutlet weak var licenseHeightCons: NSLayoutConstraint!
        @IBOutlet weak var dlyfeeTf: UITextField!
        @IBOutlet weak var dlyDurTf: UITextField!
        @IBOutlet weak var dlyfeeTfHeightCons: NSLayoutConstraint!
        @IBOutlet weak var dlyDurTfHeightcons: NSLayoutConstraint!
        @IBOutlet weak var startTimeTfHeightCons: NSLayoutConstraint!
               @IBOutlet weak var endTimeTfHeightcons: NSLayoutConstraint!
       
        @IBOutlet weak var addImgLbl: UILabel!
        
        //BANKadd
        @IBOutlet weak var bankNameTf: UITextField!
        @IBOutlet weak var accholdNametf: UITextField!
        @IBOutlet weak var acctyeTf: UITextField!
        @IBOutlet weak var accnoTf: UITextField!
        @IBOutlet weak var brancNameTf: UITextField!
        @IBOutlet weak var ifscTf: UITextField!
        
       var imagePicker = UIImagePickerController()
        
       var fromView = ""
    
       var imgRef = 0
       var ambType = ""
        var morType = ""
        var pickupBOOL = false
        var deliveryBool = false
        var profilepicBool = false
        var aadharImgBOOL = false
        var regCerImgBOOL = false
        var degCerImgBOOL = false
        
        //FOR DEGREE
        let acctyeDD = DropDown()
        let ugDD = DropDown()
        let AmbTypeDD = DropDown()
        let MORDD = DropDown()
        let pgSpeacilityDD = DropDown()
        let masterSpeacilityDD = DropDown()
        
        var ugArr = [NSDictionary]()
       
        var ugID = ""
        var lat = ""
        var long = ""
        
        var docCategory = ""
        var reisterImg : UIImage!
        var aadharImg : UIImage!
        var degImg : UIImage!
        
        var level1 = ""
        var level2 = ""
        var adminVerify = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            if UserDefaults.standard.value(forKey: "docCat") as? String != nil {
                docCategory = UserDefaults.standard.value(forKey: "docCat") as! String
            }
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
                 NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
          view.addGestureRecognizer(tap)
          viewSetU()
          ProfileAPICall()
            
        }
        @objc func dismissKeyboard() {
            //Causes the view (or one of its embedded text fields) to resign the first responder status.
            view.endEditing(true)
        }
        func viewSetU(){
            
            if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                      if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                        self.view.makeToast("Please update your profile.", duration: 1.0, position: .center)
                        alternateTf.text = ""
                       addImgLbl.isHidden = false
                      }else{
                        alternateTf.text = "6754895787"
                       addImgLbl.isHidden = true
                      }
                   }
            
              self.aadharNoTf.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
            
            if fromView == "dash" {
                  MenuBtn.isHidden = true
                  backBtn.isHidden = false
                              
            }else {
                   MenuBtn.isHidden = false
                   backBtn.isHidden = true
                    gestur()
            }
            
            
            if docCategory == "PHA" {
                pharmacistTf.placeholder = "Pharmacist Name"
                propertynameTf.placeholder = "Propertier Name"
                timeTf.placeholder = "Open Time"
                closeTimeTf.placeholder = "Close Time"
                degreeHeightCons.constant = 45
                degreeTf.placeholder = "Degree"
                homedeliGeightCons.constant = 35
                homedelImg.superview?.isHidden = false
                pickupHeightCons.constant = 35
                pickupImg.superview?.isHidden = false
                priceperKMTf.placeholder = "< 100"
                priceperKMTf1.isHidden = false
                priceperKMTf2.isHidden = false
                priceperKMViewHeight.constant = 100
                priceperKMTFHeight.constant = 45
                degCertHeightCons.constant = 50
                degCertView.isHidden = false
                attachTopCons.constant = 205
                priceLbl.isHidden = false
                licenseHeightCons.constant = 0
                dlyfeeTfHeightCons.constant = 0
                dlyDurTfHeightcons.constant = 0
                startTimeTfHeightCons.constant = 45
                endTimeTfHeightcons.constant = 45
            }else if docCategory == "MOR" {
                pharmacistTf.placeholder = "Vehicle Number"
                propertynameTf.placeholder = "contact person name"
                degreeTf.placeholder = "Mortuary Type"
                timeTf.placeholder = "Start Time"
                closeTimeTf.placeholder = "End Time"
                degreeHeightCons.constant = 0
                homedeliGeightCons.constant = 0
                homedelImg.superview?.isHidden = true
                pickupHeightCons.constant = 0
                pickupImg.superview?.isHidden = true
                priceperKMTf.placeholder = "price per km for with freezer"
                priceperKMTf1.placeholder = "price per km for without freezer"
                priceperKMTf1.isHidden = false
                priceperKMTf2.isHidden = true
                degCertHeightCons.constant = 0
                degCertView.isHidden = true
                priceperKMViewHeight.constant = 50
                priceperKMTFHeight.constant = 45
                attachTopCons.constant = 160
                priceLbl.isHidden = false
                licenseHeightCons.constant = 0
                dlyfeeTfHeightCons.constant = 0
                dlyDurTfHeightcons.constant = 0
                startTimeTfHeightCons.constant = 0
                endTimeTfHeightcons.constant = 0
            }else if docCategory == "AMB" {
                pharmacistTf.placeholder = "Vehicle Number"
                degreeTf.placeholder = "Ambulance Type"
                timeTf.placeholder = "Start Time"
                closeTimeTf.placeholder = "End Time"
                priceperKMTf.placeholder = "price per km for with ICU"
                priceperKMTf1.placeholder = "price per km for without ICU"
                propertynameTf.placeholder = "contact person name"
                degreeHeightCons.constant = 0
                homedeliGeightCons.constant = 0
                homedelImg.superview?.isHidden = true
                pickupHeightCons.constant = 0
                pickupImg.superview?.isHidden = true
                priceperKMTf1.isHidden = false
                priceperKMTf2.isHidden = true
                degCertHeightCons.constant = 0
                degCertView.isHidden = true
                priceperKMViewHeight.constant = 50
                priceperKMTFHeight.constant = 45
                attachTopCons.constant = 160
                priceLbl.isHidden = false
                licenseHeightCons.constant = 0
                dlyfeeTfHeightCons.constant = 0
                dlyDurTfHeightcons.constant = 0
                startTimeTfHeightCons.constant = 45
                endTimeTfHeightcons.constant = 45
            }else if docCategory == "DYL" {
                pharmacistTf.placeholder = "Dialysis director Name"
                propertynameTf.placeholder = "contact person name"
                timeTf.placeholder = "Open Time"
                closeTimeTf.placeholder = "Close Time"
                priceperKMTf.isHidden = true
                homedeliGeightCons.constant = 0
                degreeHeightCons.constant = 0
                homedelImg.superview?.isHidden = true
                pickupHeightCons.constant = 0
                pickupImg.superview?.isHidden = true
                priceperKMTf1.isHidden = true
                priceperKMTf2.isHidden = true
                degCertHeightCons.constant = 0
                degCertView.isHidden = true
                priceperKMViewHeight.constant = 0
               // priceperKMTFHeight.constant = 45
                attachTopCons.constant = 10
                priceLbl.isHidden = true
                licenseHeightCons.constant = 45
                dlyfeeTfHeightCons.constant = 45
                dlyDurTfHeightcons.constant = 45
                startTimeTfHeightCons.constant = 45
                endTimeTfHeightcons.constant = 45
            }
            
            imagePicker.delegate = self
            docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
            
            //loader
                   KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
                   KVSpinnerView.settings.statusTextColor = UIColor.darkGray
                   KVSpinnerView.settings.tintColor = UIColor.darkGray
            
            Constant.addStatusBar(view: self.view)
                   if fromView == "dash" {
                              MenuBtn.isHidden = true
                              backBtn.isHidden = false
                       
                    }else {
                              MenuBtn.isHidden = false
                             backBtn.isHidden = true
                       gestur()
                   }
                   
           scrollView.isHidden = false
         
     }
        
         func gestur(){
            let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                         left.direction = .left
                         self.view.addGestureRecognizer(left)
                 
                         let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                         right.direction = .right
                         self.view.addGestureRecognizer(right)
         }
                 
         @objc func Swipe(){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
          }
        
        func resizeImage(image: UIImage) -> UIImage {
              
              var newHeight = 1024 as CGFloat
              var newWidth = 1024 as CGFloat
              
              if image.size.width > image.size.height {
                  let scale = newWidth / image.size.width
                  newHeight = image.size.height * scale
              }
              else if image.size.width < image.size.height {
                  let scale = newHeight / image.size.height
                  newWidth = image.size.width * scale
              }
              
              UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
              image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
              let newImage = UIGraphicsGetImageFromCurrentImageContext()
              UIGraphicsEndImageContext()
              print(newImage!)
              return newImage!
          }
        
       //MARK:- setting droDowns
        func setupAccTyeDropDown() {
            let accTye = ["SAVINGS","CURRENT"]
            acctyeDD.anchorView = acctyeTf
            acctyeDD.topOffset = CGPoint(x: 0, y: acctyeTf.bounds.height)
            let joinedString = accTye
            acctyeDD.dataSource = joinedString
            acctyeDD.selectionAction = { [unowned self] (index, item) in
                self.acctyeTf.text = item
            }
            acctyeDD.show()
        }
        func setupDegreeDropDown() {
             var ugStrArr = [String]()
               for i in 0..<ugArr.count {
                   let dict = ugArr[i]
                   ugStrArr.append(dict.value(forKey: "deg_degree") as! String)
               }
                ugDD.anchorView = degreeTf
                ugDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                let joinedString = ugStrArr
                ugDD.dataSource = joinedString
                ugDD.selectionAction = { [unowned self] (index, item) in
                    self.degreeTf.text = item
                    self.ugID = "\(self.ugArr[index].value(forKey: "deg_id") as! Int)"
                }
                ugDD.show()
            }
        
          func setupAMBDropDown() {
              let AMBArr = ["ICU SUPPORT", "NORMAL"]
                      AmbTypeDD.anchorView = degreeTf
                      AmbTypeDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                      let joinedString = AMBArr
                      AmbTypeDD.dataSource = joinedString
                      AmbTypeDD.selectionAction = { [unowned self] (index, item) in
                          self.degreeTf.text = item
                          self.ambType = AMBArr[index]
                      }
                      AmbTypeDD.show()
                  }
        
        func setupMORDropDown() {
            let MORArr = ["WITHFREEZER","WITHOUTFREEZER"]
                    MORDD.anchorView = degreeTf
                    MORDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                    let joinedString = MORArr
                    MORDD.dataSource = joinedString
                    MORDD.selectionAction = { [unowned self] (index, item) in
                        self.degreeTf.text = item
                        self.morType = MORArr[index]
                    }
                    MORDD.show()
                }
        
        // MARK: - tf delegates
        
        
        @objc func didChangeText(textField:UITextField) {
                textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
            }
            func modifyCreditCardString(creditCardString : String) -> String {
                let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()

                let arrOfCharacters = Array(trimmedString)
                var modifiedCreditCardString = ""

                if(arrOfCharacters.count > 0) {
                    for i in 0...arrOfCharacters.count-1 {
                        modifiedCreditCardString.append(arrOfCharacters[i])
                        if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                            modifiedCreditCardString.append(" ")
                        }
                    }
                }
                return modifiedCreditCardString
            }
            func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                if textField == aadharNoTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                            if(textField == aadharNoTf) {
                                return newLength <= 14
                            }
                }
                if textField == priceperKMTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                            if(textField == priceperKMTf) {
                                return newLength <= 4
                            }
                }
                 if textField == priceperKMTf1 {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                            if(textField == priceperKMTf1) {
                                return newLength <= 4
                            }
                }
                 if textField == priceperKMTf2 {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                            if(textField == priceperKMTf2) {
                                return newLength <= 4
                            }
                }
                if textField == dlyDurTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == dlyDurTf) {
                        return newLength <= 2
                    }
                }
                if textField == dlyfeeTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == dlyfeeTf) {
                        return newLength <= 6
                    }
                }
               if textField == alternateTf {
                   let newLength = (textField.text ?? "").count + string.count - range.length
                   if(textField == alternateTf) {
                       return newLength <= 10
                   }
               }
                 return true
            }
        
       
        @objc func keyboardWillHide(notification: Notification) {
            let contentInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
        }

        @objc func keyboardWillShow(notification: Notification) {
            guard let keyboardFrame: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
            
                    if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                        case 1136:
                            scrollView.contentInset.bottom = keyboardFrame.height
                        case 1334:
                            scrollView.contentInset.bottom = keyboardFrame.height
                        case 1920, 2208:
                            print("iPhone 6+/6S+/7+/8+")
                          scrollView.contentInset.bottom = keyboardFrame.height
                        case 2436:
                            print("iPhone X/XS/11 Pro")
                           scrollView.contentInset.bottom = keyboardFrame.height + 78
                        case 2688:
                            print("iPhone XS Max/11 Pro Max")
                        scrollView.contentInset.bottom = keyboardFrame.height + 80
                        case 1792:
                            print("iPhone XR/ 11 ")
                        scrollView.contentInset.bottom = keyboardFrame.height + 80
                        default:
                            print("Unknown")
                        }
                    }else{
                         scrollView.contentInset.bottom = keyboardFrame.height + 200
                 }
        }
        
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//            if textField == alternateTf {
//               if self.timeTf.text != "" &&  self.closeTimeTf.text != "" {
//                let fromTime = Int((self.timeTf.text)!.components(separatedBy: ":").first!)
//                let toTime = Int((self.closeTimeTf.text)!.components(separatedBy: ":").first!)
//                if fromTime > toTime {
//                       self.view.makeToast("Check your close time", duration: 2.0, position: .center)
//                    }
//                }
//                return true
//            }
            if  textField == acctyeTf {
                setupAccTyeDropDown()
                return false
            }
           if textField == degreeTf {
                if docCategory == "AMB" {
                    setupAMBDropDown()
                   return false
                }else if docCategory == "MOR" {
                    setupMORDropDown()
                   return false
                }else if docCategory == "PHA" {
                    setupDegreeDropDown()
                    return false
                }
           }
            if textField == timeTf {
                let currentDate = Date()
                      var dateComponents = DateComponents()
                      dateComponents.month = 12
                    let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
                      
                      DatePickerDialog().show("Time", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .time) { (date) in
                          if let dt = date {
                              let formatter = DateFormatter()
                              formatter.dateFormat = "HH:mm"
                              self.timeTf.text = formatter.string(from: dt)
                          }
                      }
              return false
           }
            if textField == closeTimeTf {
              let currentDate = Date()
                var dateComponents = DateComponents()
                dateComponents.month = 12
              let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
                
                DatePickerDialog().show("Time", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .time) { (date) in
                    if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "HH:mm"
                        self.closeTimeTf.text = formatter.string(from: dt)
                    }
                }
                return false
            }
            if textField == addressTf {
                let autocompleteController = GMSAutocompleteViewController()
                autocompleteController.delegate = self
                autocompleteController.modalPresentationStyle = .fullScreen
                self.present(autocompleteController, animated: true, completion: nil)
                return false
            }
            return true
         }
            
            func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                textField.resignFirstResponder()
                return true
            }
            
        // MARK: - btnActions
         @IBAction func menuAction(_ sender: UIButton) {
             self.view.endEditing(true)
//                 if sender.tag == 0 {
//
//                   }else if sender.tag == 1 {
//                       self.dismiss(animated: true, completion: nil)
//                   }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }
        
        @IBAction func nextAction(_ sender: UIButton) {
            infoValidation()
        }
        
     
        
       @IBAction func tabAction(_ sender: UIButton) {
        
         }
        
       @IBAction func serviceSelectAction(_ sender: UIButton) {
              if sender.tag == 0 {
                 if pickupBOOL == false {
                   pickupImg.image = UIImage(named: "checkBox")
                   pickupBOOL = true
                 }else{
                   pickupBOOL = false
                   pickupImg.image = UIImage(named: "unCheck")
                 }
               }else if sender.tag == 1 {
                   if deliveryBool == false {
                      homedelImg.image = UIImage(named: "checkBox")
                      deliveryBool = true
                    priceperKMTf.isHidden = false
                    priceperKMTf1.isHidden = false
                    priceperKMTf2.isHidden = false
                    priceperKMViewHeight.constant = 0
                    priceperKMViewHeight.constant = 100
                    priceperKMTFHeight.constant = 45
                    propertyTopCons.constant = 65
                    attachTopCons.constant = 255
                    priceLbl.isHidden = false

                    }else{
                      deliveryBool = false
                      homedelImg.image = UIImage(named: "unCheck")
                       priceperKMTf.isHidden = true
                       priceperKMTf1.isHidden = true
                       priceperKMTf2.isHidden = true
                       priceperKMViewHeight.constant = 0
                       priceperKMTFHeight.constant = 45
                       propertyTopCons.constant = 10
                       attachTopCons.constant = 60
                       priceLbl.isHidden = true

                    }
              }
           }
       
        @IBAction func editprofileAction(_ sender: UIButton) {
            imgRef = sender.tag
            
               if sender.tag == 0 {
                   if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                        {
                            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                            imagePicker.allowsEditing = true
                            imagePicker.cameraDevice = .front
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                        else
                        {
                            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else{
                         let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                                           let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
                                           {
                                               UIAlertAction in
                                               self.openCamera()
                                               
                                           }
                                           let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
                                           {
                                               UIAlertAction in
                                               self.openGallary()
                                           }
                                           let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                                           {
                                               UIAlertAction in
                                           }
                                           
                                           // Add the actions
                                           imagePicker.delegate = self
                                           alert.addAction(cameraAction)
                                           alert.addAction(gallaryAction)
                                           alert.addAction(cancelAction)
                                           // Present the controller
                                       
                                       if UIDevice.current.userInterfaceIdiom == .phone {
                                            self.present(alert, animated: true, completion: nil)
                                       }else{
                                           if let popoverController = alert.popoverPresentationController {
                                               popoverController.sourceView = sender
                                               popoverController.sourceRect = sender.bounds
                                           }
                                           self.present(alert, animated: true, completion: nil)
                                       }
                                       
                    }
                }else {
                    let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                    let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
                    {
                        UIAlertAction in
                        self.openCamera()
                        
                    }
                    let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
                    {
                        UIAlertAction in
                        self.openGallary()
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                    {
                        UIAlertAction in
                    }
                    
                    // Add the actions
                    imagePicker.delegate = self
                    alert.addAction(cameraAction)
                    alert.addAction(gallaryAction)
                    alert.addAction(cancelAction)
                    // Present the controller
                
                if UIDevice.current.userInterfaceIdiom == .phone {
                     self.present(alert, animated: true, completion: nil)
                }else{
                    if let popoverController = alert.popoverPresentationController {
                        popoverController.sourceView = sender
                        popoverController.sourceRect = sender.bounds
                    }
                    self.present(alert, animated: true, completion: nil)
                }
                
                }
            }
        
        //MARK:- imageickerDelegate
              func openCamera()
              {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                  {
                      imagePicker.sourceType = UIImagePickerController.SourceType.camera
                      imagePicker.allowsEditing = true
                      self.present(imagePicker, animated: true, completion: nil)
                  }
                  else
                  {
                      let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }
              }
              
              func openGallary()
              {
                  if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                      let imagePicker = UIImagePickerController()
                      imagePicker.delegate = self
                      imagePicker.allowsEditing = true
                      imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                      self.present(imagePicker, animated: true, completion: nil)
                  }
                  else
                  {
                      let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }
              }
              
              func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
                  guard let selectedImage = info[.originalImage] as? UIImage else {
                      fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
                  }
                
                if imgRef == 0 {
                    profilepicBool = true
                   userImg.image = selectedImage
                }else if imgRef == 1 {
                    aadharImg = selectedImage
                    aadharImgView.image = selectedImage
                    aadharImgBOOL = true
                }else if imgRef == 2 {
                    reisterImg = selectedImage
                    regCerImgView.image = selectedImage
                    regCerImgBOOL = true
                }else if imgRef == 3 {
                    degImg = selectedImage
                    degreeImg.image = selectedImage
                    degCerImgBOOL = true
                }
                  
               
                  dismiss(animated: true, completion: nil)
                  print(selectedImage.size.width)
                  print(selectedImage.size.height)
        }
           //MARK:- mapview del
           func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
               let pickedplace = place.formattedAddress
               print(pickedplace!)
               lat = "\(place.coordinate.latitude)"
               long = "\(place.coordinate.longitude)"
               self.addressTf.text = "\(place.name!)"
               self.dismiss(animated: true, completion: nil)
           }
           
           func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
               print("Error: ", error.localizedDescription)
           }
           
           func wasCancelled(_ viewController: GMSAutocompleteViewController) {
               self.dismiss(animated: true, completion: nil)
           }
        func nav(){
            let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            controller.modalPresentationStyle = .fullScreen
            controller.modalTransitionStyle = .coverVertical
            controller.selectedMenu = "Home"
            present(controller, animated: true, completion: nil)
        }
        //MARK:- API CALL
        func ProfileAPICall() {
          if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = profileURL
             let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken]
                     print(strURL)
                  
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true" {
                                    self.ugArr = allServiceDict.value(forKey: "degree1") as! [NSDictionary]
                                   
                                    self.loadDocData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                }else{
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                              // Constant.hideLoader(view: self.view)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                            case .failure(let error):
                                print(error)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                               // Constant.hideLoader(view: self.view)
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
        
        
        func specialityAPICall(Id:String,type:String) {
          if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "Loading..")
            let strURL = specialityURL
             let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken]
                     print(strURL)
            let params = ["degreeid": Id]
            print(params)
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true" {
                                }else{
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                               DispatchQueue.main.async {
                                   KVSpinnerView.dismiss()
                               }
                            case .failure(let error):
                                print(error)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                                
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
      
          
        func uploadProfileData() {


               if Reachability.isConnectedToNetwork()  {
                  
                          var params: [String: Any]
                                         
                                                   params = [
                                                     "name": nametf.text!,
                                                     "email":mailTf.text!,
                                                     "regno": regnoLbl.text!,
                                                     "aadharno": aadharNoTf.text!,
                                                     "bankname":bankNameTf.text!,
                                                     "accountholdername":accholdNametf.text!,
                                                     "accounttype":acctyeTf.text!,
                                                     "accountnumber":accnoTf.text!,
                                                     "branchname":brancNameTf.text!,
                                                     "ifsccode":ifscTf.text!,
                                                     "address": addressTf.text!,
                                                     "alternateno":alternateTf.text!,
                                                     "starttime":timeTf.text!,
                                                     "endtime":closeTimeTf.text!,
                                                     "contactpersonname": propertynameTf.text!,
                                                     "longitude":lat,
                                                     "latitude":long,
                                                     "level1_verify" : level1,
                                                     "level2_verify" : level2,
                                                     "admin_verify" : adminVerify
                                                      ]
                if docCategory == "PHA" {
                                   params.updateValue(ugID, forKey: "degree1")
                                   params.updateValue(pharmacistTf.text!, forKey: "pharmacistname")
                                   params.updateValue(pickupBOOL == false ?("N"):("Y"), forKey: "consultation")
                                   params.updateValue(deliveryBool == false ?("N"):("Y"), forKey: "housevisit")
                                   params.updateValue(priceperKMTf.text!, forKey: "priceperkm")
                                   params.updateValue(priceperKMTf1.text!, forKey: "priceperkm2")
                                   params.updateValue(priceperKMTf2.text!, forKey: "priceperkm3")
                               }else if docCategory == "AMB"  {
                                   params.updateValue(degreeTf.text!, forKey: "type")
                                   params.updateValue(priceperKMTf1.text!, forKey: "priceperkmwithoutfreezer")
                                   params.updateValue(priceperKMTf.text!, forKey: "priceperkmwithfreezer")
                                   params.updateValue(pharmacistTf.text!, forKey: "vehicleno")
                               }
                               else if docCategory == "MOR" {
                                   params.updateValue(degreeTf.text!, forKey: "type")
//                                   if degreeTf.text == "WITHFREEZER" {
//                                      
//                                   }else{
//                                       
//                                   }
                     params.updateValue(priceperKMTf.text!, forKey: "priceperkmwithfreezer")
                    params.updateValue(priceperKMTf1.text!, forKey: "priceperkmwithoutfreezer")
                                   params.updateValue(pharmacistTf.text!, forKey: "vehicleno")
                               }
                               else if docCategory == "DYL" {
                                   params.updateValue(pharmacistTf.text!, forKey: "directorname")
                                   params.updateValue(licenseTf.text!, forKey: "vehicleno")
                                   params.updateValue(dlyDurTf.text!, forKey: "timeinterval")
                                   params.updateValue(dlyfeeTf.text!, forKey: "consultationfees")
                               }
                               
                               print(params)
                
                let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                              let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                             "Authorization":strAccessToken]
                              let url = profileUpdateURL
                              print("url \(url)")
                  
                   let resizedImage = self.resizeImage(image: self.userImg.image!)
                              let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                              print("imageData New1 \(imgData!)")
                  
                  Alamofire.upload(multipartFormData: { (multipartFormData) in
                      for (key, value) in params {
                          multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                      }
                      
                                      if imgData != nil {
                                          multipartFormData.append(imgData!, withName: "profpic", fileName: "image.png", mimeType: "image/png")
                      
                                      }
                      

                                     if imgData != nil {
                                                                       multipartFormData.append(imgData!, withName: "profpic", fileName: "image.png", mimeType: "image/png")
                                     
                                                                   }

                                                               if self.aadharImg != nil {
                                                                                    let resizedImage = self.resizeImage(image: self.aadharImg)
                                                                                                                                                  let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                      print("imageData New \(imgData!)")
                                                                                                               multipartFormData.append(imgData!, withName: "aadharimage", fileName: "image1.png", mimeType: "image/png")
                                                                                }
                                                                                
                                                                                
                                                                          if self.reisterImg != nil {
                                                                              let resizedImage = self.resizeImage(image: self.reisterImg)
                                                                                                                                                        let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                                            print("imageData New \(imgData!)")
                                                                                                                                     multipartFormData.append(imgData!, withName: "regcertificateimage", fileName: "image2.png", mimeType: "image/png")
                                                                        }
                    if self.degImg != nil {
                                                                                       let resizedImage = self.resizeImage(image: self.degImg)
                                                                                                                                                                 let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                                                     print("imageData New \(imgData!)")
                                                                                                                                              multipartFormData.append(imgData!, withName: "degreeimage", fileName: "image3.png", mimeType: "image/png")
                                                                                 }

                  }, usingThreshold: UInt64.init(), to: url, method: .post,headers: headers) { (result) in
                      switch result{
                      case .success(let upload, _, _):
                          upload.responseJSON { response in
                              print("Succesfully uploaded  = \(response)")
                              if let err = response.error{
                                  print(err)
                                  return
                              }
                              
                                upload.responseJSON { response in
                                                      if response.result.value != nil {
                                                          let JSON = response.result.value
                                                          let allServiceDict : NSDictionary = JSON as! NSDictionary
                                                          print(allServiceDict)
                                                        self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         if allServiceDict.value(forKey: "status") as! String == "true" {
                                                             if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                                                             if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                                                                 UserDefaults.standard.setValue("Y", forKey: "profileEdit")
                                                                
                                                                }
                                                            }
                                                        
                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                                                                                                              self.nav()
                                                                                                    })
                                                                                                   
                                                         }else{
                                                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                    self.logout()
                                                                                                                })
                                                                                                            }else {
                                                                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                            }
                                                        }
                                  }
                              }
                              

                          }
                      case .failure(let error):
                          print("Error in upload: \(error.localizedDescription)")
                          self.view.makeToast("Server error,please try again later.", duration: 2.0, position: .center)
                      }
                  }
              }

          }
        
        
        
        func infoValidation(){
           
            if docCategory == "PHA" {
                if deliveryBool == true {
                    if priceperKMTf.text != "" {
                        if priceperKMTf1.text != "" {
                            if priceperKMTf2.text != "" {
                                PharmacyValidation()
                            }else{
                                self.priceperKMTf2.becomeFirstResponder()
                                self.view.makeToast("Please enter your delivery charge for greater than  500 km.", duration: 2.0, position: .center)
                            }
                        }else{
                            self.priceperKMTf1.becomeFirstResponder()
                            self.view.makeToast("Please enter your delivery charge for greater than  100 km.", duration: 2.0, position: .center)
                        }
                    }else{
                        self.priceperKMTf.becomeFirstResponder()
                        self.view.makeToast("Please enter your delivery charge for less than 100 km.", duration: 2.0, position: .center)
                    }
                }else{
                    PharmacyValidation()
                }

            }else if docCategory == "AMB" {
                if pharmacistTf.text != "" {
                    if propertynameTf.text != "" {
                //if self.ambType != "" {
                    if self.timeTf.text != "" {
                        if self.closeTimeTf.text != "" {
                             //   if self.ambType == "ICUSUPPORT" {
                            if priceperKMTf.text == "" &&  priceperKMTf1.text == "" {
                                                  self.view.makeToast("Please enter price per KM ", duration: 2.0, position: .center)
                                               }else{
                                                     subValidation()
                                               }
//                                    if priceperKMTf.text != "" {
//                                        subValidation()
//                                    }else{
//                                       self.view.makeToast("Please enter price per KM for with ICUSUPPORT ", duration: 2.0, position: .center)
//                                    }
//                                }
//                                else{
//                                   if priceperKMTf1.text != "" {
//                                      subValidation()
//                                    }else{
//                                      self.view.makeToast("Please enter price per KM for without ICU", duration: 2.0, position: .center)
//                                    }
//                                }
                      }else{
                             self.view.makeToast("Please select your close time.", duration: 2.0, position: .center)
                        }
                         }else{
                              self.view.makeToast("Please select your start time.", duration: 2.0, position: .center)
                      }
          //     }
                    //else{
//                    self.view.makeToast("Please select your ambulance type.", duration: 2.0, position: .center)
//                }
                 }else{
                    self.propertynameTf.becomeFirstResponder()
                    self.view.makeToast("Please select your contact person name.", duration: 2.0, position: .center)
                }
                }else{
                    self.pharmacistTf.becomeFirstResponder()
                    self.view.makeToast("Please select your vehicle number.", duration: 2.0, position: .center)
                }
            }else if docCategory == "DYL" {
                if pharmacistTf.text != "" {
                  if propertynameTf.text != "" {
                    if self.timeTf.text != "" {
                       if self.closeTimeTf.text != "" {
                        if licenseTf.text != "" {
                            if self.dlyfeeTf.text != "" {
                                if self.dlyDurTf.text != "" {
                                      subValidation()
                                    }else{
                                        self.dlyDurTf.becomeFirstResponder()
                                        self.view.makeToast("Please enter your consultation duration.", duration: 2.0, position: .center)
                                    }
                            }else{
                                self.dlyfeeTf.becomeFirstResponder()
                                self.view.makeToast("Please enter your fee", duration: 2.0, position: .center)
                            }
                        }else{
                            self.licenseTf.becomeFirstResponder()
                            self.view.makeToast("Please enter your license number.", duration: 2.0, position: .center)
                        }
                           }else{
                                 self.view.makeToast("Please select your close time.", duration: 2.0, position: .center)
                           }
                         }else{
                           self.view.makeToast("Please select your start time.", duration: 2.0, position: .center)
                        }
                  }else{
                    self.propertynameTf.becomeFirstResponder()
                         self.view.makeToast("Please enter your contact person name.", duration: 2.0, position: .center)
                   }
                }else{
                    self.pharmacistTf.becomeFirstResponder()
                   self.view.makeToast("Please enter your director name.", duration: 2.0, position: .center)
                }

                
            }else if docCategory == "MOR" {
             
              if pharmacistTf.text != "" {
                if propertynameTf.text != "" {
     //           if self.morType != "" {
//                if timeTf.text != "" {
//                                         if closeTimeTf.text != "" {
                    
                    if priceperKMTf.text == "" &&  priceperKMTf1.text == "" {
                       self.view.makeToast("Please enter price per KM ", duration: 2.0, position: .center)
                    }else{
                          subValidation()
                    }
                    
//                if self.morType == "WITHFREEZER" {
//                             if priceperKMTf.text != "" {
//
//                             }else{
//                                self.view.makeToast("Please enter price per KM for with freezer ", duration: 2.0, position: .center)
//                             }
//                         }else{
//                            if priceperKMTf1.text != "" {
//                               subValidation()
//                             }else{
//                               self.view.makeToast("Please enter price per KM for without freezer", duration: 2.0, position: .center)
//                             }
//                         }
//                    }
//
//                else{
//                        self.view.makeToast("Please select your mortuary type.", duration: 2.0, position: .center)
//                 }
                }else{
                    self.propertynameTf.becomeFirstResponder()
                    self.view.makeToast("Please enter your contact person name.", duration: 2.0, position: .center)
                 }
              }else{
                 self.pharmacistTf.becomeFirstResponder()
                 self.view.makeToast("Please enter your vehicle number.", duration: 2.0, position: .center)
              }
            
        }
        
    }
        func PharmacyValidation(){
            if pharmacistTf.text != "" {
                if propertynameTf.text != "" {
                    if degreeTf.text != "" {
                        if timeTf.text != "" {
                            if closeTimeTf.text != "" {
                                if degCerImgBOOL == true {
                                    subValidation()
                                }else{
                                    self.view.makeToast("Please select your degree certificate", duration: 2.0, position: .center)
                                }
                              
                            }else{
                                self.view.makeToast("Please select your pharmacy closing time", duration: 2.0, position: .center)
                            }
                        }else{
                            self.view.makeToast("Please select your pharmacy opening time", duration: 2.0, position: .center)
                        }
                    }else{
                         self.view.makeToast("Please select your degree qualification", duration: 2.0, position: .center)
                    }
                }else{
                    self.propertynameTf.becomeFirstResponder()
                     self.view.makeToast("Please enter propertier name", duration: 2.0, position: .center)
                }
            }else{
                self.pharmacistTf.becomeFirstResponder()
                self.view.makeToast("Please enter pharmacist name", duration: 2.0, position: .center)
            }
        }
        
        
        func subValidation(){
            if nametf.text != "" {
                if mobilenoTf.text != "" {
                    if mailTf.text != "" {
                        if addressTf.text != "" {
                            if alternateTf.text != "" {
                                if aadharNoTf.text != "" {
                                   if aadharImgBOOL != false {
                                                            if regCerImgBOOL != false {
                                                                if aadharNoTf.text?.count == 14 {
                                                                    if profilepicBool == true {
                                                                      uploadProfileData()
                                                                    }else{
                                                                   self.view.makeToast("please edit your Profile Picture", duration: 2.0, position: .center)
                                                                    }
                                                                }else{
                                                                    self.aadharNoTf.becomeFirstResponder()
                                                               self.view.makeToast("please check your aadhaar number", duration: 2.0, position: .center)
                                                                }
                                                            }else{
                                                              self.view.makeToast("please submit your regiser certificate", duration: 2.0, position: .center)
                                                            }
                                                          
                                  }else{
                                      self.view.makeToast("please select your aadhaar image", duration: 2.0, position: .center)
                                  }
                                }else{
                                  self.aadharNoTf.becomeFirstResponder()
                                  self.view.makeToast("Please enter your aadhaar number", duration: 2.0, position: .center)
                                }
                            }else{
                                self.alternateTf.becomeFirstResponder()
                                self.view.makeToast("Please enter your alternate mobile number", duration: 2.0, position: .center)
                            }
                        }else{
                            self.addressTf.becomeFirstResponder()
                            self.view.makeToast("Please enter your address", duration: 2.0, position: .center)
                        }
                    }else{
                        self.mailTf.becomeFirstResponder()
                       self.view.makeToast("Please enter your mailID", duration: 2.0, position: .center)
                    }
                }else{
                    self.mobilenoTf.becomeFirstResponder()
                   self.view.makeToast("Please enter mobile number", duration: 2.0, position: .center)
                }
             }else{
                self.nametf.becomeFirstResponder()
                self.view.makeToast("Please enter your name", duration: 2.0, position: .center)
             }
        }
        
        //MARK:- loadData
        func loadDocData(dict:NSDictionary){
           
            //verification
                   if dict.value(forKey: "level1_verify") as? String != nil {
                       level1 = "\(dict.value(forKey: "level1_verify") as! String)"
                   }
                   if dict.value(forKey: "level2_verify") as? String != nil {
                              level2 = "\(dict.value(forKey: "level2_verify") as! String)"
                          }
                   if level2 == "P" {
                       level2 = "R"
                   }
                   if dict.value(forKey: "admin_verify") as? String != nil {
                              adminVerify = "\(dict.value(forKey: "admin_verify") as! String)"
                          }
                   if adminVerify == "P" {
                       adminVerify = "R"
                   }
            //MRK : - commmon
                       nametf.text = dict.value(forKey: "name") as? String
                                 
                                  //deg1
                                  if dict.value(forKey: "degree1_name") as? String != nil {
                                      degreeTf.text = (dict.value(forKey: "degree1_name") as! String)
                                    if dict.value(forKey: "degree1_name") as! String == "" {
                                        degreeTf.isUserInteractionEnabled = true
                                    }else{
                                        degreeTf.isUserInteractionEnabled = false
                                    }
                                  }
                                  //mail
                                  if dict.value(forKey: "email") as? String != nil {
                                      mailTf.text = (dict.value(forKey: "email") as! String)
                                    mailTf.isUserInteractionEnabled = false
                                  }
                                  
                                  if dict.value(forKey: "degree1") as? Int != nil {
                                       ugID = "\(dict.value(forKey: "degree1") as! Int)"
                                  }
                                  //Img
                                  if dict.value(forKey: "aadhar_image") as? String != nil && dict.value(forKey: "aadhar_image") as? String != ""{
                                      aadharImgView.af_setImage(withURL: URL(string: dict.value(forKey: "aadhar_image") as! String)!)
                                      aadharImgBOOL = true
                                  }
                                  //rofile Img
                                  if dict.value(forKey: "prof_pic") as? String != nil && dict.value(forKey: "prof_pic") as? String != ""{
                                      userImg.af_setImage(withURL: URL(string: dict.value(forKey: "prof_pic") as! String)!)
                                    profilepicBool = true
                                  }
            //rofile Img
                                             if dict.value(forKey: "degree_image") as? String != nil && dict.value(forKey: "degree_image") as? String != ""{
                                                 degreeImg.af_setImage(withURL: URL(string: dict.value(forKey: "degree_image") as! String)!)
                                                degCerImgBOOL = true
                                             }
                       //alternate
                       if dict.value(forKey: "alternateno") as? String != nil {
                            alternateTf.text = "\(dict.value(forKey: "alternateno") as! String)"
                         if dict.value(forKey: "alternateno") as! String == "" {
                             alternateTf.isUserInteractionEnabled = true
                         }else{
                            alternateTf.isUserInteractionEnabled = false
                         }
                       }
                        //aadhar no
                                  if dict.value(forKey: "aadhar_no") as? String != nil {
                                      aadharNoTf.text = "\(dict.value(forKey: "aadhar_no") as! String)"
                                      aadharNoTf.isUserInteractionEnabled = false
                                  }
                                  
                                  //Mobile
                                  if dict.value(forKey: "mobile_no") as? String != nil {
                                      mobilenoTf.text = dict.value(forKey: "mobile_no") as? String
                                    
                                  }
                                  //regsiteNo
                                  if dict.value(forKey: "regno") as? String != nil {
                                      regnoLbl.text = "\(dict.value(forKey: "regno") as! String)"
                                  }
                       //address
                                 if dict.value(forKey: "address") as? String != nil {
                                            addressTf.text = "\(dict.value(forKey: "address") as! String)"
                                    addressTf.isUserInteractionEnabled = false
                                 }
                                //bankName
                                       if dict.value(forKey: "bank_name") as? String != nil {
                                           bankNameTf.text = "\(dict.value(forKey: "bank_name") as! String)"
                                           if dict.value(forKey: "bank_name") as! String == "" {
                                               bankNameTf.isUserInteractionEnabled = true
                                            }else{
                                               bankNameTf.isUserInteractionEnabled = false
                                           }
                                       }
                                       //ACCholdNAme
                                       if dict.value(forKey: "account_holder_name") as? String != nil {
                                           accholdNametf.text = "\(dict.value(forKey: "account_holder_name") as! String)"
                                           if dict.value(forKey: "account_holder_name") as! String == "" {
                                                          accholdNametf.isUserInteractionEnabled = true
                                                       }else{
                                                          accholdNametf.isUserInteractionEnabled = false
                                                      }
                                       }
                                       //accTye
                                       if dict.value(forKey: "account_type") as? String != nil {
                                           acctyeTf.text = "\(dict.value(forKey: "account_type") as! String)"
                                           if dict.value(forKey: "account_type") as! String == "" {
                                                                     acctyeTf.isUserInteractionEnabled = true
                                                                  }else{
                                                                     acctyeTf.isUserInteractionEnabled = false
                                                                 }
                                       }
                                       //AccNo
                                       if dict.value(forKey: "account_number") as? String != nil {
                                           accnoTf.text = "\(dict.value(forKey: "account_number") as! String)"
                                           if dict.value(forKey: "account_number") as! String == "" {
                                               accnoTf.isUserInteractionEnabled = true
                                            }else{
                                               accnoTf.isUserInteractionEnabled = false
                                           }
                                       }
                                       //BranchName
                                       if dict.value(forKey: "branch_name") as? String != nil {
                                           brancNameTf.text = "\(dict.value(forKey: "branch_name") as! String)"
                                           if dict.value(forKey: "branch_name") as! String == "" {
                                                          brancNameTf.isUserInteractionEnabled = true
                                                       }else{
                                                          brancNameTf.isUserInteractionEnabled = false
                                                      }
                                       }
                                       //ifscCode
                                       if dict.value(forKey: "ifsc_code") as? String != nil {
                                           ifscTf.text = "\(dict.value(forKey: "ifsc_code") as! String)"
                                           if dict.value(forKey: "ifsc_code") as! String == "" {
                                               ifscTf.isUserInteractionEnabled = true
                                            }else{
                                               ifscTf.isUserInteractionEnabled = false
                                           }
                                       }
            
            //regCer Img
            if dict.value(forKey: "reg_certificate_image") as? String != nil && dict.value(forKey: "reg_certificate_image") as? String != ""{
                regCerImgView.af_setImage(withURL: URL(string: dict.value(forKey: "reg_certificate_image") as! String)!)
                regCerImgBOOL = true
            }
            
           
            //pickup
                  if dict.value(forKey: "consultation") as? String != nil {
                      if dict.value(forKey: "consultation") as! String == "N" {
                                pickupBOOL = false
                                pickupImg.image = UIImage(named: "unCheck")
                            } else{
                                pickupBOOL = true
                                pickupImg.image = UIImage(named: "checkBox")
                            }
                  }
                
                  //HouseVisit
                  if dict.value(forKey: "housevisit") as? String != nil {
                      if dict.value(forKey: "housevisit") as! String == "N" {
                                 deliveryBool = false
                                 homedelImg.image = UIImage(named: "unCheck")
                             } else{
                                 deliveryBool = true
                                 homedelImg.image = UIImage(named: "checkBox")
                             }
                  }
            
          
            if docCategory == "AMB" || docCategory == "MOR" {
                if dict.value(forKey: "vehicleno") as? String != nil {
                    pharmacistTf.text = "\(dict.value(forKey: "vehicleno") as! String)"
                }
              //  if dict.value(forKey: "type") as? String != nil {
               //     degreeTf.text = "\(dict.value(forKey: "type") as! String)"
                    if docCategory == "AMB" {
                      //  ambType = "\(dict.value(forKey: "type") as! String)"
                        if dict.value(forKey: "priceperkm") as? Int != nil {
                            priceperKMTf1.text = "\(dict.value(forKey: "priceperkm") as! Int)"
                        }
                        if dict.value(forKey: "priceperkmicu") as? Int != nil {
                                                   priceperKMTf.text = "\(dict.value(forKey: "priceperkmicu") as! Int)"
                                               }
                         
                    }else{
                       // morType = "\(dict.value(forKey: "type") as! String)"
                        // price per km
                                        if dict.value(forKey: "priceperkmwithoutfreezer") as? Int != nil {
                                            priceperKMTf1.text = "\(dict.value(forKey: "priceperkmwithoutfreezer") as! Int)"
                                        }
                                  if dict.value(forKey: "priceperkm") as? Int != nil {
                                           priceperKMTf.text = "\(dict.value(forKey: "priceperkm") as! Int)"
                                       }
                    }
                    
           //     }
                // contactpersonname
                        if dict.value(forKey: "contactpersonname") as? String != nil {
                                          propertynameTf.text = "\(dict.value(forKey: "contactpersonname") as! String)"
                       }
               
            }
            
            //km
            if docCategory == "PHA" {
                if dict.value(forKey: "priceperkm") as? String != nil {
                           priceperKMTf.text = "\(dict.value(forKey: "priceperkm") as! String)"
                }
                if dict.value(forKey: "priceperkm2") as? String != nil {
                           priceperKMTf1.text = "\(dict.value(forKey: "priceperkm2") as! String)"
                }
                if dict.value(forKey: "priceperkm3") as? String != nil {
                           priceperKMTf2.text = "\(dict.value(forKey: "priceperkm3") as! String)"
                }
                if dict.value(forKey: "pharmacist_name") as? String != nil {
                           pharmacistTf.text = "\(dict.value(forKey: "pharmacist_name") as! String)"
                }
                if dict.value(forKey: "contactpersonname") as? String != nil {
                        propertynameTf.text = "\(dict.value(forKey: "contactpersonname") as! String)"
                }
                
            }
            //time
            if dict.value(forKey: "starttime") as? String != nil {
                if dict.value(forKey: "starttime") as! String != "00:00:00" {
                   timeTf.text = "\(dict.value(forKey: "starttime") as! String)"
                }else{
                   timeTf.text = ""
                }
            }
            
            if dict.value(forKey: "endtime") as? String != nil {
                if dict.value(forKey: "endtime") as! String != "00:00:00" {
                  closeTimeTf.text = "\(dict.value(forKey: "endtime") as! String)"
                }else{
                  closeTimeTf.text = ""
                }
            }
          
            if docCategory == "DYL" {
                //director name

                if dict.value(forKey: "directorname") as? String != nil {
                            pharmacistTf.text = "\(dict.value(forKey: "directorname") as! String)"
                           }
                //contact name
                if dict.value(forKey: "contactpersonname") as? String != nil {
                 propertynameTf.text = "\(dict.value(forKey: "contactpersonname") as! String)"
                }

                if dict.value(forKey: "license_image") as? String != nil && dict.value(forKey: "license_image") as? String != "" {
                                   regCerImgView.af_setImage(withURL: URL(string: dict.value(forKey: "license_image") as! String)!)
                                   regCerImgBOOL = true
                               }
                               if dict.value(forKey: "licenseno") as? String != nil && dict.value(forKey: "licenseno") as? String != "" {
                                   self.licenseTf.text = dict.value(forKey: "licenseno") as? String
                               }
                if dict.value(forKey: "timeinterval") as? NSNumber != nil {
                                dlyDurTf.text = "\(dict.value(forKey: "timeinterval") as! NSNumber)"
                               }
                if dict.value(forKey: "consultationfees") as? NSNumber != nil {
                 dlyfeeTf.text = "\(dict.value(forKey: "consultationfees") as! NSNumber)"
                }
                
            }
       
        }
      
        @available(iOS 11.0, *)
        func logout(){
            let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                 controller.modalPresentationStyle = .fullScreen
                 controller.modalTransitionStyle = .coverVertical
            present(controller, animated: true, completion: nil)
        }
    }
