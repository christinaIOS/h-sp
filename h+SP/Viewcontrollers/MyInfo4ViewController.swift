//
//  MyInfo2ViewController.swift
//  h+SP
//
//  Created by mirrorminds on 14/02/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//



    import UIKit
    import DatePickerDialog
    import DropDown
    import Alamofire
    import KVSpinnerView
    import AlamofireImage
    import GooglePlaces

class slotCell2 : UITableViewCell {
    @IBOutlet weak var startTime: UIButton!
    @IBOutlet weak var endTime: UIButton!
    @IBOutlet weak var lbl: UILabel!
 
}

    class MyInfo4ViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate,UITableViewDelegate,UITableViewDataSource {
        
        @IBOutlet weak var scrollView: UIScrollView!
        @IBOutlet weak var backBtn: UIButton!
        @IBOutlet weak var MenuBtn: UIButton!
        @IBOutlet weak var genralBtn: UIButton!
        
        //genral
        @IBOutlet weak var nametf: MyTextField!
        @IBOutlet weak var mailTf: UITextField!
        @IBOutlet weak var regnoLbl: UILabel!
        @IBOutlet weak var mobilenoTf: MyTextField!
        @IBOutlet weak var pharmacistNameTf: MyTextField!
        @IBOutlet weak var contactersonTf : MyTextField!
        @IBOutlet weak var addressTf : MyTextField!
        @IBOutlet weak var alternateTf : MyTextField!
        @IBOutlet weak var degreeTf: MyTextField!
        @IBOutlet weak var aadharNoTf: MyTextField!
     
        @IBOutlet weak var userImg: UIImageView!
        @IBOutlet weak var degreeImg: UIImageView!
        @IBOutlet weak var aadharImgView: UIImageView!
        @IBOutlet weak var regCerImgView: UIImageView!
        @IBOutlet weak var nameBoardImg: UIImageView!
       
        @IBOutlet weak var addImgLbl: UILabel!
        
         @IBOutlet weak var clinicBtn: UIButton!
        //BANKadd
        @IBOutlet weak var bankNameTf: UITextField!
        @IBOutlet weak var accholdNametf: UITextField!
        @IBOutlet weak var acctyeTf: UITextField!
        @IBOutlet weak var accnoTf: UITextField!
        @IBOutlet weak var brancNameTf: UITextField!
        @IBOutlet weak var ifscTf: UITextField!
        var bankNameID = ""
         var bankNameArr = [NSDictionary]()
        //FOR  CLINIC
        @IBOutlet weak var ClinicScrollView: UIScrollView!
        @IBOutlet weak var clinicTblView: UITableView!
       
        
        @IBOutlet weak var addBtn: UIButton!
        
     
        
       var imagePicker = UIImagePickerController()
        
       var fromView = ""
    
       var imgRef = 0
      
        
       var profilepicBool = false
       var aadharImgBOOL = false
       var regCerImgBOOL = false
       var degCerImgBOOL = false
       var nameBoardImgBOOL = false
        
        //FOR DEGREE
        let acctyeDD = DropDown()
        let ugDD = DropDown()
        let AmbTypeDD = DropDown()
        let MORDD = DropDown()
        let pgSpeacilityDD = DropDown()
        let masterSpeacilityDD = DropDown()
        let bankNameDD = DropDown()
        
        var ugArr = [NSDictionary]()
        var dayArr = [NSDictionary]()
        
        var ugID = ""
        var lat = ""
        var long = ""
        
        var docCategory = ""
        var reisterImg : UIImage!
        var aadharImg : UIImage!
        var degImg : UIImage!
        var boardImg : UIImage!
        
       
        var level1 = ""
        var level2 = ""
        var adminVerify = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            if UserDefaults.standard.value(forKey: "docCat") as? String != nil {
                docCategory = UserDefaults.standard.value(forKey: "docCat") as! String
            }
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
                 NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
          view.addGestureRecognizer(tap)
          viewSetU()
          ProfileAPICall()
            bankNameAPICall()
        }
        @objc func dismissKeyboard() {
            //Causes the view (or one of its embedded text fields) to resign the first responder status.
            view.endEditing(true)
        }
        func viewSetU(){
            
            if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                      if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                        self.view.makeToast("Please update your profile.", duration: 1.0, position: .center)
                        //alternateTf.text = ""
                       addImgLbl.isHidden = false
                         clinicBtn.isEnabled = false
                      }else{
                       // alternateTf.text = "6754895787"
                       addImgLbl.isHidden = true
                         clinicBtn.isEnabled = true
                      }
                   }
            
              self.aadharNoTf.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
            
            if fromView == "dash" {
                  MenuBtn.isHidden = true
                  backBtn.isHidden = false
                              
            }else {
                   MenuBtn.isHidden = false
                   backBtn.isHidden = true
                    gestur()
            }
            
            

            
            imagePicker.delegate = self
            docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
            
            //loader
                   KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
                   KVSpinnerView.settings.statusTextColor = UIColor.darkGray
                   KVSpinnerView.settings.tintColor = UIColor.darkGray
            
            Constant.addStatusBar(view: self.view)
                   if fromView == "dash" {
                              MenuBtn.isHidden = true
                              backBtn.isHidden = false
                       
                    }else {
                              MenuBtn.isHidden = false
                             backBtn.isHidden = true
                       gestur()
                   }
                   
           scrollView.isHidden = false
         
     }
        
         func gestur(){
            let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                         left.direction = .left
                         self.view.addGestureRecognizer(left)
                 
                         let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                         right.direction = .right
                         self.view.addGestureRecognizer(right)
         }
                 
         @objc func Swipe(){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
          }
        
        func resizeImage(image: UIImage) -> UIImage {
              
              var newHeight = 1024 as CGFloat
              var newWidth = 1024 as CGFloat
              
              if image.size.width > image.size.height {
                  let scale = newWidth / image.size.width
                  newHeight = image.size.height * scale
              }
              else if image.size.width < image.size.height {
                  let scale = newHeight / image.size.height
                  newWidth = image.size.width * scale
              }
              
              UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
              image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
              let newImage = UIGraphicsGetImageFromCurrentImageContext()
              UIGraphicsEndImageContext()
              print(newImage!)
              return newImage!
          }
        
        
        
       //MARK:- setting droDowns
        func bankNameDropDown() {
             var bankNameStrArr = [String]()
                   for i in 0..<bankNameArr.count {
                       let dict = bankNameArr[i]
                       bankNameStrArr.append(dict.value(forKey: "bankname") as! String)
                   }
                   bankNameDD.anchorView = bankNameTf
                   bankNameDD.topOffset = CGPoint(x: 0, y: bankNameTf.bounds.height + 10)
                   let joinedString = bankNameStrArr
                   bankNameDD.dataSource = joinedString
                   bankNameDD.selectionAction = { [unowned self] (index, item) in
                       self.bankNameTf.text = item
                       self.bankNameID = "\(self.bankNameArr[index].value(forKey: "id") as! NSNumber)"
                      // self.SpecialityHeightCons2.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
                      // self.deg13btn.isHidden = false
                   }
                   bankNameDD.show()
        }
        
        func setupAccTyeDropDown() {
            let accTye = ["SAVINGS","CURRENT"]
            acctyeDD.anchorView = acctyeTf
            acctyeDD.topOffset = CGPoint(x: 0, y: acctyeTf.bounds.height)
            let joinedString = accTye
            acctyeDD.dataSource = joinedString
            acctyeDD.selectionAction = { [unowned self] (index, item) in
                self.acctyeTf.text = item
            }
            acctyeDD.show()
        }
        func setupDegreeDropDown() {
             var ugStrArr = [String]()
               for i in 0..<ugArr.count {
                   let dict = ugArr[i]
                   ugStrArr.append(dict.value(forKey: "deg_degree") as! String)
               }
                ugDD.anchorView = degreeTf
                ugDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                let joinedString = ugStrArr
                ugDD.dataSource = joinedString
                ugDD.selectionAction = { [unowned self] (index, item) in
                    self.degreeTf.text = item
                    self.ugID = "\(self.ugArr[index].value(forKey: "deg_id") as! Int)"
                }
                ugDD.show()
            }
        
          func setupAMBDropDown() {
              let AMBArr = ["ICU SUPPORT", "NORMAL"]
                      AmbTypeDD.anchorView = degreeTf
                      AmbTypeDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                      let joinedString = AMBArr
                      AmbTypeDD.dataSource = joinedString
                      AmbTypeDD.selectionAction = { [unowned self] (index, item) in
                          self.degreeTf.text = item
                        //  self.ambType = AMBArr[index]
                      }
                      AmbTypeDD.show()
                  }
        
        func setupMORDropDown() {
            let MORArr = ["WITHFREEZER","WITHOUTFREEZER"]
                    MORDD.anchorView = degreeTf
                    MORDD.topOffset = CGPoint(x: 0, y: degreeTf.bounds.height + 100)
                    let joinedString = MORArr
                    MORDD.dataSource = joinedString
                    MORDD.selectionAction = { [unowned self] (index, item) in
                        self.degreeTf.text = item
                       // self.morType = MORArr[index]
                    }
                    MORDD.show()
                }
        
        // MARK: - tf delegates
        
        
        @objc func didChangeText(textField:UITextField) {
                textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
            }
            func modifyCreditCardString(creditCardString : String) -> String {
                let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()

                let arrOfCharacters = Array(trimmedString)
                var modifiedCreditCardString = ""

                if(arrOfCharacters.count > 0) {
                    for i in 0...arrOfCharacters.count-1 {
                        modifiedCreditCardString.append(arrOfCharacters[i])
                        if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                            modifiedCreditCardString.append(" ")
                        }
                    }
                }
                return modifiedCreditCardString
            }
            func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                if textField == aadharNoTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                            if(textField == aadharNoTf) {
                                return newLength <= 14
                            }
                }
              
               if textField == alternateTf {
                   let newLength = (textField.text ?? "").count + string.count - range.length
                   if(textField == alternateTf) {
                       return newLength <= 10
                   }
               }
                
                if textField == brancNameTf {
                    let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == brancNameTf) {
                        return newLength <= 10
                    }
                }
                
                 return true
            }
        
       
        @objc func keyboardWillHide(notification: Notification) {
            let contentInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
        }

        @objc func keyboardWillShow(notification: Notification) {
            guard let keyboardFrame: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
            
                    if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                        case 1136:
                            scrollView.contentInset.bottom = keyboardFrame.height
                        case 1334:
                            scrollView.contentInset.bottom = keyboardFrame.height
                        case 1920, 2208:
                            print("iPhone 6+/6S+/7+/8+")
                          scrollView.contentInset.bottom = keyboardFrame.height
                        case 2436:
                            print("iPhone X/XS/11 Pro")
                           scrollView.contentInset.bottom = keyboardFrame.height + 78
                        case 2688:
                            print("iPhone XS Max/11 Pro Max")
                        scrollView.contentInset.bottom = keyboardFrame.height + 80
                        case 1792:
                            print("iPhone XR/ 11 ")
                        scrollView.contentInset.bottom = keyboardFrame.height + 80
                        default:
                            print("Unknown")
                        }
                    }else{
                         scrollView.contentInset.bottom = keyboardFrame.height + 200
                 }
        }
        
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

            if  textField == acctyeTf {
                setupAccTyeDropDown()
                return false
            }
           if textField == degreeTf {
            textField.resignFirstResponder()
                if docCategory == "AMB" {
                    setupAMBDropDown()
                   return false
                }else if docCategory == "MOR" {
                    setupMORDropDown()
                   return false
                }else if docCategory == "PHA" {
                    setupDegreeDropDown()
                    return false
                }
           }

         
            if textField == addressTf {
                let autocompleteController = GMSAutocompleteViewController()
                autocompleteController.delegate = self
                autocompleteController.modalPresentationStyle = .fullScreen
                self.present(autocompleteController, animated: true, completion: nil)
                return false
            }
            
            if  textField == bankNameTf {
                      textField.resignFirstResponder()
                            bankNameDropDown()
                            return false
                  }
            
            return true
         }
            
            func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                textField.resignFirstResponder()
                return true
            }
            
        // MARK: - btnActions
        
        @IBAction func notificationAction(_ sender: Any) {
                 let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
                   controller.fromview = "O"
                 controller.modalPresentationStyle = .fullScreen
                 controller.modalTransitionStyle = .coverVertical
                 present(controller, animated: true, completion: nil)
           }
        
         @IBAction func menuAction(_ sender: UIButton) {
             self.view.endEditing(true)
//                 if sender.tag == 0 {
//
//                   }else if sender.tag == 1 {
//                       self.dismiss(animated: true, completion: nil)
//                   }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }
        
        @IBAction func nextAction(_ sender: UIButton) {
            infoValidation()
        }
        
     
        
          @IBAction func tabAction(_ sender: UIButton) {
           if sender.tag == 0 {
               ProfileAPICall()
            //   toastLbl.isHidden = true
                      genralBtn.setTitleColor(UIColor.white, for: .normal)
                      clinicBtn.setTitleColor(UIColor.black, for: .normal)
                      genralBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                      clinicBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                      scrollView.isHidden = false
                    //  tblView.isHidden = true
                      addBtn.isHidden = true
                      ClinicScrollView.isHidden = true
                  }else if sender.tag == 1 {
                    //  AMShimmer.start(for: tblView)
                      clinicBtn.setTitleColor(UIColor.white, for: .normal)
                      genralBtn.setTitleColor(UIColor.black, for: .normal)
                      clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                      genralBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                      if docCategory == "PHA" {
                       clinicBtn.setTitle("TIMINGS", for: .normal)
                          scrollView.isHidden = true
                         // tblView.isHidden = true
                          ClinicScrollView.isHidden = false
                          addBtn.isHidden = true
                        getphaTimeAPICall()
                       
                      }else{
                       clinicBtn.setTitle("Clinic", for: .normal)
                          scrollView.isHidden = true
                          ClinicScrollView.isHidden = true
                          //tblView.isHidden = false
                          addBtn.isHidden = false
                           //clinicAPICall()
                      }
                     
                     
                  }
       }
        
   
        @IBAction func editprofileAction(_ sender: UIButton) {
            imgRef = sender.tag
            
               if sender.tag == 0 {
                   if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                        {
                            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                            imagePicker.allowsEditing = true
                            imagePicker.cameraDevice = .front
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                        else
                        {
                            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else{
                         let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                                           let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
                                           {
                                               UIAlertAction in
                                               self.openCamera()
                                               
                                           }
                                           let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
                                           {
                                               UIAlertAction in
                                               self.openGallary()
                                           }
                                           let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                                           {
                                               UIAlertAction in
                                           }
                                           
                                           // Add the actions
                                           imagePicker.delegate = self
                                           alert.addAction(cameraAction)
                                           alert.addAction(gallaryAction)
                                           alert.addAction(cancelAction)
                                           // Present the controller
                                       
                                       if UIDevice.current.userInterfaceIdiom == .phone {
                                            self.present(alert, animated: true, completion: nil)
                                       }else{
                                           if let popoverController = alert.popoverPresentationController {
                                               popoverController.sourceView = sender
                                               popoverController.sourceRect = sender.bounds
                                           }
                                           self.present(alert, animated: true, completion: nil)
                                       }
                                       
                    }
                }else {
                    let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                    let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
                    {
                        UIAlertAction in
                        self.openCamera()
                        
                    }
                    let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
                    {
                        UIAlertAction in
                        self.openGallary()
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                    {
                        UIAlertAction in
                    }
                    
                    // Add the actions
                    imagePicker.delegate = self
                    alert.addAction(cameraAction)
                    alert.addAction(gallaryAction)
                    alert.addAction(cancelAction)
                    // Present the controller
                
                if UIDevice.current.userInterfaceIdiom == .phone {
                     self.present(alert, animated: true, completion: nil)
                }else{
                    if let popoverController = alert.popoverPresentationController {
                        popoverController.sourceView = sender
                        popoverController.sourceRect = sender.bounds
                    }
                    self.present(alert, animated: true, completion: nil)
                }
                
                }
            }
        
         @IBAction func timeSlotUdateAction(_ sender: UIButton) {
            EditSlotAPICall()
        }
        //MARK:- imageickerDelegate
              func openCamera()
              {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                  {
                      imagePicker.sourceType = UIImagePickerController.SourceType.camera
                      imagePicker.allowsEditing = true
                      self.present(imagePicker, animated: true, completion: nil)
                  }
                  else
                  {
                      let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }
              }
              
              func openGallary()
              {
                  if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                      let imagePicker = UIImagePickerController()
                      imagePicker.delegate = self
                      imagePicker.allowsEditing = true
                      imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                      self.present(imagePicker, animated: true, completion: nil)
                  }
                  else
                  {
                      let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                  }
              }
              
              func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
                  guard let selectedImage = info[.originalImage] as? UIImage else {
                      fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
                  }
                
                if imgRef == 0 {
                    profilepicBool = true
                
                    userImg.image = selectedImage
                }else if imgRef == 1 {
                    aadharImg = selectedImage
                    aadharImgView.image = selectedImage
                    aadharImgBOOL = true
                }else if imgRef == 2 {
                    reisterImg = selectedImage
                    regCerImgView.image = selectedImage
                    regCerImgBOOL = true
                }else if imgRef == 3 {
                    degImg = selectedImage
                    degreeImg.image = selectedImage
                    degCerImgBOOL = true
                }else if imgRef == 4 {
                    boardImg = selectedImage
                    nameBoardImg.image = selectedImage
                    nameBoardImgBOOL = true
                }
               
                  dismiss(animated: true, completion: nil)
                  print(selectedImage.size.width)
                  print(selectedImage.size.height)
        }
        
        
           //MARK:- mapview del
           func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
               let pickedplace = place.formattedAddress
               print(pickedplace!)
               lat = "\(place.coordinate.latitude)"
               long = "\(place.coordinate.longitude)"
               self.addressTf.text = "\(place.name!)"
               self.dismiss(animated: true, completion: nil)
           }
           
           func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
               print("Error: ", error.localizedDescription)
           }
           
           func wasCancelled(_ viewController: GMSAutocompleteViewController) {
               self.dismiss(animated: true, completion: nil)
           }
        func nav(){
            let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            controller.modalPresentationStyle = .fullScreen
            controller.modalTransitionStyle = .coverVertical
            controller.selectedMenu = "Home"
            present(controller, animated: true, completion: nil)
        }
        
    

        // MARK: - tblView Delegates
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
            return  dayArr.count
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                return 60
            }else{
                return 100
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! slotCell2
            cell.lbl.text = (self.dayArr[indexPath.row].value(forKey: "day") as! String
            ).uppercased()
            
            if self.dayArr[indexPath.row].value(forKey: "starttime") as! String != "" {
                cell.startTime.setTitle(self.dayArr[indexPath.row].value(forKey: "starttime") as? String, for: .normal)
            }else{
                cell.startTime.setTitle("Start", for: .normal)
            }
            
            if self.dayArr[indexPath.row].value(forKey: "endtime") as! String != "" {
                cell.endTime.setTitle(self.dayArr[indexPath.row].value(forKey: "endtime") as? String, for: .normal)
            }else{
                cell.endTime.setTitle("End", for: .normal)
            }
            
            cell.startTime.tag = indexPath.row
            cell.endTime.tag = indexPath.row
            cell.startTime.layer.cornerRadius = 10
            cell.endTime.layer.cornerRadius = 10
            
            cell.startTime.addTarget(self, action: #selector(selectStartTime(sender:)), for: .touchUpInside)
            cell.endTime.addTarget(self, action: #selector(selectEndTime(sender:)), for: .touchUpInside)
            
            return cell
        }
     
        @objc func selectStartTime(sender:UIButton){
            
           
            
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.month = 12
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
                           
            DatePickerDialog().show("Time", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .time) { (date) in
            if let dt = date {
               let formatter = DateFormatter()
                 formatter.dateFormat = "HH:mm"
                
                
              
                
                let dict = self.dayArr[sender.tag].mutableCopy() as! NSMutableDictionary
                    dict.setValue(formatter.string(from: dt), forKey: "starttime")
                self.dayArr[sender.tag] = dict as NSDictionary
                self.clinicTblView.reloadData()
                
               

               }
            }
        }
        
        @objc func selectEndTime(sender:UIButton){
          
                       
                       let currentDate = Date()
                       var dateComponents = DateComponents()
                       dateComponents.month = 12
                       let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
                                      
                       DatePickerDialog().show("Time", doneButtonTitle: "OK", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .time) { (date) in
                       if let dt = date {
                          let formatter = DateFormatter()
                            formatter.dateFormat = "HH:mm"
                           
                         
                        
                        let dict = self.dayArr[sender.tag].mutableCopy() as! NSMutableDictionary
                            dict.setValue(formatter.string(from: dt), forKey: "endtime")
                        self.dayArr[sender.tag] = dict as NSDictionary
                        self.clinicTblView.reloadData()
                        
                          }
                       }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
        }
        
        func bankValidation() {
            if bankNameTf.text != "" {
                if accholdNametf.text != "" {
                    if acctyeTf.text != "" {
                        if accnoTf.text != "" {
                            if ifscTf.text != "" {
                                if brancNameTf.text != "" {
                                     uploadProfileData()
                                }else{
                                   // self.brancNameTf.becomeFirstResponder()
                                    self.view.makeToast("please enter  pan card number", duration: 2.0, position: .center)
                                }

                            }else{
                               // self.ifscTf.becomeFirstResponder()
                                self.view.makeToast("please enter IFSC code", duration: 2.0, position: .center)
                            }
                        }else{
                            //self.accnoTf.becomeFirstResponder()
                            self.view.makeToast("please choose account number", duration: 2.0, position: .center)
                        }
                    }else{
                        // self.acctyeTf.becomeFirstResponder()
                        self.view.makeToast("please choose account type", duration: 2.0, position: .center)
                    }
                }else{
                   // self.accholdNametf.becomeFirstResponder()
                    self.view.makeToast("please enter account holder name", duration: 2.0, position: .center)
                }
            }else{
              //  self.bankNameTf.becomeFirstResponder()
                self.view.makeToast("please select your bank name", duration: 2.0, position: .center)
            }
        }

        //MARK:- API CALL
        
        func bankNameAPICall() {
            if Reachability.isConnectedToNetwork() {
                KVSpinnerView.show(saying: "")
                let strURL = bankName
                let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                let headers = ["Content-Type": "application/x-www-form-urlencoded",
                               "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
                print(strURL)
              //  let params = ["degreeid": Id]
               // print(params)
                print(strURL)
                print(headers)
                Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                    {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.bankNameArr = allServiceDict.value(forKey: "list") as! [NSDictionary]
                            }else{
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                    UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                       self.logout()
                                                                   })
                                                               }else {
                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               }
                            }
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                }
            }else {
                self.view.makeToast("NetWork error", duration: 3.0, position: .center)
            }
        }
        
        func ProfileAPICall() {
          if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = profileURL
             let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken]
                     print(strURL)
                  
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true" {
                                    self.ugArr = allServiceDict.value(forKey: "degree1") as! [NSDictionary]
                                   
                                    self.loadDocData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                }else{
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                              // Constant.hideLoader(view: self.view)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                            case .failure(let error):
                                print(error)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                               // Constant.hideLoader(view: self.view)
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
        
        
        
        
        
        func specialityAPICall(Id:String,type:String) {
          if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "Loading..")
            let strURL = specialityURL
             let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken]
                     print(strURL)
            let params = ["degreeid": Id]
            print(params)
            print(strURL)
            print(headers)
                      Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                          {
                            response in switch response.result {
                            case .success(let JSON):
                                let allServiceDict = (JSON as! NSDictionary)
                                print(allServiceDict)
                                if allServiceDict.value(forKey: "status") as! String == "true" {
                                }else{
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                               DispatchQueue.main.async {
                                   KVSpinnerView.dismiss()
                               }
                            case .failure(let error):
                                print(error)
                                DispatchQueue.main.async {
                                    KVSpinnerView.dismiss()
                                }
                                
                            }
                      }
                  }else {
                   self.view.makeToast("NetWork error", duration: 3.0, position: .center)
                 }
        }
      
          
        func uploadProfileData() {


               if Reachability.isConnectedToNetwork()  {
                  
                          var params: [String: Any]
                                         
                                                   params = [
                                                     "name": nametf.text!,
                                                     "email":mailTf.text!,
                                                     "regno": regnoLbl.text!,
                                                     "aadharno": aadharNoTf.text!,
                                                     "bankname":bankNameTf.text!,
                                                     "accountholdername":accholdNametf.text!,
                                                     "accounttype":acctyeTf.text!,
                                                     "accountnumber":accnoTf.text!,
                                                     "branchname":brancNameTf.text!,
                                                     "ifsccode":ifscTf.text!,
                                                     "address": addressTf.text!,
                                                     "alternateno":alternateTf.text!,
                                                     "contactpersonname": contactersonTf.text!,
                                                     "longitude":lat,
                                                     "latitude":long,
                                                     "pharmacistname" : pharmacistNameTf.text!,
                                                     "level1_verify" : level1,
                                                     "level2_verify" : level2,
                                                     "admin_verify" : adminVerify,
                                                      "degree1" : ugID,
                                                      "consultation" : "N",
                                                      "housevisit" : "N"
                                                      
                                                ]
               
                        
                
                               print(params)
                
                let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                              let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                             "Authorization":strAccessToken]
                              let url = profileUpdateURL
                              print("url \(url)")
                  
                   let resizedImage = self.resizeImage(image: self.userImg.image!)
                              let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                              print("imageData New1 \(imgData!)")
                  
                  Alamofire.upload(multipartFormData: { (multipartFormData) in
                      for (key, value) in params {
                          multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                      }
                      
                                      if imgData != nil {
                                          multipartFormData.append(imgData!, withName: "profpic", fileName: "image.png", mimeType: "image/png")
                      
                                      }
                      

                                     if imgData != nil {
                                                                       multipartFormData.append(imgData!, withName: "profpic", fileName: "image.png", mimeType: "image/png")
                                     
                                                                   }

                                                               if self.aadharImg != nil {
                                                                                    let resizedImage = self.resizeImage(image: self.aadharImg)
                                                                                                                                                  let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                      print("imageData New \(imgData!)")
                                                                                                               multipartFormData.append(imgData!, withName: "aadharimage", fileName: "image1.png", mimeType: "image/png")
                                                                                }
                                                                                
                                                                                
                                                                          if self.reisterImg != nil {
                                                                              let resizedImage = self.resizeImage(image: self.reisterImg)
                                                                                                                                                        let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                                            print("imageData New \(imgData!)")
                                                                                                                                     multipartFormData.append(imgData!, withName: "regcertificateimage", fileName: "image2.png", mimeType: "image/png")
                                                                        }
                    if self.degImg != nil {
                                                                                       let resizedImage = self.resizeImage(image: self.degImg)
                                                                                                                                                                 let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                                                     print("imageData New \(imgData!)")
                                                                                                                                              multipartFormData.append(imgData!, withName: "degreeimage", fileName: "image3.png", mimeType: "image/png")
                                                                                 }

                    if self.boardImg != nil {
                                                                        let resizedImage = self.resizeImage(image: self.boardImg)
                                                                                                                                                                                   let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                                                                                                                                                                                       print("imageData New \(imgData!)")
                                                                                                                                                                multipartFormData.append(imgData!, withName: "frontviewphoto", fileName: "image4.png", mimeType: "image/png")
                                                                                                   }
                    
                  }, usingThreshold: UInt64.init(), to: url, method: .post,headers: headers) { (result) in
                      switch result{
                      case .success(let upload, _, _):
                          upload.responseJSON { response in
                              print("Succesfully uploaded  = \(response)")
                              if let err = response.error{
                                  print(err)
                                  return
                              }
                              
                                upload.responseJSON { response in
                                                      if response.result.value != nil {
                                                          let JSON = response.result.value
                                                          let allServiceDict : NSDictionary = JSON as! NSDictionary
                                                          print(allServiceDict)
                                                        self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         if allServiceDict.value(forKey: "status") as! String == "true" {
                                                             if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                                                                                         if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                                                                                             self.clinicBtn.isEnabled = true
                                                                                             self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                                                                             self.genralBtn.setTitleColor(UIColor.black, for: .normal)
                                                                                             self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                                                                             self.genralBtn.backgroundColor = UIColor.lightGray
                                                             
                                                                                             if self.docCategory == "PHA" {
                                                                                                  UserDefaults.standard.setValue("V", forKey: "profileEdit")
                                                                                                 self.clinicBtn.setTitle("Clinic", for: .normal)
                                                                                                 self.scrollView.isHidden = true
                                                                                                 //self.tblView.isHidden = true
                                                                                                 self.ClinicScrollView.isHidden = false
                                                                                                 self.addBtn.isHidden = true
                                                                                                self.getphaTimeAPICall()
                                                                                                 self.clinicBtn.setTitle("Clinic", for: .normal)
                                                                                           }else{
                                                                                                 UserDefaults.standard.setValue("C", forKey: "profileEdit")
                                                                                                 self.clinicBtn.setTitle("Clinic", for: .normal)
                                                                                                 self.scrollView.isHidden = true
                                                                                                 self.ClinicScrollView.isHidden = true
                                                                                                 //self.tblView.isHidden = false
                                                                                                 self.addBtn.isHidden = false
                                                                                                self.getphaTimeAPICall()
                                                                                                
                                                                                            }
                                                                                         }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "C" {
                                                                                             self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                                                                                                                    self.genralBtn.setTitleColor(UIColor.black, for: .normal)
                                                                                                                                    self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                                                                                                                    self.genralBtn.backgroundColor = UIColor.lightGray
                                                                                             self.clinicBtn.setTitle("Clinic", for: .normal)
                                                                                                                                        self.scrollView.isHidden = true
                                                                                                                                        self.ClinicScrollView.isHidden = true
                                                                                                                                        //self.tblView.isHidden = false
                                                                                                                                        self.addBtn.isHidden = false
                                                                                            self.getphaTimeAPICall()
                                                                                         }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "Y"{
                                                                                             DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                       self.nav()
                                                                                             })
                                                                                         }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "V" {
                                                                                             self.clinicBtn.isEnabled = true
                                                                                             self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                                                                             self.genralBtn.setTitleColor(UIColor.black, for: .normal)
                                                                                             self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                                                                             self.genralBtn.backgroundColor = UIColor.lightGray
                                                                                             self.clinicBtn.setTitle("TIMINGS", for: .normal)
                                                                                                                                        self.scrollView.isHidden = true
                                                                                                                                        //self.tblView.isHidden = true
                                                                                                                                        self.ClinicScrollView.isHidden = false
                                                                                                                                        self.addBtn.isHidden = true
                                                                                                                                        
                                                                                            self.getphaTimeAPICall()
                                                                                            self.clinicBtn.setTitle("TIMINGS", for: .normal)
                                                                                         }
                                                                                     }
                                                                                                   
                                                         }else{
                                                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                                                                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                    self.logout()
                                                                                                                })
                                                                                                            }else {
                                                                                                                   self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                                            }
                                                        }
                                  }
                              }
                              

                          }
                      case .failure(let error):
                          print("Error in upload: \(error.localizedDescription)")
                          self.view.makeToast("Server error,please try again later.", duration: 2.0, position: .center)
                      }
                  }
              }

          }
        
         func EditSlotAPICall() {
            if Reachability.isConnectedToNetwork() {
                KVSpinnerView.show(saying: "")

                let strURL = pharmacyTiming
                let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                let headers = ["Content-Type": "application/x-www-form-urlencoded",
                               "Authorization":strAccessToken]
                let params = [
                    "monday" : "\(self.dayArr[0].value(forKey: "starttime") as! String),\(self.dayArr[0].value(forKey: "endtime") as! String)",
                              "tuesday" : "\(self.dayArr[1].value(forKey: "starttime") as! String),\(self.dayArr[1].value(forKey: "endtime") as! String)",
                              "wednesday" : "\(self.dayArr[2].value(forKey: "starttime") as! String),\(self.dayArr[2].value(forKey: "endtime") as! String)",
                              "thursday": "\(self.dayArr[3].value(forKey: "starttime") as! String),\(self.dayArr[3].value(forKey: "endtime") as! String)",
                              "friday": "\(self.dayArr[4].value(forKey: "starttime") as! String),\(self.dayArr[4].value(forKey: "endtime") as! String)",
                              "saturday": "\(self.dayArr[5].value(forKey: "starttime") as! String),\(self.dayArr[5].value(forKey: "endtime") as! String)",
                              "sunday": "\(self.dayArr[6].value(forKey: "starttime") as! String),\(self.dayArr[6].value(forKey: "endtime") as! String)",
                              "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")] as [String : Any]

                print(strURL)
                print(headers)
                print(params)

                Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                    {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" { UserDefaults.standard.setValue("Y", forKey: "profileEdit")
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                              DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                               self.nav()
                                                                     })

                            }else{
                                           if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                              self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                          self.logout()
                                                                                      })
                                                                                  }else {
                                                                                        self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                  }
                            }
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }

                        }
                }
            }else {
                self.view.makeToast("NetWork error", duration: 3.0, position: .center)
            }
        }
        
        func infoValidation(){
           
            PharmacyValidation()
        
       }
        func PharmacyValidation(){
            if pharmacistNameTf.text != "" {
                if contactersonTf.text != "" {
                    if degreeTf.text != "" {
                        if nameBoardImgBOOL == true {
                            
                            subValidation()
//
//                                if degCerImgBOOL == true {
//                                    subValidation()
//                                }else{
//                                    self.view.makeToast("Please select your degree certificate", duration: 2.0, position: .center)
//                                }
                              
                     }else{
                           self.view.makeToast("Please select your name board image", duration: 2.0, position: .center)
                                        }
                    }else{
                         self.view.makeToast("Please select your degree qualification", duration: 2.0, position: .center)
                    }
                }else{
                    self.contactersonTf.becomeFirstResponder()
                     self.view.makeToast("Please enter contact person name", duration: 2.0, position: .center)
                }
            }else{
                self.pharmacistNameTf.becomeFirstResponder()
                self.view.makeToast("Please enter pharmacist name", duration: 2.0, position: .center)
            }
        }
        
        
        func subValidation(){
            if nametf.text != "" {
                if mobilenoTf.text != "" {
                    if mailTf.text != "" {
                        if addressTf.text != "" {
                            if alternateTf.text != "" {
//                                if aadharNoTf.text != "" {
//                                   if aadharImgBOOL != false {
                                                            if regCerImgBOOL != false {
                                                            //    if aadharNoTf.text?.count == 14 {
                                                                    if profilepicBool == true {
                                                                     // bankValidation()
                                                                        uploadProfileData()
                                                                    }else{
                                                                   self.view.makeToast("please edit your Profile Picture", duration: 2.0, position: .center)
                                                                    }
//                                                                }else{
//                                                                    self.aadharNoTf.becomeFirstResponder()
//                                                               self.view.makeToast("please check your aadhaar number", duration: 2.0, position: .center)
//                                                                }
                                                            }else{
                                                              self.view.makeToast("please submit your regiser certificate", duration: 2.0, position: .center)
                                                            }
                                                          
//                                  }else{
//                                      self.view.makeToast("please select your aadhaar image", duration: 2.0, position: .center)
//                                  }
//                                }else{
//                                  self.aadharNoTf.becomeFirstResponder()
//                                  self.view.makeToast("Please enter your aadhaar number", duration: 2.0, position: .center)
//                                }
                            }else{
                                self.alternateTf.becomeFirstResponder()
                                self.view.makeToast("Please enter your alternate mobile number", duration: 2.0, position: .center)
                            }
                        }else{
                            self.addressTf.becomeFirstResponder()
                            self.view.makeToast("Please enter your address", duration: 2.0, position: .center)
                        }
                    }else{
                        self.mailTf.becomeFirstResponder()
                       self.view.makeToast("Please enter your mailID", duration: 2.0, position: .center)
                    }
                }else{
                    self.mobilenoTf.becomeFirstResponder()
                   self.view.makeToast("Please enter mobile number", duration: 2.0, position: .center)
                }
             }else{
                self.nametf.becomeFirstResponder()
                self.view.makeToast("Please enter your name", duration: 2.0, position: .center)
             }
        }
        

        //MARK:- FOR NURSE
        func getphaTimeAPICall() {
            if Reachability.isConnectedToNetwork() {
                KVSpinnerView.show(saying: "")
                let strURL = pharmacyTiming
                let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                let headers = ["Content-Type": "application/x-www-form-urlencoded",
                               "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
                print(strURL)
                
                print(strURL)
                print(headers)
                Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                    {
                        response in switch response.result {
               case .success(let JSON):
                    let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                    if allServiceDict.value(forKey: "status") as! String == "true" {
                        self.dayArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                        self.clinicTblView.reloadData()
                            }else{
                    if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                                 self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                 DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                     UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                     self.logout()
                                                                 })
                                                             }else {
                                                                 self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                             }
                            }
                            // Constant.hideLoader(view: self.view)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            // Constant.hideLoader(view: self.view)
                        }
                }
            }else {
                self.view.makeToast("NetWork error", duration: 3.0, position: .center)
            }
        }

        
        //MARK:- loadData
        func loadDocData(dict:NSDictionary){
           
            //verification
                             if dict.value(forKey: "level1_verify") as? String != nil {
                                 level1 = "\(dict.value(forKey: "level1_verify") as! String)"
                             }
                             if dict.value(forKey: "level2_verify") as? String != nil {
                                        level2 = "\(dict.value(forKey: "level2_verify") as! String)"
                                    }
                             if level2 == "P" {
                                 level2 = "R"
                             }
                             if dict.value(forKey: "admin_verify") as? String != nil {
                                        adminVerify = "\(dict.value(forKey: "admin_verify") as! String)"
                                    }
                             if adminVerify == "P" {
                                 adminVerify = "R"
                             }
            //MRK : - commmon
                       nametf.text = dict.value(forKey: "name") as? String
             UserDefaults.standard.setValue(dict.value(forKey: "name") as! String, forKey: "name")
            
                                 if dict.value(forKey: "contactpersonname") as? String != nil {
                                     contactersonTf.text = (dict.value(forKey: "contactpersonname") as! String)
                                   if dict.value(forKey: "contactpersonname") as! String == "" {
                                       contactersonTf.isUserInteractionEnabled = true
                                   }else{
                                       contactersonTf.isUserInteractionEnabled = false
                                   }
                                 }
            
            
            if dict.value(forKey: "pharmacist_name") as? String != nil {
                                              pharmacistNameTf.text = (dict.value(forKey: "pharmacist_name") as! String)
                                            if dict.value(forKey: "pharmacist_name") as! String == "" {
                                                pharmacistNameTf.isUserInteractionEnabled = true
                                            }else{
                                                pharmacistNameTf.isUserInteractionEnabled = false
                                            }
                                          }
            
                                  //deg1
                                  if dict.value(forKey: "degree1_name") as? String != nil {
                                      degreeTf.text = (dict.value(forKey: "degree1_name") as! String)
                                    if dict.value(forKey: "degree1_name") as! String == "" {
                                        degreeTf.isUserInteractionEnabled = true
                                    }else{
                                        degreeTf.isUserInteractionEnabled = false
                                    }
                                  }
                                  //mail
                                  if dict.value(forKey: "email") as? String != nil {
                                      mailTf.text = (dict.value(forKey: "email") as! String)
                                    mailTf.isUserInteractionEnabled = false
                                  }
                                  
                                  if dict.value(forKey: "degree1") as? Int != nil {
                                       ugID = "\(dict.value(forKey: "degree1") as! Int)"
                                  }
                                  //Img
                                  if dict.value(forKey: "aadhar_image") as? String != nil && dict.value(forKey: "aadhar_image") as? String != ""{
                                      aadharImgView.af_setImage(withURL: URL(string: dict.value(forKey: "aadhar_image") as! String)!)
                                      aadharImgBOOL = true
                                  }
                                  //rofile Img
                                  if dict.value(forKey: "prof_pic") as? String != nil && dict.value(forKey: "prof_pic") as? String != ""{
                                      userImg.af_setImage(withURL: URL(string: dict.value(forKey: "prof_pic") as! String)!)
                                    
                                    UserDefaults.standard.set(dict.value(forKey: "prof_pic") as! String, forKey: "profileImg")
                                    
                                    profilepicBool = true
                                  }
            //rofile Img
                                             if dict.value(forKey: "degree_image") as? String != nil && dict.value(forKey: "degree_image") as? String != ""{
                                                 degreeImg.af_setImage(withURL: URL(string: dict.value(forKey: "degree_image") as! String)!)
                                                degCerImgBOOL = true
                                             }
                       //alternate
                       if dict.value(forKey: "alternateno") as? String != nil {
                            alternateTf.text = "\(dict.value(forKey: "alternateno") as! String)"
                         if dict.value(forKey: "alternateno") as! String == "" {
                             alternateTf.isUserInteractionEnabled = true
                         }else{
                            alternateTf.isUserInteractionEnabled = false
                         }
                       }
                        //aadhar no
                                  if dict.value(forKey: "aadhar_no") as? String != nil {
                                      aadharNoTf.text = "\(dict.value(forKey: "aadhar_no") as! String)"
                                      aadharNoTf.isUserInteractionEnabled = false
                                  }
                                  
                                  //Mobile
                                  if dict.value(forKey: "mobile_no") as? String != nil {
                                      mobilenoTf.text = dict.value(forKey: "mobile_no") as? String
                                     UserDefaults.standard.setValue(dict.value(forKey: "mobile_no") as! String, forKey: "mobile")
                                  }
                                  //regsiteNo
                                  if dict.value(forKey: "regno") as? String != nil {
                                      regnoLbl.text = "\(dict.value(forKey: "regno") as! String)"
                                  }
                       //address
                                 if dict.value(forKey: "address") as? String != nil {
                                            addressTf.text = "\(dict.value(forKey: "address") as! String)"
                                    addressTf.isUserInteractionEnabled = false
                                 }
                                //bankName
                                       if dict.value(forKey: "bank_name") as? String != nil {
                                           bankNameTf.text = "\(dict.value(forKey: "bank_name") as! String)"
                                           if dict.value(forKey: "bank_name") as! String == "" {
                                               bankNameTf.isUserInteractionEnabled = true
                                            }else{
                                               bankNameTf.isUserInteractionEnabled = false
                                           }
                                       }
                                       //ACCholdNAme
                                       if dict.value(forKey: "account_holder_name") as? String != nil {
                                           accholdNametf.text = "\(dict.value(forKey: "account_holder_name") as! String)"
                                           if dict.value(forKey: "account_holder_name") as! String == "" {
                                                          accholdNametf.isUserInteractionEnabled = true
                                                       }else{
                                                          accholdNametf.isUserInteractionEnabled = false
                                                      }
                                       }
                                       //accTye
                                       if dict.value(forKey: "account_type") as? String != nil {
                                           acctyeTf.text = "\(dict.value(forKey: "account_type") as! String)"
                                           if dict.value(forKey: "account_type") as! String == "" {
                                                                     acctyeTf.isUserInteractionEnabled = true
                                                                  }else{
                                                                     acctyeTf.isUserInteractionEnabled = false
                                                                 }
                                       }
                                       //AccNo
                                       if dict.value(forKey: "account_number") as? String != nil {
                                           accnoTf.text = "\(dict.value(forKey: "account_number") as! String)"
                                           if dict.value(forKey: "account_number") as! String == "" {
                                               accnoTf.isUserInteractionEnabled = true
                                            }else{
                                               accnoTf.isUserInteractionEnabled = false
                                           }
                                       }
                                       //BranchName
                                       if dict.value(forKey: "branch_name") as? String != nil {
                                           brancNameTf.text = "\(dict.value(forKey: "branch_name") as! String)"
                                           if dict.value(forKey: "branch_name") as! String == "" {
                                                          brancNameTf.isUserInteractionEnabled = true
                                                       }else{
                                                          brancNameTf.isUserInteractionEnabled = false
                                                      }
                                       }
                                       //ifscCode
                                       if dict.value(forKey: "ifsc_code") as? String != nil {
                                           ifscTf.text = "\(dict.value(forKey: "ifsc_code") as! String)"
                                           if dict.value(forKey: "ifsc_code") as! String == "" {
                                               ifscTf.isUserInteractionEnabled = true
                                            }else{
                                               ifscTf.isUserInteractionEnabled = false
                                           }
                                       }
            
            //regCer Img
            if dict.value(forKey: "reg_certificate_image") as? String != nil && dict.value(forKey: "reg_certificate_image") as? String != ""{
                regCerImgView.af_setImage(withURL: URL(string: dict.value(forKey: "reg_certificate_image") as! String)!)
                regCerImgBOOL = true
            }
            //frontviewphoto Img
                       if dict.value(forKey: "frontviewphoto") as? String != nil && dict.value(forKey: "frontviewphoto") as? String != ""{
                           nameBoardImg.af_setImage(withURL: URL(string: dict.value(forKey: "frontviewphoto") as! String)!)
                           nameBoardImgBOOL = true
                       }
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadData"), object: nil)
          
        }
      
        @available(iOS 11.0, *)
        func logout(){
            let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                 controller.modalPresentationStyle = .fullScreen
                 controller.modalTransitionStyle = .coverVertical
            present(controller, animated: true, completion: nil)
        }
    }


