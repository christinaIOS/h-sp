//
//  myInfoViewController.swift
//  h+SP
//
//  Created by mirrorminds on 13/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import DatePickerDialog
import DropDown
import Alamofire
import KVSpinnerView
import AlamofireImage
import GooglePlaces
import AMShimmer

class MyInfo5ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate {
    
    var activeField: UITextField?
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var MenuBtn: UIButton!
    @IBOutlet weak var genralBtn: UIButton!
    @IBOutlet weak var clinicBtn: UIButton!
    @IBOutlet weak var tabView: UIView!
    //clinic
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var clinicTf: UITextField!
    @IBOutlet weak var adrsTf: UITextField!
    @IBOutlet weak var consDurTf: UITextField!
    @IBOutlet weak var mornTblView: UITableView!
    @IBOutlet weak var noonTblView: UITableView!
    @IBOutlet weak var eveTblView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var monimg: UIImageView!
    @IBOutlet weak var tueimg: UIImageView!
    @IBOutlet weak var wedimg: UIImageView!
    @IBOutlet weak var thuimg: UIImageView!
    @IBOutlet weak var friimg: UIImageView!
    @IBOutlet weak var satimg: UIImageView!
    @IBOutlet weak var sunimg: UIImageView!
    
    @IBOutlet var btnDays: [UIButton]!
    
   
    @IBOutlet weak var deg2btn: UIButton!
    @IBOutlet weak var deg3btn: UIButton!
    @IBOutlet weak var deg12btn: UIButton!
    @IBOutlet weak var deg13btn: UIButton!
    
    
    //FOR NURSE CLINIC
    @IBOutlet weak var nurseScrollView: UIScrollView!
    @IBOutlet weak var nurMorTblView: UITableView!
    @IBOutlet weak var nurNoonTblView: UITableView!
    @IBOutlet weak var nurEveTblView: UITableView!
    @IBOutlet weak var nurMonImgView: UIImageView!
    @IBOutlet weak var nurTueImgView: UIImageView!
    @IBOutlet weak var nurWedImgView: UIImageView!
    @IBOutlet weak var nurThuImgView: UIImageView!
    @IBOutlet weak var nurFriImgView: UIImageView!
    @IBOutlet weak var nurSatImgView: UIImageView!
    @IBOutlet weak var nurSunImgView: UIImageView!
    
    var exDic = ["a":"b"]
    var monBool = false
    var tueBool = false
    var wedBool = false
    var thuBool = false
    var friBool = false
    var satBool = false
    var sunBool = false
    var accType = ""
    var btnSelect = ""
    
    //genral
    @IBOutlet weak var mailTf: UITextField!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var dobTf: UITextField!
    @IBOutlet weak var ugQualtf: UITextField!
    @IBOutlet weak var pgSpecialityTf: UITextField!
    @IBOutlet weak var pgQualtf: UITextField!
    @IBOutlet weak var thirdDegtf: UITextField!
    @IBOutlet weak var masSpecialityTf: UITextField!
    @IBOutlet weak var expeTf: UITextField!
    @IBOutlet weak var consultFeetf: UITextField!
    @IBOutlet weak var housFeetf: UITextField!
    @IBOutlet weak var aadarNoTf: UITextField!
    @IBOutlet weak var docImgView: UIImageView!
    @IBOutlet weak var campImg: UIImageView!
    @IBOutlet weak var consulImg: UIImageView!
    @IBOutlet weak var houseImg: UIImageView!
    @IBOutlet weak var registerLbl: UILabel!
    @IBOutlet weak var mobileTf: UITextField!
    @IBOutlet weak var genderTf: UITextField!
    @IBOutlet weak var videoCallFeeTf: UITextField!
    @IBOutlet weak var zeroDealayConsultFeeTf: UITextField!
    @IBOutlet weak var aadharImgView: UIImageView!
    @IBOutlet weak var consultChooseHeightCons: NSLayoutConstraint!
    @IBOutlet weak var consultFeeHeightCons: NSLayoutConstraint!
    @IBOutlet weak var houseChooseHeightCons: NSLayoutConstraint!
    @IBOutlet weak var houseFeeHeightCons: NSLayoutConstraint!
    @IBOutlet weak var SpecialityHeightCons1: NSLayoutConstraint!
    @IBOutlet weak var SpecialityHeightCons2: NSLayoutConstraint!
    @IBOutlet weak var videoConsultHeightCons: NSLayoutConstraint!
    @IBOutlet weak var deg2HeightCons: NSLayoutConstraint!
    @IBOutlet weak var deg3HeightCons: NSLayoutConstraint!
    @IBOutlet weak var consultToCons: NSLayoutConstraint!
    @IBOutlet weak var houVisitLbl: UILabel!
    @IBOutlet weak var clinicConTf: UITextField!
    @IBOutlet weak var addImgLbl: UILabel!
    @IBOutlet weak var covidYesBtn: UIButton!
    @IBOutlet weak var covidNoBtn: UIButton!
    @IBOutlet weak var cancellationFeeYesBtn: UIButton!
    @IBOutlet weak var cancellationFeeNoBtn: UIButton!
    
    //BAnk
    @IBOutlet weak var bankNameTf: UITextField!
    @IBOutlet weak var accholdNametf: UITextField!
    @IBOutlet weak var acctyeTf: UITextField!
    @IBOutlet weak var accnoTf: UITextField!
    @IBOutlet weak var brancNameTf: UITextField!
    @IBOutlet weak var ifscTf: UITextField!
    @IBOutlet weak var toastLbl: UIButton!
    var imagePicker = UIImagePickerController()
    
    var fromView = ""
    var selectSection = ""
    var camBool = false
    var consulBool = false
    var houseBool = true
    var imgRef = 0
    var houseVisitFee = ""
    var consFee = ""
    var cancellationFee = ""
    var covidStatus = ""
    var bankNameArr = [NSDictionary]()
    //time data
    var addClinincTimingArr = [NSMutableDictionary]()
    var morningArr = [NSDictionary]()
    var noonArr = [NSDictionary]()
    var eveArr = [NSDictionary]()
    var selectedDaysArr = ["mon","","","","","",""]
    var gender = ""
    var monIDArr = [Int]()
    var TueIDArr = [Int]()
    var wedIDArr = [Int]()
    var thuIDArr = [Int]()
    var friIDArr = [Int]()
    var satIDArr = [Int]()
    var sunIDArr = [Int]()
    
    var clinicListArr = [NSDictionary]()
    
     //FOR NURSE
     var nurTimeListArr = [NSDictionary]()
     var nurMornArr = [NSDictionary]()
     var nurNoonArr = [NSDictionary]()
     var nurEveArr = [NSDictionary]()
     var nurMonIDStr = ""
     var nurTueIDStr = ""
     var nurWedIDStr = ""
     var nurThuIDStr = ""
     var nurFriIDStr = ""
     var nurSatIDStr = ""
     var nurSunIDStr = ""
      
    //cell day selection
    var cellSelectedDay = "0 M"
    var nurSelectedDay = "M"
    
    
    //FOR DEGREE
    let ugDD = DropDown()
    let pgDD = DropDown()
    let thridDegDD = DropDown()
    let pgSpeacilityDD = DropDown()
    let masterSpeacilityDD = DropDown()
    let genderDD = DropDown()
    let acctyeDD = DropDown()
    let houseFeeDD = DropDown()
    let bankNameDD = DropDown()
    
    var ugArr = [NSDictionary]()
    var pgArr = [NSDictionary]()
    var thirdArr = [NSDictionary]()
    var pgSpeacilityArr = [NSDictionary]()
    var masterSpeacilityArr = [NSDictionary]()
    
    var ugID = ""
    var pgID = ""
    var thirdDegID = ""
    var pgSpeacilityID = ""
    var masterSpeacilityID = ""
    var bankNameID = ""
    
    var docCategory = ""
    var reisterImg : UIImage!
    var aadharImg : UIImage!
    var aadharImgBOOL = false
    var profilepicBool = false
    var lat = ""
    var long = ""
    
    //EDIT SLOT
    var editMorArr = [NSDictionary]()
    var editNoonArr = [NSDictionary]()
    var editEveArr = [NSDictionary]()
    
    var level1 = ""
    var level2 = ""
    var adminVerify = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSetU()
        ProfileAPICall()
        bankNameAPICall()
        
    }
    
    @available(iOS 11.0, *)
    func viewSetU(){
        

        self.aadarNoTf.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

       tap.cancelsTouchesInView = false

        view.addGestureRecognizer(tap)
        imagePicker.delegate = self
        docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
        
        //loader
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
        KVSpinnerView.settings.statusTextColor = UIColor.darkGray
        KVSpinnerView.settings.tintColor = UIColor.darkGray
        
        Constant.addStatusBar(view: self.view)
        if fromView == "dash" {
            MenuBtn.isHidden = true
            backBtn.isHidden = false
            
        }else {
            MenuBtn.isHidden = false
            backBtn.isHidden = true
            gestur()
        }
        
        scrollView.isHidden = false
        tblView.isHidden = true

       tabView.layer.cornerRadius = 10
        centerView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 60 : 80))
        if docCategory == "NUR" {
            consultFeeHeightCons.constant = 0
            consultFeetf.isHidden = true
            zeroDealayConsultFeeTf.isHidden = true
            consultChooseHeightCons.constant = 0
            houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 50 : 65)
            houseChooseHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            clinicBtn.setTitle("Visiting Hours", for: .normal)
            campImg.superview?.isHidden = true
            consulImg.superview?.isHidden = true
            videoConsultHeightCons.constant = 0
            
            
            houseImg.image = UIImage(named: "checkBox")
           
            houseBool = true
            housFeetf.isHidden  = false
            houVisitLbl.isHidden = false
            
        }else if  docCategory == "DEN" {
            houseFeeHeightCons.constant = 0
            houseChooseHeightCons.constant = 0
            deg2HeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            deg3HeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            houseImg.superview?.isHidden = true
            campImg.superview?.isHidden = false
            clinicBtn.setTitle("CLINIC", for: .normal)
            videoConsultHeightCons.constant = 40
            consultToCons.constant = -50
        }else{
            deg2HeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            deg3HeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 90 : 120)
            consultFeetf.isHidden = false
            zeroDealayConsultFeeTf.isHidden = false
            consultChooseHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 50 : 65)
            houseChooseHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            consulImg.superview?.isHidden = false
            campImg.superview?.isHidden = false
            clinicBtn.setTitle("CLINIC", for: .normal)
            videoConsultHeightCons.constant = 40
        }
        SpecialityHeightCons1.constant = -10
        SpecialityHeightCons2.constant = -10
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
            if UserDefaults.standard.value(forKey: "profileEdit") as? String == "N" {
                self.view.makeToast("Please update your profile.", duration: 1.0, position: .center)
                ProfileAPICall()
                  toastLbl.isHidden = true
                addImgLbl.isHidden = false
                                  genralBtn.setTitleColor(UIColor.white, for: .normal)
                                  clinicBtn.setTitleColor(UIColor.black, for: .normal)
                                  genralBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                  clinicBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                                  scrollView.isHidden = false
                                  tblView.isHidden = true
                                  addBtn.isHidden = true
                                  nurseScrollView.isHidden = true
                clinicBtn.isEnabled = false
            }else if UserDefaults.standard.value(forKey: "profileEdit") as? String == "C" {
                //  AMShimmer.start(for: tblView)
                addImgLbl.isHidden = true
                self.view.makeToast("Please update your clinic Details.", duration: 1.0, position: .center)
                clinicBtn.isEnabled = true
                clinicBtn.setTitleColor(UIColor.white, for: .normal)
                genralBtn.setTitleColor(UIColor.black, for: .normal)
                clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                genralBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
           
                 clinicBtn.setTitle("Clinic", for: .normal)
                    scrollView.isHidden = true
                    nurseScrollView.isHidden = true
                    tblView.isHidden = false
                    addBtn.isHidden = false
                     clinicAPICall()
                
            }else if UserDefaults.standard.value(forKey: "profileEdit") as? String == "V" {
                if docCategory == "NUR" {
                    addImgLbl.isHidden = true
                                   self.view.makeToast("Please update your visiting  hours.", duration: 1.0, position: .center)
                                   clinicBtn.isEnabled = true
                                   clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                   genralBtn.setTitleColor(UIColor.black, for: .normal)
                                   clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                   genralBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                               clinicBtn.setTitle("Visiting Hours", for: .normal)
                                  scrollView.isHidden = true
                                  tblView.isHidden = true
                                  nurseScrollView.isHidden = false
                                  addBtn.isHidden = true
                                  getNurseTimeAPICall()
                               
                              }
            }else {
                addImgLbl.isHidden = true
                toastLbl.isHidden = true
            }
        }

    }
    
  @objc func dismissKeyboard() {
      view.endEditing(true)
  }
    
    func gestur(){
        let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
        left.direction = .left
        self.view.addGestureRecognizer(left)
        
        let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
        right.direction = .right
        self.view.addGestureRecognizer(right)
    }
    
    @objc func Swipe(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    
    func resizeImage(image: UIImage) -> UIImage {
        
        var newHeight = 1024 as CGFloat
        var newWidth = 1024 as CGFloat
        
        if image.size.width > image.size.height {
            let scale = newWidth / image.size.width
            newHeight = image.size.height * scale
        }
        else if image.size.width < image.size.height {
            let scale = newHeight / image.size.height
            newWidth = image.size.width * scale
        }
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        print(newImage!)
        return newImage!
    }
    private func textFieldShouldClear(textField: UITextField) -> Bool {
        textField.text = ""
        textField.resignFirstResponder()
        return false
    }
    //MARK:- setting droDowns
    
    func bankNameDropDown() {
         var bankNameStrArr = [String]()
               for i in 0..<bankNameArr.count {
                   let dict = bankNameArr[i]
                   bankNameStrArr.append(dict.value(forKey: "bankname") as! String)
               }
               bankNameDD.anchorView = bankNameTf
               bankNameDD.topOffset = CGPoint(x: 0, y: bankNameTf.bounds.height + 10)
               let joinedString = bankNameStrArr
               bankNameDD.dataSource = joinedString
               bankNameDD.selectionAction = { [unowned self] (index, item) in
                   self.bankNameTf.text = item
                   self.bankNameID = "\(self.bankNameArr[index].value(forKey: "id") as! NSNumber)"
                  // self.SpecialityHeightCons2.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
                  // self.deg13btn.isHidden = false
               }
               bankNameDD.show()
    }
    
    func setupHouseFeeTyeDropDown() {
        let accTye = ["500","1000","1500","2000","2500","3000"]
        houseFeeDD.anchorView = housFeetf
        houseFeeDD.topOffset = CGPoint(x: 0, y: housFeetf.bounds.height)
        let joinedString = accTye
        houseFeeDD.dataSource = joinedString
        houseFeeDD.selectionAction = { [unowned self] (index, item) in
            self.housFeetf.text = item
        }
        houseFeeDD.show()
    }
    
    func setupAccTyeDropDown() {
        let accTye = ["SAVINGS","CURRENT"]
        acctyeDD.anchorView = acctyeTf
        acctyeDD.topOffset = CGPoint(x: 0, y: acctyeTf.bounds.height)
        let joinedString = accTye
        acctyeDD.dataSource = joinedString
        acctyeDD.selectionAction = { [unowned self] (index, item) in
            self.acctyeTf.text = item
            self.accType = item
        }
        acctyeDD.show()
    }
    func setupUGDropDown() {
        var ugStrArr = [String]()
        for i in 0..<ugArr.count {
            let dict = ugArr[i]
            ugStrArr.append(dict.value(forKey: "deg_degree") as! String)
        }
        ugDD.anchorView = ugQualtf
        ugDD.topOffset = CGPoint(x: 0, y: ugQualtf.bounds.height )
        let joinedString = ugStrArr
        ugDD.dataSource = joinedString
        ugDD.selectionAction = { [unowned self] (index, item) in
            self.ugQualtf.text = item
            self.ugID = "\(self.ugArr[index].value(forKey: "deg_id") as! NSNumber)"
        }
        ugDD.show()
    }
    
    func setuppGDropDown() {
        var pgStrArr = [String]()
        for i in 0..<pgArr.count {
            let dict = pgArr[i]
            pgStrArr.append(dict.value(forKey: "deg_degree") as! String)
        }
        pgDD.anchorView = pgQualtf
        pgDD.topOffset = CGPoint(x: 0, y: pgQualtf.bounds.height)
        let joinedString = pgStrArr
        pgDD.dataSource = joinedString
        pgDD.selectionAction = { [unowned self] (index, item) in
            self.pgQualtf.text = item
            self.pgID = "\(self.pgArr[index].value(forKey: "deg_id") as! NSNumber)"
            self.SpecialityHeightCons1.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
            //self.deg12btn.isHidden = false
        }
        pgDD.show()
    }
    
    func thirdDegDropDown() {
        var thridDegStrArr = [String]()
        for i in 0..<thirdArr.count {
            let dict = thirdArr[i]
            thridDegStrArr.append(dict.value(forKey: "deg_degree") as! String)
        }
        thridDegDD.anchorView = thirdDegtf
        thridDegDD.topOffset = CGPoint(x: 0, y: thirdDegtf.bounds.height + 10)
        let joinedString = thridDegStrArr
        thridDegDD.dataSource = joinedString
        thridDegDD.selectionAction = { [unowned self] (index, item) in
            self.thirdDegtf.text = item
            self.thirdDegID = "\(self.thirdArr[index].value(forKey: "deg_id") as! NSNumber)"
            self.SpecialityHeightCons2.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
           // self.deg13btn.isHidden = false
        }
        thridDegDD.show()
    }
    
    func setuppgSpeacilityDropDown() {
        var pgSpeacilityStrArr = [String]()
        for i in 0..<pgSpeacilityArr.count {
            let dict = pgSpeacilityArr[i]
            pgSpeacilityStrArr.append(dict.value(forKey: "desp_speciality") as! String)
        }
        pgSpeacilityDD.anchorView = pgSpecialityTf
        pgSpeacilityDD.topOffset = CGPoint(x: 0, y: pgSpecialityTf.bounds.height + 10)
        let joinedString = pgSpeacilityStrArr
        pgSpeacilityDD.dataSource = joinedString
        pgSpeacilityDD.selectionAction = { [unowned self] (index, item) in
            self.pgSpecialityTf.text = item
            self.pgSpeacilityID = "\(self.pgSpeacilityArr[index].value(forKey: "desp_id") as! NSNumber)"
        }
        pgSpeacilityDD.show()
    }
    
    func masterSpecialityDropDown() {
        var masterStrArr = [String]()
        for i in 0..<masterSpeacilityArr.count {
            let dict = masterSpeacilityArr[i]
            masterStrArr.append(dict.value(forKey: "desp_speciality") as! String)
        }
        masterSpeacilityDD.anchorView = masSpecialityTf
        masterSpeacilityDD.topOffset = CGPoint(x: 0, y: masSpecialityTf.bounds.height + 10)
        let joinedString = masterStrArr
        masterSpeacilityDD.dataSource = joinedString
        masterSpeacilityDD.selectionAction = { [unowned self] (index, item) in
            self.masSpecialityTf.text = item
            self.masterSpeacilityID = "\(self.masterSpeacilityArr[index].value(forKey: "desp_id") as! NSNumber)"
        }
        masterSpeacilityDD.show()
    }
    func setuGenderDD() {
        let genderStrArr = ["MALE","FEMALE","OTHERS"]
        genderDD.anchorView = genderTf
        genderDD.topOffset = CGPoint(x: 0, y: thirdDegtf.bounds.height + 10)
        let joinedString = genderStrArr
        genderDD.dataSource = joinedString
        genderDD.selectionAction = { [unowned self] (index, item) in
            self.genderTf.text = item
            self.gender = String("\(self.genderTf.text!)".prefix(1))
            
        }
        genderDD.show()
    }
    
    // MARK: - tf delegates
    
    @objc func didChangeText(textField:UITextField) {
          textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
      }
      func modifyCreditCardString(creditCardString : String) -> String {
          let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()

          let arrOfCharacters = Array(trimmedString)
          var modifiedCreditCardString = ""

          if(arrOfCharacters.count > 0) {
              for i in 0...arrOfCharacters.count-1 {
                  modifiedCreditCardString.append(arrOfCharacters[i])
                  if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                      modifiedCreditCardString.append(" ")
                  }
              }
          }
          return modifiedCreditCardString
      }
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if textField == aadarNoTf {
              let newLength = (textField.text ?? "").count + string.count - range.length
                      if(textField == aadarNoTf) {
                          return newLength <= 14
                      }
          }
        if textField == consDurTf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == consDurTf) {
                        return newLength <= 2
                    }
        }
        if textField == expeTf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == expeTf) {
                        return newLength <= 2
                    }
        }
        if textField == clinicConTf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == clinicConTf) {
                        return newLength <= 10
                    }
        }
        if textField == consultFeetf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == consultFeetf) {
                        return newLength <= 6
                    }
        }
        if textField == housFeetf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == housFeetf) {
                        return newLength <= 6
                    }
        }
        
        if textField == housFeetf {
            let newLength = (textField.text ?? "").count + string.count - range.length
                    if(textField == housFeetf) {
                        return newLength <= 6
                    }
        }
        
        if textField == brancNameTf {
                   let newLength = (textField.text ?? "").count + string.count - range.length
                           if(textField == brancNameTf) {
                               return newLength <= 10
                           }
               }
        
           return true
      }
    
      var keyboardSizeCopy = CGRect(x: 0, y: 0, width: 0, height: 0)
     
    @objc func keyboardWillHide(notification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillShow(notification: Notification) {
        guard let keyboardFrame: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        scrollView.contentInset.bottom = keyboardFrame.height
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == acctyeTf {
            textField.resignFirstResponder()
            setupAccTyeDropDown()
            return false
        }
        if  textField == housFeetf {
            textField.resignFirstResponder()
            setupHouseFeeTyeDropDown()
            return false
        }
      
         if textField == dobTf {
             let currentDate = Date()
            DatePickerDialog().show("Date Of Birth", doneButtonTitle: "OK", cancelButtonTitle: "CANCEL",maximumDate: currentDate, datePickerMode: .date) { (date) in
                if let dt = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    self.dobTf.text = formatter.string(from: dt)
                }
            }
            return false
        }else if textField == ugQualtf {
            textField.resignFirstResponder()
            setupUGDropDown()
            return false
        }else if textField == pgQualtf {
            textField.resignFirstResponder()
            setuppGDropDown()
            return false
        }else if textField == pgSpecialityTf {
            if pgID != "" {
                specialityAPICall(Id: pgID,type: "pg")
            }else {
                self.view.makeToast("Select your pg degree", duration: 2.0, position: .center)
            }
            return false
        }else if textField == thirdDegtf {
            
            textField.resignFirstResponder()
            thirdDegDropDown()
            return false
        }else if textField == masSpecialityTf{
            if thirdDegID != "" {
                specialityAPICall(Id: thirdDegID,type: "master")
            }else {
                self.view.makeToast("Select your master degree", duration: 2.0, position: .center)
            }
            return false
        }else if textField == adrsTf {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            autocompleteController.modalPresentationStyle = .fullScreen
            self.present(autocompleteController, animated: true, completion: nil)
            return false
        }else if textField == genderTf {
            textField.resignFirstResponder()
            setuGenderDD()
            return false
        }
        if textField != clinicTf && textField  != adrsTf && textField != consDurTf {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSizeCopy.height/1.5
            }
        }
        if  textField == bankNameTf {
            textField.resignFirstResponder()
                  bankNameDropDown()
                  return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - btnActions
    
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    
    @IBAction func menuAction(_ sender: UIButton) {
        self.view.endEditing(true)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    @IBAction func clearAction(_ sender: UIButton) {
     
        if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
           if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
            if sender.tag == 0 {
                ugQualtf.text = ""
            }else if sender.tag == 1 {
                pgQualtf.text = ""
                self.SpecialityHeightCons1.constant = 0
            }else if sender.tag == 2 {
                thirdDegtf.text = ""
                self.SpecialityHeightCons2.constant = 0
            }else if sender.tag == 3 {
                pgSpecialityTf.text = ""
            }else if sender.tag == 4 {
                masSpecialityTf.text = ""
            }else if sender.tag == 5 {
                acctyeTf.text = ""
            }
           }else{
            }
        }
        
    }
    
    @IBAction func nurseSlotSubmitAction(_ sender: UIButton) {
          nurMonIDStr = ""
          nurThuIDStr = ""
          nurWedIDStr = ""
          nurThuIDStr = ""
          nurFriIDStr = ""
          nurSatIDStr = ""
          nurSunIDStr = ""
          
          for i in 0..<nurMornArr.count {
                        let dict = nurMornArr[i]
                        if dict.value(forKey: "monday") as! String == "Y" {
                            nurMonIDStr = nurMonIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                      if dict.value(forKey: "tuesday") as! String == "Y" {
                            nurTueIDStr = nurTueIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                       if dict.value(forKey: "wednesday") as! String == "Y" {
                            nurWedIDStr = nurWedIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                       if dict.value(forKey: "thursday") as! String == "Y" {
                            nurThuIDStr = nurThuIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                       if dict.value(forKey: "friday") as! String == "Y" {
                            nurFriIDStr = nurFriIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                       if dict.value(forKey: "saturday") as! String == "Y" {
                            nurSatIDStr = nurSatIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                       if dict.value(forKey: "sunday") as! String == "Y" {
                            nurSunIDStr = nurSunIDStr + ",\(dict.value(forKey: "mtid") as! Int)"
                        }
                    }
                    
                    for i in 0..<nurNoonArr.count {
                        let dict = nurNoonArr[i]
                        if dict.value(forKey: "monday") as! String == "Y" {
                           nurMonIDStr =  nurMonIDStr + ((nurMonIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "tuesday") as! String == "Y" {
                           nurTueIDStr =  nurTueIDStr + ((nurTueIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "wednesday") as! String == "Y" {
                           nurWedIDStr =  nurWedIDStr + ((nurWedIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "thursday") as! String == "Y" {
                           nurThuIDStr =  nurThuIDStr + ((nurThuIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "friday") as! String == "Y" {
                            nurFriIDStr =  nurFriIDStr + ((nurFriIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "saturday") as! String == "Y" {
                           nurSatIDStr =  nurSatIDStr + ((nurSatIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "sunday") as! String == "Y" {
                           nurSunIDStr =  nurSunIDStr + ((nurSunIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                    }
                    
                    for i in 0..<nurEveArr.count {
                        let dict = nurEveArr[i]
                        if dict.value(forKey: "monday") as! String == "Y" {
                           nurMonIDStr =  nurMonIDStr + ((nurMonIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                            if dict.value(forKey: "tuesday") as! String == "Y" {
                           nurTueIDStr =  nurTueIDStr + ((nurTueIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "wednesday") as! String == "Y" {
                           nurWedIDStr =  nurWedIDStr + ((nurWedIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "thursday") as! String == "Y" {
                           nurThuIDStr =  nurThuIDStr + ((nurThuIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "friday") as! String == "Y" {
                            nurFriIDStr =  nurFriIDStr + ((nurFriIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "saturday") as! String == "Y" {
                           nurSatIDStr =  nurSatIDStr + ((nurSatIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                        if dict.value(forKey: "sunday") as! String == "Y" {
                           nurSunIDStr =  nurSunIDStr + ((nurSunIDStr == "") ? (dict.value(forKey: "mtid") as! NSNumber).stringValue : ("," + (dict.value(forKey: "mtid") as! NSNumber).stringValue))
                        }
                    }
          
//          if nurMonIDStr.count != 0 &&  nurTueIDStr.count != 0 && nurWedIDStr.count != 0 && nurThuIDStr.count != 0 && nurFriIDStr.count != 0 && nurSatIDStr.count != 0 && nurSunIDStr.count != 0 {
//
//          }else{
//              self.view.makeToast("please select your available timings", duration: 2.0, position: .center)
//          }
         nurEditSlotAPICall()
      }
      @IBAction func nurseDatSelectAction(_ sender: UIButton) {
          if sender.tag == 0 {
              nurMonImgView.image = UIImage(named: "filledCircle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "M"
          }else if sender.tag == 1 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "filledCircle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "Tu"
          }else if sender.tag == 2 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "filledCircle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "W"
          }else if sender.tag == 3 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "filledCircle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "Th"
          }else if sender.tag == 4 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "filledCircle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "F"
          }else if sender.tag == 5 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "filledCircle")
              nurSunImgView.image = UIImage(named: "circle")
              nurSelectedDay = "Sa"
          }else if sender.tag == 6 {
              nurMonImgView.image = UIImage(named: "circle")
              nurTueImgView.image = UIImage(named: "circle")
              nurWedImgView.image = UIImage(named: "circle")
              nurThuImgView.image = UIImage(named: "circle")
              nurFriImgView.image = UIImage(named: "circle")
              nurSatImgView.image = UIImage(named: "circle")
              nurSunImgView.image = UIImage(named: "filledCircle")
              nurSelectedDay = "Su"
          }
          self.nurMorTblView.reloadData()
          self.nurNoonTblView.reloadData()
          self.nurEveTblView.reloadData()
      }
      
    
    @IBAction func nextAction(_ sender: UIButton) {
        infoValidation()
    }
    
    @IBAction func serviceSelectAction(_ sender: UIButton) {
        if sender.tag == 0 {
            if camBool == false {
                campImg.image = UIImage(named: "checkBox")
                camBool = true
                videoConsultHeightCons.constant = 40
            }else{
                camBool = false
                campImg.image = UIImage(named: "unCheck")
                videoConsultHeightCons.constant = 0
            }
        }else if sender.tag == 1 {
            if consulBool == false {
                consulImg.image = UIImage(named: "checkBox")
                consulBool = true
                consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 90 : 120)
                consultFeetf.isHidden = false
                zeroDealayConsultFeeTf.isHidden = false
            }else{
                consulBool = false
                consulImg.image = UIImage(named: "unCheck")
                consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 0 : 0)
                consultFeetf.isHidden = true
                zeroDealayConsultFeeTf.isHidden = true
                consultFeetf.text = ""
            }
        }else if sender.tag == 2 {
            if houseBool == false {
                houseImg.image = UIImage(named: "checkBox")
                houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 55 : 65)
                houseBool = true
                housFeetf.isHidden  = false
                houVisitLbl.isHidden = false
            }else{
                houseBool = false
                houseImg.image = UIImage(named: "unCheck")
                houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 0 : 0)
                houVisitLbl.isHidden = true
                 housFeetf.isHidden  = true
                housFeetf.text = ""
            }
        }
    }
    
    @IBAction func daySelectAction(_ sender: UIButton) {
        if sender.tag == 0 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                monimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                monimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 1 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                tueimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                tueimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 2 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                wedimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                wedimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 3 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                thuimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                thuimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 4 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                friimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                friimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 5 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                satimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                satimg.image = UIImage(named: "unCheck")
            }
        }else if sender.tag == 6 {
            if addClinincTimingArr[sender.tag].value(forKey: "isSelect") as! Bool == false {
                addClinincTimingArr[sender.tag].setValue(true, forKey: "isSelect")
                sunimg.image = UIImage(named: "checkBox")
            }else{
                addClinincTimingArr[sender.tag].setValue(false, forKey: "isSelect")
                sunimg.image = UIImage(named: "unCheck")
            }
        }
    }
    
    @IBAction func tabAction(_ sender: UIButton) {
        if sender.tag == 0 {
            ProfileAPICall()
            toastLbl.isHidden = true
                   genralBtn.setTitleColor(UIColor.white, for: .normal)
                   clinicBtn.setTitleColor(UIColor.black, for: .normal)
                   genralBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                   clinicBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                   scrollView.isHidden = false
                   tblView.isHidden = true
                   addBtn.isHidden = true
                   nurseScrollView.isHidden = true
               }else if sender.tag == 1 {
                 //  AMShimmer.start(for: tblView)
                   clinicBtn.setTitleColor(UIColor.white, for: .normal)
                   genralBtn.setTitleColor(UIColor.black, for: .normal)
                   clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                   genralBtn.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
                   if docCategory == "NUR" {
                    clinicBtn.setTitle("Visiting Hours", for: .normal)
                       scrollView.isHidden = true
                       tblView.isHidden = true
                       nurseScrollView.isHidden = false
                       addBtn.isHidden = true
                       getNurseTimeAPICall()
                    clinicBtn.setTitle("Visiting Hours", for: .normal)
                   }else{
                    clinicBtn.setTitle("Clinic", for: .normal)
                       scrollView.isHidden = true
                       nurseScrollView.isHidden = true
                       tblView.isHidden = false
                       addBtn.isHidden = false
                        clinicAPICall()
                   }
                  
                  
               }
    }
    
    @IBAction func AddAction(_ sender: UIButton) {
        clinicTf.text = ""
        adrsTf.text = ""
        consDurTf.text = ""
        clinicConTf.text = ""
        timingAPICall()
        hideView.isHidden = false
    }
    
    @IBAction func hideViewClose(_ sender: UIButton) {
        self.view.endEditing(true)
        hideView.isHidden = true
    }
    func addOtherDays() {
      var isDayBending = false
      var intDayBending = 0
       
      for i in 0..<btnDays.count {
        if (addClinincTimingArr[btnDays[i].tag].value(forKey: "isSelect") as! Bool == true) {
          if (btnDays[i].titleLabel!.textColor != UIColor.lightGray) {
            btnDays[i].setTitleColor(UIColor.lightGray, for: .normal)
            btnDays[i].isEnabled = false
             
            let dictDays = addClinincTimingArr[btnDays[i].tag]
            dictDays.setValue(morningArr, forKey: "morn")
            dictDays.setValue(noonArr, forKey: "noon")
            dictDays.setValue(eveArr, forKey: "even")
            self.addClinincTimingArr[btnDays[i].tag] = dictDays
          }
        }
        else {
          isDayBending = true
          intDayBending = btnDays[i].tag
           
          btnDays[i].setTitleColor(UIColor.darkText, for: .normal)
          btnDays[i].isEnabled = true
        }
      }
       
      if isDayBending == false {
        print("No bending day to add")
      }
      else {
        morningArr = addClinincTimingArr[intDayBending].value(forKey: "morn") as! [NSDictionary]
        noonArr = addClinincTimingArr[intDayBending].value(forKey: "noon") as! [NSDictionary]
        eveArr = addClinincTimingArr[intDayBending].value(forKey: "even") as! [NSDictionary]
         
        self.mornTblView.reloadData()
        self.noonTblView.reloadData()
        self.eveTblView.reloadData()
      }
    }

       
       func addSubmit() {
           for i in 0..<btnDays.count {
               if (addClinincTimingArr[btnDays[i].tag].value(forKey: "isSelect") as! Bool == true) {
                   if  (btnDays[i].titleLabel!.textColor != UIColor.lightGray) {
                       btnDays[i].setTitleColor(UIColor.lightGray, for: .normal)
                       btnDays[i].isEnabled = false
                       
                       let dictDays = addClinincTimingArr[btnDays[i].tag]
                       dictDays.setValue(morningArr, forKey: "morn")
                       dictDays.setValue(noonArr, forKey: "noon")
                       dictDays.setValue(eveArr, forKey: "even")
                       self.addClinincTimingArr[btnDays[i].tag] = dictDays
                   }
               }
           }
           
           for i in 0..<addClinincTimingArr.count {
               var timeIdArray = [Int]()
               
               let mornArr = addClinincTimingArr[i].value(forKey: "morn") as! [NSDictionary]
               let noonArr = addClinincTimingArr[i].value(forKey: "noon") as! [NSDictionary]
               let evenArr = addClinincTimingArr[i].value(forKey: "even") as! [NSDictionary]
               
               for j in 0..<mornArr.count {
                   if mornArr[j].value(forKey: "isSelect") as! String == "1" {
                       timeIdArray.append((Int(truncating: mornArr[j].value(forKey: "mt_id") as! NSNumber)))
                   }
               }
               for j in 0..<noonArr.count {
                   if noonArr[j].value(forKey: "isSelect") as! String == "1" {
                       timeIdArray.append((Int(truncating: noonArr[j].value(forKey: "mt_id") as! NSNumber)))
                   }
               }
               for j in 0..<evenArr.count {
                   if evenArr[j].value(forKey: "isSelect") as! String == "1" {
                       timeIdArray.append((Int(truncating: evenArr[j].value(forKey: "mt_id") as! NSNumber)))
                   }
               }
               
               if addClinincTimingArr[i].value(forKey: "day") as! String == "monday" {
                   monIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "tuesday" {
                   TueIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "wednesday" {
                   wedIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "thursday" {
                   thuIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "friday" {
                   friIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "saturday" {
                   satIDArr = timeIdArray
               }
               else if addClinincTimingArr[i].value(forKey: "day") as! String == "sunday" {
                   sunIDArr = timeIdArray
               }
           }
           
           if clinicTf.text != "" {
               if adrsTf.text != "" {
                   if consDurTf.text != "" {
                       if clinicConTf.text != "" {
                           createClinicPICall()
                       }else{
                           self.view.makeToast("please choose your clinic contact number", duration: 2.0, position: .center)
                       }
                   }else {
                       self.view.makeToast("please enter your consutation duration time", duration: 2.0, position: .center)
                   }
               }else{
                   self.view.makeToast("please enter your clinic Address", duration: 2.0, position: .center)
               }
           }else{
               self.view.makeToast("please enter your clinic name", duration: 2.0, position: .center)
           }
       }
    
    @IBAction func clinicAddSubmitAction(_ sender: UIButton) {
        //For morning slot
        
        let alertController = UIAlertController(title: "", message: "Do you want to add more days?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Add Other Days", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.addOtherDays()
        }
        let submitAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.addSubmit()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(submitAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func clinicEditSubmitAction(_ sender: UIButton) {
        editClinicAPICall(intSelect: sender.tag)
    }
    
    @IBAction func editprofileAction(_ sender: UIButton) {
        imgRef = sender.tag
        
        if sender.tag == 0 {
           if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
                if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                {
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = true
                    imagePicker.cameraDevice = .front
                    self.present(imagePicker, animated: true, completion: nil)
                }
                else
                {
                    let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
           }else{
            let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openCamera()
                
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                
            }
            
            // Add the actions
            imagePicker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            // Present the controller
            if UIDevice.current.userInterfaceIdiom == .phone {
                               self.present(alert, animated: true, completion: nil)
                          }else{
                              if let popoverController = alert.popoverPresentationController {
                                  popoverController.sourceView = sender
                                  popoverController.sourceRect = sender.bounds
                              }
                              self.present(alert, animated: true, completion: nil)
                          }

            }
        }else {
            let alert:UIAlertController=UIAlertController(title: "Take photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openCamera()
                
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                
            }
            
            // Add the actions
            imagePicker.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            // Present the controller
            if UIDevice.current.userInterfaceIdiom == .phone {
                               self.present(alert, animated: true, completion: nil)
                          }else{
                              if let popoverController = alert.popoverPresentationController {
                                  popoverController.sourceView = sender
                                  popoverController.sourceRect = sender.bounds
                              }
                              self.present(alert, animated: true, completion: nil)
                          }
        }
        
        
        
    }
     @IBAction func cancelationAction(_ sender: UIButton) {
        if sender.tag == 0 {
            cancellationFee = "Y"
            cancellationFeeNoBtn.setImage(UIImage(named: "circle"), for: .normal)
            cancellationFeeYesBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
        }else if sender.tag == 1 {
            cancellationFee = "N"
            cancellationFeeYesBtn.setImage(UIImage(named: "circle"), for: .normal)
            cancellationFeeNoBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
        }
    }
     @IBAction func covidStatusAction(_ sender: UIButton) {
        if sender.tag == 0 {
                   covidStatus = "Y"
                   covidNoBtn.setImage(UIImage(named: "circle"), for: .normal)
                   covidYesBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
        }else if sender.tag == 1 {
                   covidStatus = "N"
                   covidYesBtn.setImage(UIImage(named: "circle"), for: .normal)
                   covidNoBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
         }
    }
    //MARK:- imageickerDelegate
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        if imgRef == 0 {
            profilepicBool = true
            docImgView.image = selectedImage
        }else if imgRef == 1 {
            aadharImgBOOL = true
            aadharImg = selectedImage
            aadharImgView.image = selectedImage
        }else if imgRef == 2 {
            reisterImg = selectedImage
        }
        
        dismiss(animated: true, completion: nil)
        print(selectedImage.size.width)
        print(selectedImage.size.height)
    }
    
    
    
    // MARK: - tblView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return ((tableView == tblView) ? clinicListArr.count : 1)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            return (((btnSelect == "Visiting Hours") && (selectSection == "\(section)")) ? (2 + (clinicListArr[section].value(forKey: "slotdetail") as! [NSDictionary]).count) : 1)
        }else if tableView == mornTblView {
            return morningArr.count
        }else if tableView == noonTblView {
            return noonArr.count
        }else if tableView == eveTblView {
            return eveArr.count
        }else if tableView == nurMorTblView {
            return nurMornArr.count
        }else if tableView == nurNoonTblView {
            return nurNoonArr.count
        }else if tableView == nurEveTblView {
            return nurEveArr.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblView {
            if indexPath.row == 0 {
                if (btnSelect == "Edit") && (selectSection == "\(indexPath.section)") {
                    return (UIDevice.current.userInterfaceIdiom == .phone ? 500 : 600)
                }else{
                    return (UIDevice.current.userInterfaceIdiom == .phone ? 120 : 130)
                }
            }
            else if indexPath.row == 1 {
                return 70
            }
            else {
                let arraySlotDetail = (clinicListArr[indexPath.section].value(forKey: "slotdetail") as! [NSDictionary])
                let arraySlot = (arraySlotDetail[indexPath.row-2].value(forKey: "Slot") as! [NSDictionary])
                
                var predicate = NSPredicate(format: "mttype == %@", "M")
                let arrayMorning = arraySlot.filter { predicate.evaluate(with: $0) };
                
                predicate = NSPredicate(format: "mttype == %@", "A")
                let arrayNoon = arraySlot.filter { predicate.evaluate(with: $0) };
                
                predicate = NSPredicate(format: "mttype == %@", "E")
                let arrayEven = arraySlot.filter { predicate.evaluate(with: $0) };
                
                let countArray = [arrayMorning.count, arrayNoon.count, arrayEven.count];
                return CGFloat(((countArray.max()!) * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)) + 10)
            }
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone {
                return 35
            }else{
                return 45
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if tableView == tblView {
            if indexPath.row == 0 {
                var  cell: ClinicListingHeaderTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "ClinicListingHeaderTableViewCell") as? ClinicListingHeaderTableViewCell)
                let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "ClinicListingHeaderTableViewCell" : "ClinicListingHeaderTableViewCell_ipad"), owner: self, options: nil)!
                cell = (nib[0]  as? ClinicListingHeaderTableViewCell)!
                cell?.bgView.layer.shadowColor = UIColor.darkGray.cgColor
                cell?.bgView.layer.shadowOpacity = 1
                cell?.bgView.layer.shadowOffset = CGSize.zero
                cell?.bgView.layer.shadowRadius = 2
                cell?.bgView.layer.cornerRadius = 0
                
                cell?.nameLbl.text = clinicListArr[indexPath.section].value(forKey: "cliname") as? String
                cell?.adrsLbl.text = clinicListArr[indexPath.section].value(forKey: "cliaddress") as? String
                
                var timingArr = [NSDictionary] ()
                if clinicListArr[indexPath.section].value(forKey: "timing") as? [NSDictionary] != nil {
                    timingArr = clinicListArr[indexPath.section].value(forKey: "timing") as! [NSDictionary]
                }
                
                
                if cellSelectedDay == "\(indexPath.section) M" {
                    cell?.monImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.monImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) Tu" {
                    cell?.tueImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.tueImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) W" {
                    cell?.wedImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.wedImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) Th" {
                    cell?.thuImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.thuImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) F" {
                    cell?.friImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.friImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) Sa" {
                    cell?.satImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.satImg.image = UIImage(named: "circle")
                }
                if cellSelectedDay == "\(indexPath.section) Su" {
                    cell?.sunImg.image = UIImage(named: "filledCircle")
                }else{
                    cell?.sunImg.image = UIImage(named: "circle")
                }
                
                cell?.monBtn.tag = indexPath.section
                cell?.tueBtn.tag = indexPath.section
                cell?.wedBtn.tag = indexPath.section
                cell?.thuBtn.tag = indexPath.section
                cell?.friBtn.tag = indexPath.section
                cell?.satBtn.tag = indexPath.section
                cell?.sunBtn.tag = indexPath.section
                
                cell?.monBtn.addTarget(self, action: #selector(montabed(sender:)), for: .touchUpInside)
                cell?.tueBtn.addTarget(self, action: #selector(tuetabed(sender:)), for: .touchUpInside)
                cell?.wedBtn.addTarget(self, action: #selector(wedtabed(sender:)), for: .touchUpInside)
                cell?.thuBtn.addTarget(self, action: #selector(thutabed(sender:)), for: .touchUpInside)
                cell?.friBtn.addTarget(self, action: #selector(fritabed(sender:)), for: .touchUpInside)
                cell?.satBtn.addTarget(self, action: #selector(sattabed(sender:)), for: .touchUpInside)
                cell?.sunBtn.addTarget(self, action: #selector(suntabed(sender:)), for: .touchUpInside)
                
                self.editMorArr.removeAll()
                self.editNoonArr.removeAll()
                self.editEveArr.removeAll()
                
                for i in 0..<timingArr.count {
                    let dict = timingArr[i]
                    if dict.value(forKey: "mttype") as! String == "M" {
                        self.editMorArr.append(dict)
                    }else if dict.value(forKey: "mttype") as! String == "A" {
                        self.editNoonArr.append(dict)
                    }else if dict.value(forKey: "mttype") as! String == "E" {
                        self.editEveArr.append(dict)
                    }
                }
                
                print(self.editMorArr.count)
                print(self.editNoonArr.count)
                print(self.editEveArr.count)
                
                
                for i in 0..<editMorArr.count {
                    let MSV: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    MSV.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    MSV.frame.origin.x = (cell?.morSlotView.frame.origin.x)!
                    MSV.frame.size.width = (cell?.morSlotView.frame.size.width)!
                    MSV.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    
                    let timeStr = editMorArr[i].value(forKey: "mttiming") as! String
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
                    var timeStr2 = ""
                    if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                    }else {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                    }
                    MSV.lbl.text = timeStr1 + "-" + timeStr2
                    
                    if cellSelectedDay == "\(indexPath.section) M" {
                        if editMorArr[i].value(forKey: "monday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Tu" {
                        if editMorArr[i].value(forKey: "tuesday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) W" {
                        if editMorArr[i].value(forKey: "wednesday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    } else if cellSelectedDay == "\(indexPath.section) Th" {
                        if editMorArr[i].value(forKey: "thursday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) F" {
                        if editMorArr[i].value(forKey: "friday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Sa" {
                        if editMorArr[i].value(forKey: "saturday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Su" {
                        if editMorArr[i].value(forKey: "sunday") as! String == "Y" {
                            MSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            MSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    MSV.btn.tag = i
                    MSV.btn.accessibilityHint = "\(indexPath.section)"
                    MSV.btn.superview?.accessibilityHint = "M"
                    MSV.btn.addTarget(self, action: #selector(timeEditAction(sender:)), for: .touchUpInside)
                    cell?.morSlotView.addSubview(MSV)
                }
                
                for i in 0..<editNoonArr.count {
                    let NSV: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    NSV.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    NSV.frame.origin.x = (cell?.morSlotView.frame.origin.x)!
                    NSV.frame.size.width = (cell?.morSlotView.frame.size.width)!
                    NSV.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    
                    let timeStr = editNoonArr[i].value(forKey: "mttiming") as! String
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
                    let timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                    
                    NSV.lbl.text = timeStr1 + "-" + timeStr2
                    
                    if cellSelectedDay == "\(indexPath.section) M" {
                        if editNoonArr[i].value(forKey: "monday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Tu" {
                        if editNoonArr[i].value(forKey: "tuesday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) W" {
                        if editNoonArr[i].value(forKey: "wednesday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    } else if cellSelectedDay == "\(indexPath.section) Th" {
                        if editNoonArr[i].value(forKey: "thursday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) F" {
                        if editNoonArr[i].value(forKey: "friday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Sa" {
                        if editNoonArr[i].value(forKey: "saturday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Su" {
                        if editNoonArr[i].value(forKey: "sunday") as! String == "Y" {
                            NSV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            NSV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    NSV.btn.tag = i
                    NSV.btn.accessibilityHint = "\(indexPath.section)"
                    NSV.btn.superview?.accessibilityHint = "A"
                    NSV.btn.addTarget(self, action: #selector(timeEditAction(sender:)), for: .touchUpInside)
                    cell?.noonSlotView.addSubview(NSV)
                }
                
                for i in 0..<editEveArr.count {
                    let ESV: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    ESV.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    ESV.frame.origin.x = (cell?.morSlotView.frame.origin.x)!
                    ESV.frame.size.width = (cell?.morSlotView.frame.size.width)!
                    ESV.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    
                    let timeStr = editEveArr[i].value(forKey: "mttiming") as! String
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
                    var timeStr2 = ""
                    if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
                    }else {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
                    }
                    ESV.lbl.text = timeStr1 + "-" + timeStr2
                    
                    if cellSelectedDay == "\(indexPath.section) M" {
                        if editEveArr[i].value(forKey: "monday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Tu" {
                        if editEveArr[i].value(forKey: "tuesday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) W" {
                        if editEveArr[i].value(forKey: "wednesday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    } else if cellSelectedDay == "\(indexPath.section) Th" {
                        if editEveArr[i].value(forKey: "thursday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    else if cellSelectedDay == "\(indexPath.section) F" {
                        if editEveArr[i].value(forKey: "friday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Sa" {
                        if editEveArr[i].value(forKey: "saturday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }else if cellSelectedDay == "\(indexPath.section) Su" {
                        if editEveArr[i].value(forKey: "sunday") as! String == "Y" {
                            ESV.imgView.image = UIImage(named: "checkBox")
                        }else{
                            ESV.imgView.image = UIImage(named: "unCheck")
                        }
                    }
                    ESV.btn.tag = i
                    ESV.btn.accessibilityHint = "\(indexPath.section)"
                    ESV.btn.superview?.accessibilityHint = "E"
                    ESV.btn.addTarget(self, action: #selector(timeEditAction(sender:)), for: .touchUpInside)
                    cell?.eveSlotView.addSubview(ESV)
                }
                cell?.editBtn.tag = indexPath.section
                if (selectSection == "\(indexPath.section)") && (btnSelect == "Edit") {
                    cell?.dayView.isHidden = false
                    cell?.slotView.isHidden = false
                }else{
                    cell?.dayView.isHidden = true
                    cell?.slotView.isHidden = true
                }
                
                cell?.deleteBtn.tag = indexPath.section
                cell?.editBtn.addTarget(self, action: #selector(editAction(sender:)), for: .touchUpInside)
                cell?.deleteBtn.addTarget(self, action: #selector(deleteAction(sender:)), for: .touchUpInside)
                
                cell?.vistingHourBtn.tag = indexPath.section
                cell?.vistingHourBtn.addTarget(self, action: #selector(visitingHoursAction(sender:)), for: .touchUpInside)
                
                cell?.btnSubmit.tag = indexPath.section
                cell?.btnSubmit.addTarget(self, action: #selector(clinicEditSubmitAction(_:)), for: .touchUpInside)
                return cell!
            }
            else if indexPath.row == 1 {
                var  cell: ClinicListingHeaderTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "ClinicListingHeaderTableViewCell") as? ClinicListingHeaderTableViewCell)
                let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "ClinicListingHeaderTableViewCell" : "ClinicListingHeaderTableViewCell_ipad"), owner: self, options: nil)!
                cell = (nib[1]  as? ClinicListingHeaderTableViewCell)!
                
                return cell!
            }
            else  {
                var  cell: ClinicListingHeaderTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "ClinicListingHeaderTableViewCell") as? ClinicListingHeaderTableViewCell)
                let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "ClinicListingHeaderTableViewCell" : "ClinicListingHeaderTableViewCell_ipad"), owner: self, options: nil)!
                cell = (nib[2]  as? ClinicListingHeaderTableViewCell)!
                
                let arraySlotDetail = (clinicListArr[indexPath.section].value(forKey: "slotdetail") as! [NSDictionary])
                cell?.lblDays.text = arraySlotDetail[indexPath.row-2].value(forKey: "Day") as? String
                
                let arraySlot = (arraySlotDetail[indexPath.row-2].value(forKey: "Slot") as! [NSDictionary])
                
                var predicate = NSPredicate(format: "mttype == %@", "M")
                let arrayMorning = arraySlot.filter { predicate.evaluate(with: $0) };
                
                predicate = NSPredicate(format: "mttype == %@", "A")
                let arrayNoon = arraySlot.filter { predicate.evaluate(with: $0) };
                
                predicate = NSPredicate(format: "mttype == %@", "E")
                let arrayEven = arraySlot.filter { predicate.evaluate(with: $0) };
                
                for i in 0..<arrayMorning.count {
                    let timeView: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    timeView.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    timeView.frame.origin.x = 0
                    timeView.frame.size.width = (cell?.morSlotView.frame.size.width)!
                    timeView.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    var timeStr = ""
                    if  arrayMorning[i].value(forKey: "mttiming") as? String != nil {
                        timeStr = arrayMorning[i].value(forKey: "mttiming") as! String
                    }
                    
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + "AM"
                    var timeStr2 = ""
                    if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + "PM"
                    }else {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + "AM"
                    }
                    timeView.lbl.font =  timeView.lbl.font.withSize(8)
                    timeView.lbl.text = timeStr1 + "-" + timeStr2
                    timeView.imgView.image = UIImage(named: "checkBox")
                    cell?.morSlotView.addSubview(timeView)
                }
                
                for i in 0..<arrayNoon.count {
                    let timeView: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    timeView.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    timeView.frame.origin.x = 0
                    timeView.frame.size.width = (cell?.noonSlotView.frame.size.width)!
                    timeView.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    let timeStr = arrayNoon[i].value(forKey: "mttiming") as! String
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + "PM"
                    let timeStr2 = (timeStr.components(separatedBy: "-").last!) + "PM"
                    timeView.lbl.font =  timeView.lbl.font.withSize(8)
                    timeView.lbl.text = timeStr1 + "-" + timeStr2
                    timeView.imgView.image = UIImage(named: "checkBox")
                    cell?.noonSlotView.addSubview(timeView)
                }
                
                for i in 0..<arrayEven.count {
                    let timeView: timeSlotView = timeSlotView.instanceFromNib() as! timeSlotView
                    timeView.frame.size.height = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50)
                    timeView.frame.origin.x = 0
                    timeView.frame.size.width = (cell?.eveSlotView.frame.size.width)!
                    timeView.frame.origin.y = CGFloat(i * (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 50))
                    let timeStr = arrayEven[i].value(forKey: "mttiming") as! String
                    let timeStr1 = (timeStr.components(separatedBy: "-").first!) + "PM"
                    var timeStr2 = ""
                    if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + "AM"
                    }else {
                        timeStr2 = (timeStr.components(separatedBy: "-").last!) + "M"
                    }
                    timeView.lbl.font =  timeView.lbl.font.withSize(8)
                    timeView.lbl.text = timeStr1 + "-" + timeStr2
                    timeView.imgView.image = UIImage(named: "checkBox")
                    cell?.eveSlotView.addSubview(timeView)
                }
                
                return cell!
            }
        }
        else if tableView == mornTblView {
            let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as? slotCell)
            
            let timeStr = morningArr[indexPath.row].value(forKey: "mt_timings") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
            var timeStr2 = ""
            if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
            }else {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
            }
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
            
            if (morningArr[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                cell?.imgView.image = UIImage(named: "unCheck")
            }else {
                cell?.imgView.image = UIImage(named: "checkBox")
            }
            return cell!
        }else if tableView == noonTblView {
            let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell1") as? slotCell)
            
            let timeStr = noonArr[indexPath.row].value(forKey: "mt_timings") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
            let timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
            if (noonArr[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                cell?.imgView.image = UIImage(named: "unCheck")
            }else {
                cell?.imgView.image = UIImage(named: "checkBox")
            }
            return cell!
        }else if tableView == eveTblView {
            let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell2") as? slotCell)
            
            let timeStr = eveArr[indexPath.row].value(forKey: "mt_timings") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
            var timeStr2 = ""
            if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
            }else {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
            }
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
            
            
            if (eveArr[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                cell?.imgView.image = UIImage(named: "unCheck")
            }else {
                cell?.imgView.image = UIImage(named: "checkBox")
            }
            return cell!
        }else if tableView == nurMorTblView {
            let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as? slotCell)
            
            let timeStr = nurMornArr[indexPath.row].value(forKey: "mttiming") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " AM"
            var timeStr2 = ""
            if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
            }else {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
            }
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
           if nurSelectedDay == "M" {
              if  nurMornArr[indexPath.row].value(forKey: "monday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Tu" {
              if  nurMornArr[indexPath.row].value(forKey: "tuesday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "W" {
              if  nurMornArr[indexPath.row].value(forKey: "wednesday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Th" {
              if  nurMornArr[indexPath.row].value(forKey: "thursday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "F" {
              if  nurMornArr[indexPath.row].value(forKey: "friday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Sa" {
              if  nurMornArr[indexPath.row].value(forKey: "saturday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Su" {
              if  nurMornArr[indexPath.row].value(forKey: "sunday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }
            return cell!
        }else if tableView == nurNoonTblView {
            let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell1") as? slotCell)
            
            let timeStr = nurNoonArr[indexPath.row].value(forKey: "mttiming") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
            let timeStr2 = (timeStr.components(separatedBy: "-").last!) + " PM"
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
            if nurSelectedDay == "M" {
              if  nurNoonArr[indexPath.row].value(forKey: "monday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Tu" {
              if  nurNoonArr[indexPath.row].value(forKey: "tuesday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "W" {
              if  nurNoonArr[indexPath.row].value(forKey: "wednesday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Th" {
              if  nurNoonArr[indexPath.row].value(forKey: "thursday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "F" {
              if  nurNoonArr[indexPath.row].value(forKey: "friday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Sa" {
              if  nurNoonArr[indexPath.row].value(forKey: "saturday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }else if nurSelectedDay == "Su" {
              if  nurNoonArr[indexPath.row].value(forKey: "sunday") as! String == "Y" {
                cell?.imgView.image = UIImage(named:"checkBox")
              }
              else {
                 cell?.imgView.image = UIImage(named:"unCheck")
              }
            }
            return cell!
        }else if tableView == nurEveTblView {
            let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell2") as? slotCell)
            
            let timeStr = nurEveArr[indexPath.row].value(forKey: "mttiming") as! String
            let timeStr1 = (timeStr.components(separatedBy: "-").first!) + " PM"
            var timeStr2 = ""
            if Int((timeStr.components(separatedBy: "-").last)!) == 12 {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
            }else {
                timeStr2 = (timeStr.components(separatedBy: "-").last!) + " AM"
            }
            
            cell?.lbl.text = timeStr1 + "-" + timeStr2
            if nurSelectedDay == "M" {
                         if  nurEveArr[indexPath.row].value(forKey: "monday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "Tu" {
                         if  nurEveArr[indexPath.row].value(forKey: "tuesday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "W" {
                         if  nurEveArr[indexPath.row].value(forKey: "wednesday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "Th" {
                         if  nurEveArr[indexPath.row].value(forKey: "thursday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "F" {
                         if  nurEveArr[indexPath.row].value(forKey: "friday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "Sa" {
                         if  nurEveArr[indexPath.row].value(forKey: "saturday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }else if nurSelectedDay == "Su" {
                         if  nurEveArr[indexPath.row].value(forKey: "sunday") as! String == "Y" {
                           cell?.imgView.image = UIImage(named:"checkBox")
                         }
                         else {
                            cell?.imgView.image = UIImage(named:"unCheck")
                         }
                       }
            return cell!
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView {
        }else if tableView == mornTblView {
            let dict = self.morningArr[indexPath.row].mutableCopy() as! NSMutableDictionary
            if dict.value(forKey: "isSelect") as! String == "0" {
                dict.setValue("1", forKey: "isSelect")
                self.morningArr[indexPath.row] = dict as NSDictionary
            }else  if dict.value(forKey: "isSelect") as! String == "1" {
                dict.setValue("0", forKey: "isSelect")
                self.morningArr[indexPath.row] = dict as NSDictionary
            }
            mornTblView.reloadData()
        }else if tableView == noonTblView {
            let dict = self.noonArr[indexPath.row].mutableCopy() as! NSMutableDictionary
            if dict.value(forKey: "isSelect") as! String == "0" {
                dict.setValue("1", forKey: "isSelect")
                self.noonArr[indexPath.row] = dict as NSDictionary
            }else  if dict.value(forKey: "isSelect") as! String == "1" {
                dict.setValue("0", forKey: "isSelect")
                self.noonArr[indexPath.row] = dict as NSDictionary
            }
            noonTblView.reloadData()
        }else if tableView == eveTblView {
            let dict = self.eveArr[indexPath.row].mutableCopy() as! NSMutableDictionary
            if dict.value(forKey: "isSelect") as! String == "0" {
                dict.setValue("1", forKey: "isSelect")
                self.eveArr[indexPath.row] = dict as NSDictionary
            }else  if dict.value(forKey: "isSelect") as! String == "1" {
                dict.setValue("0", forKey: "isSelect")
                self.eveArr[indexPath.row] = dict as NSDictionary
            }
            eveTblView.reloadData()
        }else if tableView == nurMorTblView {
           let dict = self.nurMornArr[indexPath.row].mutableCopy() as! NSMutableDictionary
              if nurSelectedDay == "M" {
                if dict.value(forKey: "monday") as! String == "Y" {
                    dict.setValue("N", forKey: "monday")
                }
                else {
                    dict.setValue("Y", forKey: "monday")
                }
              }else if nurSelectedDay == "Tu" {
                if dict.value(forKey: "tuesday") as! String == "Y" {
                    dict.setValue("N", forKey: "tuesday")
                }
                else {
                    dict.setValue("Y", forKey: "tuesday")
                }
              }else if nurSelectedDay == "W" {
                if dict.value(forKey: "wednesday") as! String == "Y" {
                    dict.setValue("N", forKey: "wednesday")
                }
                else {
                    dict.setValue("Y", forKey: "wednesday")
                }
              }else if nurSelectedDay == "Th" {
                if dict.value(forKey: "thursday") as! String == "Y" {
                    dict.setValue("N", forKey: "thursday")
                }
                else {
                    dict.setValue("Y", forKey: "thursday")
                }
              }else if nurSelectedDay == "F" {
                if dict.value(forKey: "friday") as! String == "Y" {
                    dict.setValue("N", forKey: "friday")
                }
                else {
                    dict.setValue("Y", forKey: "friday")
                }
              }else if nurSelectedDay == "Sa" {
                if dict.value(forKey: "saturday") as! String == "Y" {
                    dict.setValue("N", forKey: "saturday")
                }
                else {
                    dict.setValue("Y", forKey: "saturday")
                }
              }else if nurSelectedDay == "Su" {
                if dict.value(forKey: "sunday") as! String == "Y" {
                    dict.setValue("N", forKey: "sunday")
                }
                else {
                    dict.setValue("Y", forKey: "sunday")
                }
              }
            self.nurMornArr[indexPath.row] = dict as NSDictionary
            self.nurMorTblView.reloadData()
        }else if tableView == nurNoonTblView {
           let dict = self.nurNoonArr[indexPath.row].mutableCopy() as! NSMutableDictionary
              if nurSelectedDay == "M" {
                if dict.value(forKey: "monday") as! String == "Y" {
                    dict.setValue("N", forKey: "monday")
                }
                else {
                    dict.setValue("Y", forKey: "monday")
                }
              }else if nurSelectedDay == "Tu" {
                if dict.value(forKey: "tuesday") as! String == "Y" {
                    dict.setValue("N", forKey: "tuesday")
                }
                else {
                    dict.setValue("Y", forKey: "tuesday")
                }
              }else if nurSelectedDay == "W" {
                if dict.value(forKey: "wednesday") as! String == "Y" {
                    dict.setValue("N", forKey: "wednesday")
                }
                else {
                    dict.setValue("Y", forKey: "wednesday")
                }
              }else if nurSelectedDay == "Th" {
                if dict.value(forKey: "thursday") as! String == "Y" {
                    dict.setValue("N", forKey: "thursday")
                }
                else {
                    dict.setValue("Y", forKey: "thursday")
                }
              }else if nurSelectedDay == "F" {
                if dict.value(forKey: "friday") as! String == "Y" {
                    dict.setValue("N", forKey: "friday")
                }
                else {
                    dict.setValue("Y", forKey: "friday")
                }
              }else if nurSelectedDay == "Sa" {
                if dict.value(forKey: "saturday") as! String == "Y" {
                    dict.setValue("N", forKey: "saturday")
                }
                else {
                    dict.setValue("Y", forKey: "saturday")
                }
              }else if nurSelectedDay == "Su" {
                if dict.value(forKey: "sunday") as! String == "Y" {
                    dict.setValue("N", forKey: "sunday")
                }
                else {
                    dict.setValue("Y", forKey: "sunday")
                }
              }
            self.nurNoonArr[indexPath.row] = dict as NSDictionary
            self.nurNoonTblView.reloadData()
        }else if tableView == nurEveTblView {
           let dict = self.nurEveArr[indexPath.row].mutableCopy() as! NSMutableDictionary
              if nurSelectedDay == "M" {
                if dict.value(forKey: "monday") as! String == "Y" {
                    dict.setValue("N", forKey: "monday")
                }
                else {
                    dict.setValue("Y", forKey: "monday")
                }
              }else if nurSelectedDay == "Tu" {
                if dict.value(forKey: "tuesday") as! String == "Y" {
                    dict.setValue("N", forKey: "tuesday")
                }
                else {
                    dict.setValue("Y", forKey: "tuesday")
                }
              }else if nurSelectedDay == "W" {
                if dict.value(forKey: "wednesday") as! String == "Y" {
                    dict.setValue("N", forKey: "wednesday")
                }
                else {
                    dict.setValue("Y", forKey: "wednesday")
                }
              }else if nurSelectedDay == "Th" {
                if dict.value(forKey: "thursday") as! String == "Y" {
                    dict.setValue("N", forKey: "thursday")
                }
                else {
                    dict.setValue("Y", forKey: "thursday")
                }
              }else if nurSelectedDay == "F" {
                if dict.value(forKey: "friday") as! String == "Y" {
                    dict.setValue("N", forKey: "friday")
                }
                else {
                    dict.setValue("Y", forKey: "friday")
                }
              }else if nurSelectedDay == "Sa" {
                if dict.value(forKey: "saturday") as! String == "Y" {
                    dict.setValue("N", forKey: "saturday")
                }
                else {
                    dict.setValue("Y", forKey: "saturday")
                }
              }else if nurSelectedDay == "Su" {
                if dict.value(forKey: "sunday") as! String == "Y" {
                    dict.setValue("N", forKey: "sunday")
                }
                else {
                    dict.setValue("Y", forKey: "sunday")
                }
              }
            self.nurEveArr[indexPath.row] = dict as NSDictionary
            self.nurEveTblView.reloadData()
        }
    }
    
    //MARK:- cell Btn Actions
    @objc func deleteAction(sender:UIButton){
        clinicDeleteAPICall(id: clinicListArr[sender.tag].value(forKey: "cliid") as! Int)
        
    }
    
    @objc func editAction(sender:UIButton){
        if btnSelect == "Edit" && selectSection == "\(sender.tag)" {
            selectSection = ""
            btnSelect = ""
        }
        else{
            cellSelectedDay = "\(sender.tag) M"
            selectSection = "\(sender.tag)"
            btnSelect = "Edit"
        }
        tblView.reloadData()
    }
    
    @objc func visitingHoursAction(sender:UIButton) {
        if btnSelect == "Visiting Hours" && selectSection == "\(sender.tag)" {
            selectSection = ""
            btnSelect = ""
        }
        else {
            selectSection = "\(sender.tag)"
            btnSelect = "Visiting Hours"
        }
        tblView.reloadData()
    }
    
    @objc func timeEditAction(sender:UIButton) {
        let clinicListDict = clinicListArr[Int(sender.accessibilityHint!)!].mutableCopy() as! NSMutableDictionary
        var timeArray = clinicListDict.value(forKey: "timing") as! [NSDictionary]
        
        self.editMorArr.removeAll()
        self.editNoonArr.removeAll()
        self.editEveArr.removeAll()
        
        for i in 0..<timeArray.count {
            if timeArray[i].value(forKey: "mttype") as! String == "M" {
                self.editMorArr.append(timeArray[i])
            }
            else if timeArray[i].value(forKey: "mttype") as! String == "A" {
                self.editNoonArr.append(timeArray[i])
            }
            else if timeArray[i].value(forKey: "mttype") as! String == "E" {
                self.editEveArr.append(timeArray[i])
            }
        }
        
        var dict = NSMutableDictionary()
        if sender.superview?.accessibilityHint == "M" {
            dict = editMorArr[sender.tag].mutableCopy() as! NSMutableDictionary
        }
        else if sender.superview?.accessibilityHint == "A" {
            dict = editNoonArr[sender.tag].mutableCopy() as! NSMutableDictionary
        }
        else if sender.superview?.accessibilityHint == "E" {
            dict = editEveArr[sender.tag].mutableCopy() as! NSMutableDictionary
        }
        var arrarIndex = 0
        
        for i in 0..<timeArray.count {
            if (timeArray[i].value(forKey: "mttype") as! String == dict.value(forKey: "mttype") as! String) && (timeArray[i].value(forKey: "mttiming") as! String == dict.value(forKey: "mttiming") as! String) {
                arrarIndex = i
                break
            }
        }
        dict = timeArray[arrarIndex].mutableCopy() as! NSMutableDictionary
        
        if cellSelectedDay == "\(sender.accessibilityHint!) M" {
            if dict.value(forKey: "monday") as! String == "Y" {
                dict.setValue("N", forKey: "monday")
            }
            else {
                dict.setValue("Y", forKey: "monday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) Tu" {
            if dict.value(forKey: "tuesday") as! String == "Y" {
                dict.setValue("N", forKey: "tuesday")
            }
            else {
                dict.setValue("Y", forKey: "tuesday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) W" {
            if dict.value(forKey: "wednesday") as! String == "Y" {
                dict.setValue("N", forKey: "wednesday")
            }
            else {
                dict.setValue("Y", forKey: "wednesday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) Th" {
            if dict.value(forKey: "thursday") as! String == "Y" {
                dict.setValue("N", forKey: "thursday")
            }
            else {
                dict.setValue("Y", forKey: "thursday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) F" {
            if dict.value(forKey: "friday") as! String == "Y" {
                dict.setValue("N", forKey: "friday")
            }
            else {
                dict.setValue("Y", forKey: "friday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) Sa" {
            if dict.value(forKey: "saturday") as! String == "Y" {
                dict.setValue("N", forKey: "saturday")
            }
            else {
                dict.setValue("Y", forKey: "saturday")
            }
        }
        else if cellSelectedDay == "\(sender.accessibilityHint!) Su" {
            if dict.value(forKey: "sunday") as! String == "Y" {
                dict.setValue("N", forKey: "sunday")
            }
            else {
                dict.setValue("Y", forKey: "sunday")
            }
        }
        timeArray[arrarIndex] = dict.mutableCopy() as! NSDictionary
        clinicListDict.setValue(timeArray, forKey: "timing")
        clinicListArr[Int(sender.accessibilityHint!)!] = clinicListDict
        tblView.reloadData()
    }
    
    @objc func montabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) M"
        tblView.reloadData()
    }
    @objc func tuetabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) Tu"
        tblView.reloadData()
    }
    @objc func wedtabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) W"
        tblView.reloadData()
    }
    @objc func thutabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) Th"
        tblView.reloadData()
    }
    @objc func fritabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) F"
        tblView.reloadData()
    }
    @objc func sattabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) Sa"
        tblView.reloadData()
    }
    @objc func suntabed(sender:UIButton){
        cellSelectedDay = "\(sender.tag) Su"
        tblView.reloadData()
    }
    
    //MARK:- mapview del
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let pickedplace = place.formattedAddress
        print(pickedplace!)
        lat = "\(place.coordinate.latitude)"
        long = "\(place.coordinate.longitude)"
        self.adrsTf.text = "\(place.name!)"
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- AI CALL
    func ProfileAPICall() {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = profileURL
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            
            print(strURL)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.ugArr = allServiceDict.value(forKey: "degree1") as! [NSDictionary]
                            self.pgArr = allServiceDict.value(forKey: "degree2") as! [NSDictionary]
                            self.thirdArr = allServiceDict.value(forKey: "degree3") as! [NSDictionary]
                            self.loadDocData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                        }else{
                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                   self.logout()
                                                               })
                                                           }else {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                           }
                        }
                        // Constant.hideLoader(view: self.view)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        // Constant.hideLoader(view: self.view)
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
    
    func specialityAPICall(Id:String,type:String) {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = specialityURL
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            let params = ["degreeid": Id]
            print(params)
            print(strURL)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            if type == "pg" {
                                self.pgSpeacilityArr = allServiceDict.value(forKey: "speciality") as! [NSDictionary]
                                if self.pgSpeacilityArr.count != 0 {
                                    self.setuppgSpeacilityDropDown()
                                }else{
                                    self.view.makeToast("Speciality for pg not available", duration: 2.0, position: .center)
                                }
                            }else if type == "master" {
                                self.masterSpeacilityArr = allServiceDict.value(forKey: "speciality") as! [NSDictionary]
                                if self.masterSpeacilityArr.count != 0 {
                                    self.masterSpecialityDropDown()
                                }else{
                                    self.view.makeToast("Speciality for master not available", duration: 2.0, position: .center)
                                }
                            }
                            
                        }else{
                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                   self.logout()
                                                               })
                                                           }else {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                           }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
    
    func bankNameAPICall() {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = bankName
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
          //  let params = ["degreeid": Id]
           // print(params)
            print(strURL)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.bankNameArr = allServiceDict.value(forKey: "list") as! [NSDictionary]
                        }else{
                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                   self.logout()
                                                               })
                                                           }else {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                           }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
  func uploadProfileData() {

         if Reachability.isConnectedToNetwork()  {
            
                    var params: [String: Any]
            
                        params = [
                            "name": nameTf.text!,
                            "email": mailTf.text!,
                            "dob" : dobTf.text!,
                            //"":mobileTf.text!
                            "regno": registerLbl.text!,
                            "gender":gender,
                            "degree1":ugID,
                            "degree2":pgID,
                            "degree2Speciality":pgSpeacilityID,
                            "degree3":thirdDegID,
                            "degree3Speciality":masterSpeacilityID,
                            "experience":expeTf.text!,
                            "freecamp": "",
                            "videoconsultation": camBool == false ? ("N"):("Y"),
                            "consultation": consulBool == false ? ("N"):("Y"),
                            "housevisit": houseBool == false ? ("N"):("Y"),
                            "consultationfees": consultFeetf.text!,
                            "housevisitfees": housFeetf.text!,
                            "zerodelayconfees":zeroDealayConsultFeeTf.text!,
                            "cancellation":cancellationFee,
                            "covidstatus":covidStatus,
                            "aadharno": "\(aadarNoTf.text!)",
                            "bankname":"\(bankNameTf.text!)",
                            "accountholdername":accholdNametf.text!,
                            "videoconsultationfees":videoCallFeeTf.text!,
                            "accounttype":acctyeTf.text!,
                            "accountnumber":accnoTf.text!,
                            "branchname":brancNameTf.text!,
                            "ifsccode":ifscTf.text!,
                            "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                            "level1_verify" : level1,
                            "level2_verify" : level2,
                            "admin_verify" : adminVerify
                        ]
            
            
                        print(params)

                     let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
                        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                                       "Authorization":strAccessToken]
                        let url = profileUpdateURL
                        print("url \(url)")
            
             let resizedImage = self.resizeImage(image: self.docImgView.image!)
                        let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                        print("imageData New1 \(imgData!)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                                if imgData != nil {
                                    multipartFormData.append(imgData!, withName: "profpic", fileName: "image.png", mimeType: "image/png")
                
                                }
                

                               if self.aadharImg != nil {
                                    let resizedImage = self.resizeImage(image: self.aadharImg)
                                    let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                    print("imageData New \(imgData!)")
                                    multipartFormData.append(imgData!, withName: "aadharimage", fileName: "image1.png", mimeType: "image/png")
                                }
                
                
                                if self.reisterImg != nil {
                                    let resizedImage = self.resizeImage(image: self.reisterImg)
                                    let imgData = resizedImage.jpegData(compressionQuality: 0.2)
                                    print("imageData New \(imgData!)")
                                    multipartFormData.append(imgData!, withName: "regcertificateimage", fileName: "image2.png", mimeType: "image/png")
                                }

            }, usingThreshold: UInt64.init(), to: url, method: .post,headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded  = \(response)")
                        if let err = response.error{
                            print(err)
                            return
                        }
                         
                          upload.responseJSON { response in
                                                if response.result.value != nil {
                                                    let JSON = response.result.value
                                                    let allServiceDict : NSDictionary = JSON as! NSDictionary
                                                    print(allServiceDict)
                                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                if UserDefaults.standard.value(forKey: "profileEdit") as? String != nil {
                                    if UserDefaults.standard.value(forKey: "profileEdit") as! String == "N" {
//                                        self.clinicBtn.isEnabled = true
//                                        self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
//                                        self.genralBtn.setTitleColor(UIColor.black, for: .normal)
//                                        self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
//                                        self.genralBtn.backgroundColor = UIColor.lightGray
        
                                        if self.docCategory == "NUR" {
                                          //   UserDefaults.standard.setValue("V", forKey: "profileEdit")
                                            UserDefaults.standard.setValue("Y", forKey: "profileEdit")
                                           // self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                                                       self.nav()
                                                                                                             })
                                           /// self.clinicBtn.setTitle("Visiting Hours", for: .normal)
//                                            self.scrollView.isHidden = true
//                                            self.tblView.isHidden = true
//                                            self.nurseScrollView.isHidden = false
//                                            self.addBtn.isHidden = true
//                                            self.getNurseTimeAPICall()
//                                            self.clinicBtn.setTitle("Visiting Hours", for: .normal)
                                      }else{
                                            UserDefaults.standard.setValue("C", forKey: "profileEdit")
                                            self.clinicBtn.setTitle("Clinic", for: .normal)
                                            self.scrollView.isHidden = true
                                            self.nurseScrollView.isHidden = true
                                            self.tblView.isHidden = false
                                            self.addBtn.isHidden = false
                                            self.clinicAPICall()
                                      }
                                    }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "C" {
                                        self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                                                               self.genralBtn.setTitleColor(UIColor.black, for: .normal)
                                                                               self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                                                               self.genralBtn.backgroundColor = UIColor.lightGray
                                        self.clinicBtn.setTitle("Clinic", for: .normal)
                                                                                   self.scrollView.isHidden = true
                                                                                   self.nurseScrollView.isHidden = true
                                                                                   self.tblView.isHidden = false
                                                                                   self.addBtn.isHidden = false
                                                                                   self.clinicAPICall()
                                    }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "Y"{
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                  self.nav()
                                        })
                                    }else if UserDefaults.standard.value(forKey: "profileEdit") as! String == "V" {
                                        self.clinicBtn.isEnabled = true
                                        self.clinicBtn.setTitleColor(UIColor.white, for: .normal)
                                        self.genralBtn.setTitleColor(UIColor.black, for: .normal)
                                        self.clinicBtn.backgroundColor = UIColor(red: 4/255, green: 44/255, blue: 69/255, alpha: 1.0)
                                        self.genralBtn.backgroundColor = UIColor.lightGray
                                        self.clinicBtn.setTitle("Visiting Hours", for: .normal)
                                                                                   self.scrollView.isHidden = true
                                                                                   self.tblView.isHidden = true
                                                                                   self.nurseScrollView.isHidden = false
                                                                                   self.addBtn.isHidden = true
                                                                                   self.getNurseTimeAPICall()
                                                                                   self.clinicBtn.setTitle("Visiting Hours", for: .normal)
                                    }
                                }
        
                                }else{
                                    if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                                       self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                       DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                          UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                        self.logout()
                                                                       })
                                                                   }else {
                                                                       self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                   }
                                }
                            }
                        }
                    }
                  case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")

                }
            }
        }

    }
    func nav(){
              let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
              controller.modalPresentationStyle = .fullScreen
              controller.modalTransitionStyle = .coverVertical
              controller.selectedMenu = "Home"
              present(controller, animated: true, completion: nil)
           }

    func clinicAPICall() {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = clinicListingURL
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.clinicListArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                            if self.clinicListArr.count == 0 {
                                self.toastLbl.isHidden = false
                                UserDefaults.standard.setValue("C", forKey: "profileEdit")
                            }else{
                                UserDefaults.standard.setValue("Y", forKey: "profileEdit")
                            }
                           // AMShimmer.stop(for: self.tblView)
                            self.tblView.reloadData()
                        }else{
                            
                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                   UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                   self.logout()
                                                               })
                                                           }else {
                                                               self.toastLbl.isHidden = true
                                                           }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.toastLbl.isHidden = false
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
    func createClinicPICall() {
        
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = clinicListingURL
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            
            var mondayId = ""
            for i in 0..<monIDArr.count {
                mondayId = mondayId + "\(monIDArr[i]),"
            }
            var tuesdayId = ""
            for i in 0..<TueIDArr.count {
                tuesdayId = tuesdayId + "\(TueIDArr[i]),"
            }
            var wednesdayId = ""
            for i in 0..<wedIDArr.count {
                wednesdayId = wednesdayId + "\(wedIDArr[i]),"
            }
            var thursdayId = ""
            for i in 0..<thuIDArr.count {
                thursdayId = thursdayId + "\(thuIDArr[i]),"
            }
            var fridayId = ""
            for i in 0..<friIDArr.count {
                fridayId = fridayId + "\(friIDArr[i]),"
            }
            var saturdayId = ""
            for i in 0..<satIDArr.count {
                saturdayId = saturdayId + "\(satIDArr[i]),"
            }
            var sundayId = ""
            for i in 0..<sunIDArr.count {
                sundayId = sundayId + "\(sunIDArr[i]),"
            }
            
            let params = ["name":"\(clinicTf.text!)",
                "address":"\(adrsTf.text!)",
                "contactno":"\(mobileTf.text!)",
                "longitude":lat,
                "latitude":long,
                "timeinterval":"\(consDurTf.text!)",
                "monday": mondayId.count != 0 ? (mondayId.dropLast()): "",
                "tuesday":tuesdayId.count != 0 ? (tuesdayId.dropLast()): "",
                "wednesday":wednesdayId.count != 0 ? (wednesdayId.dropLast()): "",
                "thursday":thursdayId.count != 0 ? (thursdayId.dropLast()): "",
                "friday":fridayId.count != 0 ? (fridayId.dropLast()): "",
                "saturday":saturdayId.count != 0 ? (saturdayId.dropLast()): "",
                "sunday":sundayId.count != 0 ? (sundayId.dropLast()): "",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")] as [String : Any]
            
            print(strURL)
            print(params)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters:params , encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            self.clinicAPICall()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                self.hideView.isHidden = true
                                self.nav()
                            })
                        }else{
                            
                         if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                      self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                          UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                          self.logout()
                                                      })
                                                  }else {
                                                      self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                  }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
    @objc func editClinicAPICall(intSelect:Int) {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            
            var monIDStr = ""
            var TueIDStr = ""
            var wedIDStr = ""
            var thuIDStr = ""
            var friIDStr = ""
            var satIDStr = ""
            var sunIDStr = ""
            
            let strURL = clinicListingURL+"/\(clinicListArr[intSelect].value(forKey: "cliid") as! NSNumber))"
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            
            let clinicListDict = clinicListArr[intSelect].mutableCopy() as! NSMutableDictionary
            let timeArray = clinicListDict.value(forKey: "timing") as! [NSDictionary]
            for i in 0..<timeArray.count {
                if timeArray[i].value(forKey: "monday") as! String == "Y" {
                    monIDStr = monIDStr + ((monIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "tuesday") as! String == "Y" {
                    TueIDStr = TueIDStr + ((TueIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "wednesday") as! String == "Y" {
                    wedIDStr = wedIDStr + ((wedIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "thursday") as! String == "Y" {
                    thuIDStr = thuIDStr + ((thuIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "friday") as! String == "Y" {
                    friIDStr = friIDStr + ((friIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "saturday") as! String == "Y" {
                    satIDStr = satIDStr + ((satIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
                if timeArray[i].value(forKey: "sunday") as! String == "Y" {
                    sunIDStr = sunIDStr + ((sunIDStr == "") ? (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue : ("," + (timeArray[i].value(forKey: "mtid") as! NSNumber).stringValue))
                }
            }
            
            
            let params = ["name":clinicListArr[intSelect].value(forKey: "cliname") as! String,
                          "address":clinicListArr[intSelect].value(forKey: "cliaddress") as! String,
                          "contactno":clinicListArr[intSelect].value(forKey: "clicontactno") as! String,
                          "longitude":clinicListArr[intSelect].value(forKey: "clilatitude") as! String,
                          "latitude":clinicListArr[intSelect].value(forKey: "clilongitude") as! String,
                          "timeinterval":clinicListArr[intSelect].value(forKey: "clitimeinterval") as! Int,
                          "monday" : monIDStr,
                          "tuesday" : TueIDStr,
                          "wednesday" : wedIDStr ,
                          "thursday":thuIDStr,
                          "friday":friIDStr,
                          "saturday":satIDStr,
                          "sunday":sunIDStr] as [String : Any]
            print(strURL)
            print(headers)
            print(params)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            self.clinicAPICall()
                        }else{
                              if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                             UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                             self.logout()
                                                         })
                                                     }else {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                     }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }

    
    func timingAPICall() {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = timingURL
            
            print(strURL)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            let timeListArr = allServiceDict.value(forKey: "list") as! [NSDictionary]
                            self.morningArr.removeAll()
                            self.noonArr.removeAll()
                            self.eveArr.removeAll()
                            for i in 0..<timeListArr.count {
                                let dict = timeListArr[i].mutableCopy() as! NSMutableDictionary
                                if dict.value(forKey: "mt_types") as! String == "M" {
                                    dict.setValue("0", forKey: "isSelect")
                                    self.morningArr.append(dict as NSDictionary)
                                }else if dict.value(forKey: "mt_types") as! String == "A" {
                                    dict.setValue("0", forKey: "isSelect")
                                    self.noonArr.append(dict as NSDictionary)
                                }else if dict.value(forKey: "mt_types") as! String == "E" {
                                    dict.setValue("0", forKey: "isSelect")
                                    self.eveArr.append(dict as NSDictionary)
                                }
                            }
                            
                            self.addClinincTimingArr.removeAll()
                            let strDays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
                            for i in 0..<strDays.count {
                                var dictDays = ["day" : strDays[i], "isSelect" : ((i == 0) ? true : false)] as [String : Any]
                                dictDays["morn"] = self.morningArr
                                dictDays["noon"] = self.noonArr
                                dictDays["even"] = self.eveArr
                                self.addClinincTimingArr.append(NSMutableDictionary(dictionary: dictDays))
                            }
                            self.mornTblView.reloadData()
                            self.noonTblView.reloadData()
                            self.eveTblView.reloadData()
                            for i in 0..<self.btnDays.count {
                                self.btnDays[i].setTitleColor(UIColor.darkText, for: .normal)
                                self.btnDays[i].isEnabled = true
                                self.monimg.image = UIImage(named: "checkBox")
                                self.tueimg.image = UIImage(named: "unCheck")
                                self.wedimg.image = UIImage(named: "unCheck")
                                self.thuimg.image = UIImage(named: "unCheck")
                                self.friimg.image = UIImage(named: "unCheck")
                                self.satimg.image = UIImage(named: "unCheck")
                                self.sunimg.image = UIImage(named: "unCheck")
                            }
                        }else{
                               if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                             UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                             self.logout()
                                                         })
                                                     }else {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                     }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
    
    func clinicDeleteAPICall(id:Int) {
        if Reachability.isConnectedToNetwork() {
            KVSpinnerView.show(saying: "")
            let strURL = clinicListingURL+"/\(id)"
            let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
            print(strURL)
            print(headers)
            Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.delete, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                {
                    response in switch response.result {
                    case .success(let JSON):
                        let allServiceDict = (JSON as! NSDictionary)
                        print(allServiceDict)
                        if allServiceDict.value(forKey: "status") as! String == "true" {
                            self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            self.clinicAPICall()
                        }else{
                            if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                             UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                             self.logout()
                                                         })
                                                     }else {
                                                         self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                     }
                        }
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                        DispatchQueue.main.async {
                            KVSpinnerView.dismiss()
                        }
                        
                    }
            }
        }else {
            self.view.makeToast("NetWork error", duration: 3.0, position: .center)
        }
    }
   
    

    
    
    //MARK:- FOR NURSE
      func getNurseTimeAPICall() {
          if Reachability.isConnectedToNetwork() {
              KVSpinnerView.show(saying: "")
              let strURL = nurserTimingURL
              let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
              let headers = ["Content-Type": "application/x-www-form-urlencoded",
                             "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
              print(strURL)
              
              print(strURL)
              print(headers)
              Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                  {
                      response in switch response.result {
                      case .success(let JSON):
                          let allServiceDict = (JSON as! NSDictionary)
                          print(allServiceDict)
                          if allServiceDict.value(forKey: "status") as! String == "true" {
                              self.nurTimeListArr = allServiceDict.value(forKey: "List") as! [NSDictionary]
                              
                              self.nurMornArr.removeAll()
                              self.nurNoonArr.removeAll()
                              self.nurEveArr.removeAll()
                              
                              for i in 0..<self.nurTimeListArr.count {
                                  let dict = self.nurTimeListArr[i]
                                 if dict.value(forKey: "mttype") as! String == "M" {
                                     self.nurMornArr.append(dict)
                                 }else if dict.value(forKey: "mttype") as! String == "A" {
                                    self.nurNoonArr.append(dict)
                                 }else if dict.value(forKey: "mttype") as! String == "E" {
                                    self.nurEveArr.append(dict)
                                  }
                               }
                                                         
                             print(self.editMorArr.count)
                             print(self.editNoonArr.count)
                             print(self.editEveArr.count)
                              
                              self.nurMorTblView.reloadData()
                              self.nurNoonTblView.reloadData()
                              self.nurEveTblView.reloadData()
                          }else{
                        if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                   UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                                                   self.logout()
                                                               })
                                                           }else {
                                                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                           }
                          }
                          // Constant.hideLoader(view: self.view)
                          DispatchQueue.main.async {
                              KVSpinnerView.dismiss()
                          }
                      case .failure(let error):
                          print(error)
                          self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                          DispatchQueue.main.async {
                              KVSpinnerView.dismiss()
                          }
                          // Constant.hideLoader(view: self.view)
                      }
              }
          }else {
              self.view.makeToast("NetWork error", duration: 3.0, position: .center)
          }
      }
      
       func nurEditSlotAPICall() {
          if Reachability.isConnectedToNetwork() {
              KVSpinnerView.show(saying: "")
              
              let strURL = nurserTimingURL
              let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
              let headers = ["Content-Type": "application/x-www-form-urlencoded",
                             "Authorization":strAccessToken]
              let params = [
                            "monday" : nurMonIDStr,
                            "tuesday" : nurTueIDStr,
                            "wednesday" : nurWedIDStr ,
                            "thursday": nurThuIDStr,
                            "friday": nurFriIDStr,
                            "saturday": nurSatIDStr,
                            "sunday": nurSunIDStr,
                            "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")] as [String : Any]
              
              print(strURL)
              print(headers)
              print(params)
              
              Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                  {
                      response in switch response.result {
                      case .success(let JSON):
                          let allServiceDict = (JSON as! NSDictionary)
                          print(allServiceDict)
                          if allServiceDict.value(forKey: "status") as! String == "true" { UserDefaults.standard.setValue("Y", forKey: "profileEdit")
                              self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                             self.nav()
                                                                   })
                             
                          }else{
                                         if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                                            self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                                                                        self.logout()
                                                                                    })
                                                                                }else {
                                                                                      self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                                                                }
                          }
                          DispatchQueue.main.async {
                              KVSpinnerView.dismiss()
                          }
                      case .failure(let error):
                          print(error)
                          self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                          DispatchQueue.main.async {
                              KVSpinnerView.dismiss()
                          }
                          
                      }
              }
          }else {
              self.view.makeToast("NetWork error", duration: 3.0, position: .center)
          }
      }
    
    
    //MARK:- doctor info validation
    func infoValidation(){
        if nameTf.text != "" {
         if mailTf.text != "" {
          if mobileTf.text != "" {
           if dobTf.text != "" {
            if genderTf.text != "" {
             if ugQualtf.text != "" {
//              if aadarNoTf.text != "" {
//               if aadharImgBOOL != false {
                if expeTf.text != "" {
                 if profilepicBool == true {
             //     if aadarNoTf.text?.count == 14 {
                   
                     if docCategory == "NUR" {
                      if housFeetf.text != "" {
                        if camBool == true {
                         if videoCallFeeTf.text != "" {
                            //bankValidation()
                            uploadProfileData()
                          }else{
                            self.view.makeToast("please enter your Online Consultation Fees")
                          }
                        }else{
                          uploadProfileData()
                        }
                        
                       }else{
                         self.view.makeToast("please enter your houseViist Fees")
                        }
                      }else{
                        if consulBool == true {
                            if consultFeetf.text != "" {
                                if houseBool == true {
                                    if housFeetf.text != "" {
                                         uploadProfileData()
                                    }else{
                                        self.view.makeToast("please enter your house visit fees")
                                    }
                                }else{
                                    uploadProfileData()
                                }
                            }else{
                                self.view.makeToast("please enter your consultation fees")
                            }
                        }
                        
                        if houseBool == true {
                            if housFeetf.text != "" {
                                if consulBool == true {
                                    if consultFeetf.text != "" {
                                         uploadProfileData()
                                    }else{
                                        self.view.makeToast("please enter your consultation fees")
                                    }
                                }else{
                                     uploadProfileData()
                                }
                            }else{
                                self.view.makeToast("please enter your house visit fees")
                            }
                        }
                        
                       }
                        
                       
//                     }else{
//                        self.aadarNoTf.becomeFirstResponder()
//                         self.view.makeToast("please check your aadhaar number")
//                      }
                   }else{
                     self.view.makeToast("please take your profile picture")
                   }
                  }else{
                    self.expeTf.becomeFirstResponder()
                      self.view.makeToast("please enter your Experience", duration: 2.0, position: .center)
                                        }
//                                    }else{
//                                        self.view.makeToast("please select your aadhaar image", duration: 2.0, position: .center)
//                                    }
//                                }else{
//                                    self.aadarNoTf.becomeFirstResponder()
//                                    self.view.makeToast("please enter your aadhaar number", duration: 2.0, position: .center)
//                                }
                            }else{
                                self.view.makeToast("please select your UG Qualification", duration: 2.0, position: .center)
                            }
                        }else{
                            self.view.makeToast("please select your GENDER", duration: 2.0, position: .center)
                        }
                    }else{
                        self.view.makeToast("please enter your DOB", duration: 2.0, position: .center)
                    }
                }else{
                    self.mobileTf.becomeFirstResponder()
                    self.view.makeToast("please enter your mobile", duration: 2.0, position: .center)
                }
            }else{
                self.mailTf.becomeFirstResponder()
                self.view.makeToast("please enter your mail", duration: 2.0, position: .center)
            }
        }else{
            self.nameTf.becomeFirstResponder()
            self.view.makeToast("please enter your name", duration: 2.0, position: .center)
        }
    }
    func bankValidation() {
        if bankNameTf.text != "" {
            if accholdNametf.text != "" {
                if acctyeTf.text != "" {
                    if accnoTf.text != "" {
                        if ifscTf.text != "" {
                          uploadProfileData()
                        }else{
                            //self.nameTf.becomeFirstResponder()
                            self.view.makeToast("please enter IFSC code", duration: 2.0, position: .center)
                        }
                    }else{
                       // self.nameTf.becomeFirstResponder()
                        self.view.makeToast("please choose account number", duration: 2.0, position: .center)
                    }
                }else{
                   // self.nameTf.becomeFirstResponder()
                    self.view.makeToast("please choose account type", duration: 2.0, position: .center)
                }
            }else{
                // self.nameTf.becomeFirstResponder()
                self.view.makeToast("please enter account holder name", duration: 2.0, position: .center)
            }
        }else{
           // self.nameTf.becomeFirstResponder()
            self.view.makeToast("please enter your bank name", duration: 2.0, position: .center)
        }
    }
    //MARK:- loadData
    func loadDocData(dict:NSDictionary){
        
        //verification
                         if dict.value(forKey: "level1_verify") as? String != nil {
                             level1 = "\(dict.value(forKey: "level1_verify") as! String)"
                         }
                         if dict.value(forKey: "level2_verify") as? String != nil {
                                    level2 = "\(dict.value(forKey: "level2_verify") as! String)"
                                }
                         if level2 == "P" {
                             level2 = "R"
                         }
                         if dict.value(forKey: "admin_verify") as? String != nil {
                                    adminVerify = "\(dict.value(forKey: "admin_verify") as! String)"
                                }
                         if adminVerify == "P" {
                             adminVerify = "R"
                         }
        
        nameTf.text = dict.value(forKey: "name") as? String
        UserDefaults.standard.setValue(dict.value(forKey: "name") as! String, forKey: "name")
        nameTf.isUserInteractionEnabled = false
        //DOB
        if dict.value(forKey: "dob") as? String != nil {
            dobTf.text = dict.value(forKey: "dob") as? String
            if dict.value(forKey: "dob") as! String != "" {
              dobTf.isUserInteractionEnabled = false
            }else{
                dobTf.isUserInteractionEnabled = true
            }
        }
        //mail
        if dict.value(forKey: "email") as? String != nil {
            mailTf.text = dict.value(forKey: "email") as? String
            mailTf.isUserInteractionEnabled = false
        }
        //deg1
        if dict.value(forKey: "degree1_name") as? String != nil {
            ugQualtf.text = (dict.value(forKey: "degree1_name") as! String)
            if dict.value(forKey: "degree1_name") as! String == "" {
                ugQualtf.isUserInteractionEnabled = true
            }else{
                ugQualtf.isUserInteractionEnabled = false
            }
            ugID = "\(dict.value(forKey: "degree1") as! Int)"
        }
        //deg2
        if dict.value(forKey: "degree2_name") as? String != nil {
            pgQualtf.text = (dict.value(forKey: "degree2_name") as! String)
            pgID = "\(dict.value(forKey: "degree2") as! Int)"
            if dict.value(forKey: "degree2_name") as! String == "" {
                pgQualtf.isUserInteractionEnabled = true
            }else{
                pgQualtf.isUserInteractionEnabled = false
            }
          //  deg2btn.isHidden = false
        }
        //deg2Seacility
        if dict.value(forKey: "speciality2_name") as? String != nil {
            pgSpecialityTf.text = (dict.value(forKey: "speciality2_name") as! String)
            pgSpeacilityID = "\(dict.value(forKey: "degree2speciality") as! Int)"
            SpecialityHeightCons1.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60)
            if dict.value(forKey: "speciality2_name") as! String == "" {
                pgSpecialityTf.isUserInteractionEnabled = true
            }else{
                pgSpecialityTf.isUserInteractionEnabled = false
            }
           // deg12btn.isHidden = false
        }
        
        
        
        
        if dict.value(forKey: "videoconsultation") as? String != nil {
            if dict.value(forKey: "videoconsultation") as! String == "N" {
                camBool = false
                campImg.image = UIImage(named: "unCheck")
                videoConsultHeightCons.constant = 0
             } else{
                camBool = true
                campImg.image = UIImage(named: "checkBox")
                videoConsultHeightCons.constant = 40
                if dict.value(forKey: "videoconsultationfees") as? Int != nil {
                    videoCallFeeTf.text = "\(dict.value(forKey: "videoconsultationfees") as! Int)"
                }
            }
        }
        
        if dict.value(forKey: "cancellation") as? String != nil {
            if dict.value(forKey: "cancellation") as! String == "Y" {
                cancellationFee = "Y"
                cancellationFeeYesBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
                 cancellationFeeNoBtn.setImage(UIImage(named: "circle"), for: .normal)
            }else {
                cancellationFee = "N"
                 cancellationFeeYesBtn.setImage(UIImage(named: "circle"), for: .normal)
                 cancellationFeeNoBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
            }
        }
        if dict.value(forKey: "covidstatus") as? String != nil {
                   if dict.value(forKey: "covidstatus") as! String == "Y" {
                    covidStatus = "Y"
                       cancellationFeeYesBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
                       cancellationFeeNoBtn.setImage(UIImage(named: "circle"), for: .normal)
                   }else {
                    covidStatus = "N"
                       cancellationFeeYesBtn.setImage(UIImage(named: "circle"), for: .normal)
                       cancellationFeeNoBtn.setImage(UIImage(named: "filledCircle"), for: .normal)
                   }
               }
        //deg3Seacility
        if dict.value(forKey: "speciality3_name") as? String != nil {
            masSpecialityTf.text = (dict.value(forKey: "speciality3_name") as! String)
            masterSpeacilityID = "\(dict.value(forKey: "degree3speciality") as! Int)"
            if dict.value(forKey: "speciality3_name") as! String == "" {
                           masSpecialityTf.isUserInteractionEnabled = true
                       }else{
                           masSpecialityTf.isUserInteractionEnabled = false
                       }
           // deg13btn.isHidden = false
        }
        //deg3
        if dict.value(forKey: "degree3_name") as? String != nil {
            thirdDegtf.text = (dict.value(forKey: "degree3_name") as! String)
            thirdDegID = "\(dict.value(forKey: "degree3") as! Int)"
            SpecialityHeightCons2.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60)
            if dict.value(forKey: "degree3_name") as! String == "" {
                                      thirdDegtf.isUserInteractionEnabled = true
                                  }else{
                                      thirdDegtf.isUserInteractionEnabled = false
                                  }
          //  deg3btn.isHidden = false
        }
        //gender
        if dict.value(forKey: "gender") as? String != nil {
            if dict.value(forKey: "gender") as! String == "M" {
                genderTf.text = "MALE"
            }else if dict.value(forKey: "gender") as! String == "F" {
                genderTf.text = "FEMALE"
            }else{
                genderTf.text = "OTHERS"
            }
            gender = dict.value(forKey: "gender") as! String
            
            if dict.value(forKey: "gender") as! String != "" {
                        genderTf.isUserInteractionEnabled = false
                      }else{
                          genderTf.isUserInteractionEnabled = true
                      }
        }
        //Free cam
       
        //consult
        if dict.value(forKey: "consultation") as? String != nil {
            if dict.value(forKey: "consultation") as! String == "N" {
                consulBool = false
                consulImg.image = UIImage(named: "unCheck")
                consultFeeHeightCons.constant = 0
                consultFeetf.isHidden = true
                zeroDealayConsultFeeTf.isHidden = true
                consultFeetf.text = ""
            } else{
                consulBool = true
                consulImg.image = UIImage(named: "checkBox")
                consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 90 : 120)
                consultFeetf.isHidden = false
                zeroDealayConsultFeeTf.isHidden = false
            }
        }
        
        //HouseVisit
        if dict.value(forKey: "housevisit") as? String != nil {
            if dict.value(forKey: "housevisit") as! String == "N" {
                houseBool = false
                houseImg.image = UIImage(named: "unCheck")
                 houseFeeHeightCons.constant = 0
                housFeetf.isHidden = true
                houVisitLbl.isHidden = true
                 housFeetf.text = ""
            } else{
                houseBool = true
                houseImg.image = UIImage(named: "checkBox")
                 houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 55 : 65)
                housFeetf.isHidden = false
                houVisitLbl.isHidden = false
            }
        }
        
        //consultation Fee
        if dict.value(forKey: "consultationfees") as? Int != nil {
            consultFeetf.text = "\(dict.value(forKey: "consultationfees") as! Int)"
           // consFee = "\(dict.value(forKey: "consultationfees") as! Int)"
            consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 90 : 120)
            consultFeetf.isHidden = false
            
        }
        if dict.value(forKey: "zerodelayconfees") as? Int != nil {
            zeroDealayConsultFeeTf.text = "\(dict.value(forKey: "zerodelayconfees") as! Int)"
           // consFee = "\(dict.value(forKey: "consultationfees") as! Int)"
            consultFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 90 : 120)
            zeroDealayConsultFeeTf.isHidden = false
        }
        //houseVisit Fee
        if dict.value(forKey: "housevisitfees") as? Int != nil {
            housFeetf.text = "\(dict.value(forKey: "housevisitfees") as! Int)"
           // houseVisitFee = "\(dict.value(forKey: "housevisitfees") as! Int)"
            houseFeeHeightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 50 : 60)
            houVisitLbl.isHidden = false
            housFeetf.isHidden = false
        }
        //aadharImg
        if dict.value(forKey: "aadhar_image") as? String != nil && dict.value(forKey: "aadhar_image") as? String != ""{
            aadharImgView.af_setImage(withURL: URL(string: dict.value(forKey: "aadhar_image") as! String)!)
            aadharImgBOOL = true
        }
        
        //rofile
        if dict.value(forKey: "prof_pic") as? String != nil && dict.value(forKey: "prof_pic") as? String != ""{
            docImgView.af_setImage(withURL: URL(string: dict.value(forKey: "prof_pic") as! String)!)
            UserDefaults.standard.set(dict.value(forKey: "prof_pic") as! String, forKey: "profileImg")
            profilepicBool = true
        }
        //Ex
        if dict.value(forKey: "experience") as? Int != nil {
            expeTf.text = "\(dict.value(forKey: "experience") as! Int)"
        }
        //aadhar no
        if dict.value(forKey: "aadhar_no") as? String != nil {
            aadarNoTf.text = "\(dict.value(forKey: "aadhar_no") as! String)"
            if dict.value(forKey: "aadhar_no") as! String == "" {
                aadarNoTf.isUserInteractionEnabled = true
            }else{
                 aadarNoTf.isUserInteractionEnabled = false
            }
        }
        
        //Mobile
        if dict.value(forKey: "mobile_no") as? String != nil {
            mobileTf.text = dict.value(forKey: "mobile_no") as? String
            UserDefaults.standard.setValue(dict.value(forKey: "mobile_no") as! String, forKey: "mobile")
        }
        //regsiteNo
        if dict.value(forKey: "regno") as? String != nil {
            registerLbl.text = "\(dict.value(forKey: "regno") as! String)"
        }
        //bankName
        if dict.value(forKey: "bank_name") as? String != nil {
            bankNameTf.text = "\(dict.value(forKey: "bank_name") as! String)"
            if dict.value(forKey: "bank_name") as! String == "" {
                bankNameTf.isUserInteractionEnabled = true
             }else{
                bankNameTf.isUserInteractionEnabled = false
            }
        }
        //ACCholdNAme
        if dict.value(forKey: "account_holder_name") as? String != nil {
            accholdNametf.text = "\(dict.value(forKey: "account_holder_name") as! String)"
            if dict.value(forKey: "account_holder_name") as! String == "" {
                           accholdNametf.isUserInteractionEnabled = true
                        }else{
                           accholdNametf.isUserInteractionEnabled = false
                       }
        }
        //accTye
        if dict.value(forKey: "account_type") as? String != nil {
            acctyeTf.text = "\(dict.value(forKey: "account_type") as! String)"
            if dict.value(forKey: "account_type") as! String == "" {
                                      acctyeTf.isUserInteractionEnabled = true
                                   }else{
                                      acctyeTf.isUserInteractionEnabled = false
                                  }
        }
        //AccNo
        if dict.value(forKey: "account_number") as? String != nil {
            accnoTf.text = "\(dict.value(forKey: "account_number") as! String)"
            if dict.value(forKey: "account_number") as! String == "" {
                accnoTf.isUserInteractionEnabled = true
             }else{
                accnoTf.isUserInteractionEnabled = false
            }
        }
        //BranchName
        if dict.value(forKey: "branch_name") as? String != nil {
            brancNameTf.text = "\(dict.value(forKey: "branch_name") as! String)"
            if dict.value(forKey: "branch_name") as! String == "" {
                           brancNameTf.isUserInteractionEnabled = true
                        }else{
                           brancNameTf.isUserInteractionEnabled = false
                       }
        }
        //ifscCode
        if dict.value(forKey: "ifsc_code") as? String != nil {
            ifscTf.text = "\(dict.value(forKey: "ifsc_code") as! String)"
            if dict.value(forKey: "ifsc_code") as! String == "" {
                ifscTf.isUserInteractionEnabled = true
             }else{
                ifscTf.isUserInteractionEnabled = false
            }
        }
        //ifscCode
        if dict.value(forKey: "admin_verify") as? String != nil {
           UserDefaults.standard.setValue("\(dict.value(forKey: "admin_verify") as! String)", forKey: "verify")
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadData"), object: nil)
        
    }
    
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
    
}
