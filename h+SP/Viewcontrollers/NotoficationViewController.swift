//
//  NotoficationViewController.swift
//  h+SP
//
//  Created by mirrorminds on 13/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class NotoficationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var toastLbl: UILabel!
    
    var fromview = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.addStatusBar(view: self.view)
    }
    
    // MARK: - btnActions
    @IBAction func menuAction(_ sender: Any) {
        if fromview == "O" {
            self.dismiss(animated: true, completion: nil)
        }
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    // MARK: - tblView Delegates
          func numberOfSections(in tableView: UITableView) -> Int {
              return 1
          }
          func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              return 0
          }
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 100
          }
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              var  cell: NotificationTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as? NotificationTableViewCell)
              let nib: [Any] = Bundle.main.loadNibNamed("NotificationTableViewCell", owner: self, options: nil)!
              cell = (nib[0]  as? NotificationTableViewCell)!
              cell?.bgView.layer.shadowColor = UIColor.lightGray.cgColor
              cell?.bgView.layer.shadowOpacity = 1
              cell?.bgView.layer.shadowOffset = CGSize.zero
              cell?.bgView.layer.shadowRadius = 2
              cell?.bgView.layer.cornerRadius = 0
              return cell!
          }
          

}
