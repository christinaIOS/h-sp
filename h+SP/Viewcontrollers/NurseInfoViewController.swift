//
//  NurseInfoViewController.swift
//  h+SP
//
//  Created by mirrorminds on 23/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit

class NurseInfoViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var mornTblView: UITableView!
    @IBOutlet weak var noonTblView: UITableView!
    @IBOutlet weak var eveTblView: UITableView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var MenuBtn: UIButton!
    
      var myArray = [NSDictionary]()
      var myArray1 = [NSDictionary]()
      var myArray2 = [NSDictionary]()
      var morArr = ["6.00AM - 7.00AM","7.00AM - 8.00AM","8.00AM - 9.00AM","9.00AM - 10.00AM","10.00AM - 11.00AM","11.00AM - 12.00AM"]
      var noonArr = ["12.00PM - 1.00PM","1.00PM - 2.00PM","2.00PM - 3.00PM","3.00PM - 4.00PM","4.00PM - 5.00PM"]
      var eveArr = ["5.00PM - 6.00PM","6.00PM - 7.00PM","7.00PM - 8.00PM","8.00PM - 9.00PM","9.00PM - 10.00PM","10.00PM - 11.00PM"]
     var fromView = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for i in 0..<morArr.count {
            let dict = ["time": morArr[i], "isSelect": "0"]
            myArray.append(dict as NSDictionary)
        }
        print("\(myArray)")
        
        for i in 0..<noonArr.count {
            let dict = ["time": noonArr[i], "isSelect": "0"]
            myArray1.append(dict as NSDictionary)
        }
        print("\(myArray1)")
        
        for i in 0..<eveArr.count {
            let dict = ["time": eveArr[i], "isSelect": "0"]
            myArray2.append(dict as NSDictionary)
        }
        print("\(myArray2)")
        
        centerView.roundCorners(corners: [.topLeft], radius: 60)
        
        if fromView == "dash" {
                   MenuBtn.isHidden = true
                   backBtn.isHidden = false
            
               }else {
                   MenuBtn.isHidden = false
                  backBtn.isHidden = true
            gestur()
        }
        
    }
    func gestur(){
            let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
            left.direction = .left
            self.view.addGestureRecognizer(left)
    
            let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
            right.direction = .right
            self.view.addGestureRecognizer(right)
        }
    
    
        @objc func Swipe(){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }

   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    // MARK: - btnActions
    @IBAction func menuAction(_ sender: UIButton) {
            if sender.tag == 0 {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
              }else if sender.tag == 1 {
                  self.dismiss(animated: true, completion: nil)
              }
         }
    @IBAction func hideViewClose(_ sender: UIButton) {
           hideView.isHidden = true
       }
    // MARK: - tblView Delegates
       func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            if tableView == mornTblView {
               return myArray.count
           }else if tableView == noonTblView {
               return myArray1.count
           }else if tableView == eveTblView {
               return myArray2.count
           }
           return 0
       }
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           
              return 35
         
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cell = UITableViewCell()
               
           if tableView == mornTblView {
               let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as? slotCell)
               cell?.lbl.text = ((myArray[indexPath.row] as NSDictionary).value(forKey: "time") as! String)
               if (myArray[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                   cell?.imgView.image = UIImage(named: "unCheck")
               }else {
                   cell?.imgView.image = UIImage(named: "checkBox")
               }
               return cell!
           }else if tableView == noonTblView {
               let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell1") as? slotCell)
               cell?.lbl.text = ((myArray1[indexPath.row] as NSDictionary).value(forKey: "time") as! String)
               if (myArray1[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                   cell?.imgView.image = UIImage(named: "unCheck")
               }else {
                   cell?.imgView.image = UIImage(named: "checkBox")
               }
               return cell!
           }else if tableView == eveTblView {
               let  cell = (tableView.dequeueReusableCell(withIdentifier: "cell2") as? slotCell)
               cell?.lbl.text = ((myArray2[indexPath.row] as NSDictionary).value(forKey: "time") as! String)
               if (myArray2[indexPath.row] as NSDictionary).value(forKey: "isSelect") as! String == "0" {
                   cell?.imgView.image = UIImage(named: "unCheck")
               }else {
                   cell?.imgView.image = UIImage(named: "checkBox")
               }
               return cell!
           }
           return cell
           
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
            if tableView == mornTblView{
              
               let dict = self.myArray[indexPath.row].mutableCopy() as! NSMutableDictionary
                      if dict.value(forKey: "isSelect") as! String == "0" {
                          dict.setValue("1", forKey: "isSelect")
                          self.myArray[indexPath.row] = dict as NSDictionary
                      }else  if dict.value(forKey: "isSelect") as! String == "1" {
                          dict.setValue("0", forKey: "isSelect")
                          self.myArray[indexPath.row] = dict as NSDictionary
                      }
               mornTblView.reloadData()
           }else if tableView == noonTblView{
               let dict = self.myArray1[indexPath.row].mutableCopy() as! NSMutableDictionary
               if dict.value(forKey: "isSelect") as! String == "0" {
                                     dict.setValue("1", forKey: "isSelect")
                                     self.myArray1[indexPath.row] = dict as NSDictionary
                                 }else  if dict.value(forKey: "isSelect") as! String == "1" {
                                     dict.setValue("0", forKey: "isSelect")
                                     self.myArray1[indexPath.row] = dict as NSDictionary
                                 }
               noonTblView.reloadData()
           }else if tableView == eveTblView {
               let dict = self.myArray2[indexPath.row].mutableCopy() as! NSMutableDictionary
               if dict.value(forKey: "isSelect") as! String == "0" {
                                     dict.setValue("1", forKey: "isSelect")
                                     self.myArray2[indexPath.row] = dict as NSDictionary
                                 }else  if dict.value(forKey: "isSelect") as! String == "1" {
                                     dict.setValue("0", forKey: "isSelect")
                                     self.myArray2[indexPath.row] = dict as NSDictionary
                                 }
               eveTblView.reloadData()
           }
           
       }
    @available(iOS 11.0, *)
    func logout(){
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
        present(controller, animated: true, completion: nil)
    }
}
