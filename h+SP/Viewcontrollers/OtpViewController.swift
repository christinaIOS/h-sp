//
//  OtpViewController.swift
//  h+SP
//
//  Created by mirrorminds on 13/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView

class OtpViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var otpTf1: UITextField!
    @IBOutlet weak var otpTf2: UITextField!
    @IBOutlet weak var otpTf3: UITextField!
    @IBOutlet weak var otpTf4: UITextField!
    @IBOutlet weak var timerlbl: UILabel!
    @IBOutlet weak var resendBtn: UIButton!
    
    
    //var otpTimer: Timer?
    var timer = Timer()
    var isTimerRunning = false
    var seconds = 0
    var otStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        otpTf1.text = String(otStr[otStr.index(otStr.startIndex, offsetBy: 0)])
//        otpTf2.text = String(otStr[otStr.index(otStr.startIndex, offsetBy: 1)])
//        otpTf3.text = String(otStr[otStr.index(otStr.startIndex, offsetBy: 2)])
//        otpTf4.text = String(otStr[otStr.index(otStr.startIndex, offsetBy: 3)])
        
        resendBtn.isEnabled = false
        resendBtn.setTitleColor(UIColor.lightGray, for: .normal)
        Constant.addStatusBar(view: self.view)
        
        //loader
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
        KVSpinnerView.settings.statusTextColor = UIColor.darkGray
        KVSpinnerView.settings.tintColor = UIColor.darkGray
        
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
                     NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        seconds = 60
        if isTimerRunning == false {
            runTimer()
        }else {
            isTimerRunning = false
            timer.invalidate()
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if ((textField.text?.count)! < 1 ) && (string.count > 0) {
               if textField == otpTf1 {
                   otpTf2.becomeFirstResponder()
               }
               if textField == otpTf2 {
                   otpTf3.becomeFirstResponder()
               }
               if textField == otpTf3 {
                   otpTf4.becomeFirstResponder()
               }
               if textField == otpTf4 {
                   otpTf4.resignFirstResponder()
               }
               textField.text = string
               return false
           }else if ((textField.text?.count)! >= 1) && (string.count == 0){
               if textField == otpTf2 {
                   otpTf1.becomeFirstResponder()
               }
               if textField == otpTf3 {
                   otpTf2.becomeFirstResponder()
               }
               if textField == otpTf4 {
                    otpTf3.becomeFirstResponder()
               }
               if textField == otpTf1 {
                   otpTf1.resignFirstResponder()
               }
               textField.text = ""
               return false
           }else if (textField.text?.count)! >= 1{
               textField.text = string
               return false
           }
           
           return true
       }
    //MARK:- timer functions
      func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
          //        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
          return (seconds / 3600, (seconds / 60) % 60, seconds % 60)
      }
      func secondsToHoursMinutesSeconds1 (seconds : Int) -> (Int) {
          //        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
          return (seconds / 3600)
      }
      
      
      func runTimer(){
          timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
      }
      @objc func updateTimer(){
          if seconds < 1 {
            resendBtn.isEnabled = true
            resendBtn.setTitleColor(UIColor.black, for: .normal)
              timer.invalidate()
          }else {
            resendBtn.isEnabled = false
              seconds -= 1
              timerlbl.text = timeString(time: TimeInterval(seconds))
          }
      }
      
      func timeString(time: TimeInterval) -> String{
          let hours = Int(time)/3600
          let minutes = Int(time)/60 % 60
          let seconds = Int(time)%60
          return String(format: "%02i:%02i:%02i", hours,minutes,seconds)
      }
      
      @IBAction func back(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
      }
      
     //MARK:- btn actions
    @available(iOS 11.0, *)
    @IBAction func submitAction(_ sender: Any) {
           validation()
       }
    @available(iOS 11.0, *)
    @IBAction func goLoginAction(_ sender: Any) {
              let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                       controller.modalPresentationStyle = .fullScreen
                       controller.modalTransitionStyle = .coverVertical
                       present(controller, animated: true, completion: nil)
    }
    @available(iOS 11.0, *)
    @IBAction func resendAction(_ sender: Any) {
              ResendOTPAPICall()
          }
       

    @available(iOS 11.0, *)
    func validation() {
           if otpTf1.text != "" && otpTf2.text != "" && otpTf3.text != "" && otpTf4.text != ""{
              OTPAPICall()
           }else{
               self.view.makeToast("Please enter your OTP", duration: 2.0, position: .center)
           }
       }
       
    func nav(str:String,nameStr:String,mobileStr:String){
           let controller = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
           controller.modalPresentationStyle = .fullScreen
           controller.modalTransitionStyle = .coverVertical
        controller.name = nameStr
        controller.mobileNo = mobileStr
           controller.selectedMenu = str
        
        
        UserDefaults.standard.setValue(nameStr, forKey: "name")
        UserDefaults.standard.setValue(mobileStr, forKey: "mobile")
        
       
           present(controller, animated: true, completion: nil)
       }
    
    @objc func keyboardWillShow(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
               if self.view.frame.origin.y == 0{
                   self.view.frame.origin.y -= keyboardSize.height/1.5
               }
           }
       }
       
       @objc func keyboardWillHide(notification: NSNotification) {
           if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
               if self.view.frame.origin.y != 0{
                   self.view.frame.origin.y = 0
               }
           }
       }

    //MARK :- AI CALL
    @available(iOS 11.0, *)
    func OTPAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = otURL
         let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                       "Authorization":strAccessToken]
                 print(strURL)
              let params: [String: Any] = [
                "otp" : "\(otpTf1.text!)\(otpTf2.text!)\(otpTf3.text!)\(otpTf4.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                "devicetype" : "I"
                ]
        print(params)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                 UserDefaults.standard.setValue(true, forKey: "loginStatus")
                                self.saveUserData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                let userDict = allServiceDict.value(forKey: "user") as! NSDictionary
                                let name = userDict.value(forKey: "name") as! String
                                let mobileNo = userDict.value(forKey: "mobile_no") as! String
                                let mail = userDict.value(forKey: "email") as! String
                                
                                
                                UserDefaults.standard.setValue(name, forKey: "userName")
                                UserDefaults.standard.setValue(mail, forKey: "userEmail")
                                
                                
                                UserDefaults.standard.setValue(allServiceDict.value(forKey: "profile") as! String, forKey: "profileEdit")
                                UserDefaults.standard.setValue(allServiceDict.value(forKey: "profilepic") as! String, forKey: "profileImg")
                                
                                if allServiceDict.value(forKey: "profile") as! String == "N" {
                                    self.nav(str: "My Info",nameStr:name,mobileStr:mobileNo)
                                }else  if allServiceDict.value(forKey: "profile") as! String == "C" {
                                    self.nav(str: "My Info",nameStr:name,mobileStr:mobileNo)
                                                               }
                                else {
                                    self.nav(str: "Home",nameStr:name,mobileStr:mobileNo)
                                }
                            }else{
          self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    
    @available(iOS 11.0, *)
    func ResendOTPAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = resendOtURL
         let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                       "Authorization":strAccessToken]
                 print(strURL)
              
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.saveUserData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }else{
                               self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    @available(iOS 11.0, *)
    private func saveUserData(dict:NSDictionary) {
         do {
               let data = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
              UserDefaults.standard.setValue(data, forKey: "userDict")
              
           } catch {
              
           }
       }
}
