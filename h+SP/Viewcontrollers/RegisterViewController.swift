//
//  RegisterViewController.swift
//  h+SP
//
//  Created by mirrorminds on 09/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import KVSpinnerView

class RegisterViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var regIdTf: UITextField!
    @IBOutlet weak var counsilTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var confirmTf: UITextField!
    @IBOutlet weak var mobileTf: UITextField!
    @IBOutlet weak var mailTf: UITextField!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblBGView: UIView!
    @IBOutlet weak var regDDBtn: UIButton!
    @IBOutlet weak var counDDBtn: UIButton!
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet weak var ContactheightCons: NSLayoutConstraint!
    @IBOutlet weak var ContactTF: UITextField!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var termsLbl: UILabel!
   
    let nurseDD = DropDown()
    let counsilStateDD = DropDown()
   
    var nurseIDArr = ["RN NUMBER","RM NUMBER","RHV NUMBER","RANM NUMBER"]
    
    var duArray = [String]()
    var strReply:String = ""
    var intReply:Int = 0
    var selectedIndex = ""
    var checkBoxvalue = false
    var counsilStateArr = [NSDictionary]()
    var counsilId = ""
    var nurseIDType = ""
    var docCategory = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetu()
        counsilStatesAPICall()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        termsLbl.isUserInteractionEnabled = true
      //  termsLbl.addGestureRecognizer(tap)
        if #available(iOS 13.0, *) {
            self.view.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
      //  let text = "By creating an account you agree our TERMS AND CONDITIONS & PRIVACY POLICY"
       termsLbl.text = "By creating an account you agree our TERMS AND CONDITIONS & PRIVACY POLICY"
           let text = (termsLbl.text)!
           let underlineAttriString = NSMutableAttributedString(string: text)
           let range1 = (text as NSString).range(of: "TERMS AND CONDITIONS")
           underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
           let range2 = (text as NSString).range(of: "PRIVACY POLICY")
           underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
           termsLbl.attributedText = underlineAttriString
           let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
           termsLbl.isUserInteractionEnabled = true
           termsLbl.addGestureRecognizer(tapAction)
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (termsLbl.text)!
        let termsRange = (text as NSString).range(of: "TERMS AND CONDITIONS")
        let privacyRange = (text as NSString).range(of: "PRIVACY POLICY")

        if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: termsRange) {
            print("Terms of service")
                    if let url = URL(string: "https://www.zero-delay.com/terms_condition"), UIApplication.shared.canOpenURL(url) {
                       if #available(iOS 10.0, *) {
                          UIApplication.shared.open(url, options: [:], completionHandler: nil)
                       } else {
                          UIApplication.shared.openURL(url)
                       }
                    }
        } else if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: privacyRange) {
            print("Privacy policy")
                    if let url = URL(string: "http://zero-delay.com/privacy-policy"), UIApplication.shared.canOpenURL(url) {
                       if #available(iOS 10.0, *) {
                          UIApplication.shared.open(url, options: [:], completionHandler: nil)
                       } else {
                          UIApplication.shared.openURL(url)
                       }
                    }
        } else {
            print("Tapped none")
        }
    }
    

    func viewSetu(){
        docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
        //Loader
        KVSpinnerView.settings.backgroundRectColor = UIColor.yellow
        KVSpinnerView.settings.statusTextColor = UIColor.darkGray
        KVSpinnerView.settings.tintColor = UIColor.darkGray
        Constant.addStatusBar(view: self.view)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
              
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        if docCategory == "DOC" {
              regIdTf.placeholder = "Council Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "NUR" {
              regIdTf.placeholder = "Select Category"
              counsilTf.placeholder = "TNNC Number"
        }else if docCategory == "PHY" {
              regIdTf.placeholder = "IAP / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "DEN" {
              regIdTf.placeholder = "DCI / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "DIE" {
              regIdTf.placeholder = "IDA / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "DYL" {
              regIdTf.placeholder = "Dialysis Center Registration Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "AMB" {
              regIdTf.placeholder = "Ambulance RegisterID"
              counsilTf.placeholder = "Vehicle Number"
        }else if docCategory == "MOR" {
              regIdTf.placeholder = "Mortuary RegisterID"
              counsilTf.placeholder = "Vehicle Number"
        }else if docCategory == "PHA" {
              regIdTf.placeholder = "Drug Lisence Number"
              counsilTf.placeholder = "Council State"
        }
        
        if docCategory == "NUR" {
            regDDBtn.isHidden = false
            counDDBtn.isHidden = true
        }else if docCategory == "MOR" || docCategory == "AMB" {
            regDDBtn.isHidden = true
            counDDBtn.isHidden = true
        }
        else{
            regDDBtn.isHidden = true
            counDDBtn.isHidden = false
        }
        
        if docCategory == "AMB" || docCategory == "MOR" || docCategory == "DYL" || docCategory == "PHA" {
            ContactheightCons.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 45 : 60)
        }else {
            ContactheightCons.constant = 0
        }
        

       
    }
    
    @objc func dismissKeyboard() {
           //Causes the view (or one of its embedded text fields) to resign the first responder status.
           view.endEditing(true)
       }
       
     func setupNurseDropDown() {
             nurseDD.anchorView = regIdTf.superview
             //specialityDropDown.bottomOffset = CGPoint(x: 0, y: HealthprobTf.bounds.height + 30)
            // nurseDD.topOffset = CGPoint(x: 0, y:-(regIdTf.bounds.height))
            nurseDD.topOffset = CGPoint(x: 0, y: regIdTf.bounds.height + 10)
             let joinedString = nurseIDArr
             nurseDD.dataSource = joinedString
             nurseDD.selectionAction = { [unowned self] (index, item) in
                 self.regIdTf.text = item
                self.nurseIDType = self.nurseIDArr[index]
             }
             nurseDD.show()
         }
    func setupCounsilDropDown() {
        var counsilStrArr = [String]()
        for i in 0..<counsilStateArr.count {
            counsilStrArr.append(counsilStateArr[i].value(forKey: "cou_state_name") as! String)
        }
        counsilStateDD.anchorView = counsilTf.superview
        counsilStateDD.topOffset = CGPoint(x: 0, y: counsilTf.bounds.height + 10)
        let joinedString = counsilStrArr
        counsilStateDD.dataSource = joinedString
        counsilStateDD.selectionAction = { [unowned self] (index, item) in
            self.counsilTf.text = item
            self.counsilTf.text = self.counsilStateArr[index].value(forKey: "cou_state_name") as? String
            self.counsilId = "\(self.counsilStateArr[index].value(forKey: "cou_id") as! Int)"
        }
        counsilStateDD.show()
    }
     // MARK:- tf delefates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTf {
            if nameTf.text != "" {
                nameTf.resignFirstResponder()
                regIdTf.becomeFirstResponder()
            }else{
                self.view.makeToast("Plaese enter your name", duration: 2.0, position: .center)
            }
        }
        if textField == regIdTf {
            if regIdTf.text != "" {
                regIdTf.resignFirstResponder()
              //  counsilTf.becomeFirstResponder()
            }else{
                self.view.makeToast("Plaese enter your registerId", duration: 2.0, position: .center)
            }
        }
        if textField == mobileTf {
            if mobileTf.text != "" {
                if mobileTf.text!.count < 10 {
                   self.view.makeToast("Your mobile number is less than 10 digit", duration: 2.0, position: .center)
                }else {
                    mobileTf.resignFirstResponder()
                    mailTf.becomeFirstResponder()
                    if UIDevice.current.userInterfaceIdiom == .phone {
                        scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
                    }else{
                      //  scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
                    }
                }
            }else{
                self.view.makeToast("Plaese enter your mobile number", duration: 2.0, position: .center)
            }
        }
        if textField == mailTf {
            if mailTf.text != "" {
                if isValidEmail(emailStr: mailTf.text!) == true {
                    mailTf.resignFirstResponder()
                    passwordTf.becomeFirstResponder()
                }else{
                   self.view.makeToast("Plaese enter a valid mailId", duration: 2.0, position: .center)
                }
            }else{
                self.view.makeToast("Plaese enter your mailId", duration: 2.0, position: .center)
            }
        }
        if textField == passwordTf {
            if passwordTf.text != "" {
                if passwordTf.text!.count >= 6 {
                    passwordTf.resignFirstResponder()
                    confirmTf.becomeFirstResponder()
                }else{
                    self.view.makeToast("Password must have minimun 6 characters.", duration: 2.0, position: .center)
                }
            }else{
                self.view.makeToast("Please enter your password", duration: 2.0, position: .center)
            }
        }
        if textField == confirmTf {
            if confirmTf.text != "" {
                if confirmTf.text != passwordTf.text {
                    self.view.makeToast("Password mismatch", duration: 2.0, position: .center)
                }else{
                    confirmTf.resignFirstResponder()
                }
            }else{
                self.view.makeToast("Plaese enter your password", duration: 2.0, position: .center)
            }
        }
        if docCategory == "NUR" {
            counsilTf.resignFirstResponder()
        }
        if textField == ContactTF {
                   ContactTF.resignFirstResponder()
               }
           //textField.resignFirstResponder()
           return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == regIdTf {
            if docCategory == "NUR" {
                setupNurseDropDown()
                return false
            }else {
                return true
            }
        }
        if textField == counsilTf {
            if docCategory == "NUR" || docCategory == "AMB" || docCategory == "MOR" {
                return true
            }else {
                self.view.endEditing(true)
               // hideView.isHidden = false
                setupCounsilDropDown()
                return false
            }
        }
        if textField == mobileTf {
            if UIDevice.current.userInterfaceIdiom == .phone {
                scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
            }else{
              //  scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            }
        }
        if textField == mailTf {
            if UIDevice.current.userInterfaceIdiom == .phone {
                scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            }else{
              //  scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            }
            
            if mobileTf.text!.count < 10 {
                self.view.makeToast("Your mobile number is less than 10 digit", duration: 2.0, position: .center)
            }
        }
        
        if textField == passwordTf {
            if UIDevice.current.userInterfaceIdiom == .phone {
              //  scrollView.setContentOffset(CGPoint(x: 0, y: -250), animated: true)
                scrollView.setContentOffset(CGPoint(x: 0, y: 220), animated: true)
            }else{
              //  scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            }
        }
        if textField == confirmTf {
            if UIDevice.current.userInterfaceIdiom == .phone {
              //  scrollView.setContentOffset(CGPoint(x: 0, y: -250), animated: true)
                scrollView.setContentOffset(CGPoint(x: 0, y: 220), animated: true)
            }else{
              //  scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            }
        }
          
        if docCategory == "DOC" {
              regIdTf.placeholder = "Council Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "NUR" {
              regIdTf.placeholder = "Select Category"
              counsilTf.placeholder = "TNNC Number"
        }else if docCategory == "PHY" {
              regIdTf.placeholder = "IAP / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "DEN" {
              regIdTf.placeholder = "DCI / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "DIE" {
              regIdTf.placeholder = "IDA / Register Number"
              counsilTf.placeholder = "Council State"
        }else if docCategory == "PHA" {
            regIdTf.placeholder = "Drug Lisence Number"
            counsilTf.placeholder = "Council State"
        }else if docCategory == "AMB" {
            regIdTf.placeholder = "Register Number"
            counsilTf.placeholder = "Vehicle Number"
        }else if docCategory == "MOR" {
            regIdTf.placeholder = "Register ID"
            counsilTf.placeholder = "Vehicle Number"
        }
        
        return true
    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileTf {
            let maxLength = 10
            let currentString: NSString = mobileTf.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                       if self.view.frame.origin.y == 0 {
                           self.view.frame.origin.y -= keyboardSize.height/1.2
                       }
                   }
        }else{
            guard let keyboardFrame: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
              scrollView.contentInset.bottom = keyboardFrame.height + 200
        }
                            
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    // MARK: - btn Actions
    @available(iOS 11.0, *)
    @IBAction func callAction(_ sender: Any) {
        var no = ""
        no = UserDefaults.standard.value(forKey: "contactNo") as? String != nil ? (UserDefaults.standard.value(forKey: "contactNo") as! String) : ("")
        if let url = URL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func nextAction(_ sender: Any) {
     // navToOt()
      registerValidation()
    }
    @IBAction func hideAction(_ sender: Any) {
        hideView.isHidden = true
    }
    @IBAction func backAction(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
            }
    @IBAction func checkBoxAction(_ sender: UIButton) {
        if checkBoxvalue == false {
            checkBoxvalue = true
            sender.setImage(UIImage(named: "checkBox"), for: .normal)
        }else if checkBoxvalue == true {
            checkBoxvalue = false
            sender.setImage(UIImage(named: "unCheck"), for: .normal)
        }
      }
     // MARK: - tblView delegates
          func numberOfSections(in tableView: UITableView) -> Int {
              return 1
          }
          func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              return counsilStateArr.count
          }
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 45
          }
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! whoCell
              //cell.imgBtn.isHidden = true
             cell.lbl.textAlignment = .center
              if selectedIndex == "\(indexPath.row)" {
                  cell.bgView.backgroundColor = UIColor(red: 249/255, green: 232/255, blue: 0/255, alpha: 1.0)
              }else{
                  cell.bgView.backgroundColor = UIColor.white
              }
           
            cell.lbl.text = counsilStateArr[indexPath.row].value(forKey: "cou_state_name") as? String
            cell.bgBtn.tag = indexPath.row
            cell.bgBtn.addTarget(self, action: #selector(didSelect(sender:)), for: .touchUpInside)
              return cell
          }
  
        @objc func didSelect(sender:UIButton){
            selectedIndex = "\(sender.tag)"
            counsilTf.text = counsilStateArr[sender.tag].value(forKey: "cou_state_name") as? String
            counsilId = "\(counsilStateArr[sender.tag].value(forKey: "cou_id") as! Int)"
            tblView.reloadData()
            hideView.isHidden = true
        }
    
   
    
 //MARK:- validations
    @available(iOS 11.0, *)
    func registerValidation() {
            if passwordTf.text!.count >= 6 {
            if nameTf.text != "" {
             if regIdTf.text != "" {
              if counsilTf.text != "" {
                if mobileTf.text != "" {
                    if mailTf.text != "" {
                        if passwordTf.text != "" {
                            if confirmTf.text != "" {
                                if isValidEmail(emailStr: mailTf.text!) == true {
                                    
                                    if passwordTf.text! == confirmTf.text {
                                        if mobileTf.text?.count == 10 {
                                            if checkBoxvalue == true {
                                                RegisterAPICall()
                                            }else{
                                                self.view.makeToast("Please check our terms and condition", duration: 2.0, position: .center)
                                            }
                                            
                                        }else{
                                            self.view.makeToast("Please check your mobile number", duration: 2.0, position: .center)
                                        }
                                        
                                    }else{
                                       self.view.makeToast("Password mismatch", duration: 2.0, position: .center)
                                    }

                                }else{
                                    self.mailTf.becomeFirstResponder()
                                     self.view.makeToast("Please enter a valid mail ID", duration: 2.0, position: .center)
                                }
                            }else{
                                self.confirmTf.becomeFirstResponder()
                                self.view.makeToast("Please enter your confirm password", duration: 2.0, position: .center)
                            }
                        }else{
                            self.passwordTf.becomeFirstResponder()
                             self.view.makeToast("Please enter your password", duration: 2.0, position: .center)
                        }
                    }else{
                        self.mailTf.becomeFirstResponder()
                         self.view.makeToast("Please enter your mail-Id", duration: 2.0, position: .center)
                    }
                }else{
                    self.mobileTf.becomeFirstResponder()
                   self.view.makeToast("Please enter your mobile number", duration: 2.0, position: .center)
                }
            }else{
                self.view.makeToast("Please select State", duration: 2.0, position: .center)
            }
                }else{
                self.regIdTf.becomeFirstResponder()
                    self.view.makeToast("Please enter registerId", duration: 2.0, position: .center)
                }
                }else{
                self.nameTf.becomeFirstResponder()
                    self.view.makeToast("Please enter name", duration: 2.0, position: .center)
                }
                }else{
                               self.passwordTf.becomeFirstResponder()
                                   self.view.makeToast("Password must be minimum 6 characters", duration: 2.0, position: .center)
                               }
        }

    func isValidEmail(emailStr:String) -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: emailStr)
    }
    func navToOt(str:String){
        let controller = storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
        controller.otStr = str
                controller.modalPresentationStyle = .fullScreen
                controller.modalTransitionStyle = .coverVertical
                present(controller, animated: true, completion: nil)
    }
    
    //MARK:- API CALL
    @available(iOS 11.0, *)
    func RegisterAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = registerURL
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
                 print(strURL)
              var params: [String: Any]
        
        if docCategory == "AMB" || docCategory == "MOR" || docCategory == "PHA"{
            params = [
                          "name" : "\(nameTf.text!)",
                          "regno" : "\(regIdTf.text!)",
                          "vehicleno" : "\(counsilTf.text!)",
                          "mobileno" : "\(mobileTf.text!)",
                          "email" : "\(mailTf.text!)",
                          "usertype" : docCategory,
                          "password": "\(passwordTf.text!)",
                          "contactpersonname":"\(ContactTF.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                "devicetype" : "I"
                     ]
        } else if docCategory == "NUR" {
            params = [
                 "name" : "\(nameTf.text!)",
                 "regno" : "\(counsilTf.text!)",
                 "nurcat" : "\(nurseIDType)",
                 "mobileno" : "\(mobileTf.text!)",
                 "email" : "\(mailTf.text!)",
                 "usertype" : docCategory,
                 "password": "\(passwordTf.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                 "devicetype" : "I"
            ]
        }else if docCategory == "DYL" {
            params = [
                 "name" : "\(nameTf.text!)",
                 "regno" : "\(regIdTf.text!)",
                 "couid" : "\(counsilId)",
                 "mobileno" : "\(mobileTf.text!)",
                 "email" : "\(mailTf.text!)",
                 "usertype" : docCategory,
                 "password": "\(passwordTf.text!)",
                "contactpersonname":"\(ContactTF.text!)",
                "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                 "devicetype" : "I"
            ]
        }
        else{
            params = [
                 "name" : "\(nameTf.text!)",
                 "regno" : "\(regIdTf.text!)",
                 "couid" : "\(counsilId)",
                 "mobileno" : "\(mobileTf.text!)",
                 "email" : "\(mailTf.text!)",
                 "usertype" : docCategory,
                 "password": "\(passwordTf.text!)",
                 "fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):(""),
                 "devicetype" : "I"
            ]
        }
        
      
        print(params)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.saveUserData(dict: allServiceDict.value(forKey: "user") as! NSDictionary)
                                let dict = allServiceDict.value(forKey: "user") as! NSDictionary
                                self.navToOt(str:(dict.value(forKey: "otp") as! NSNumber).stringValue)
                            }else{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                            
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                          //  self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    func counsilStatesAPICall() {
      if Reachability.isConnectedToNetwork() {
          Constant.showLoader(view: self.view)
        let strURL = counsilStateURL
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
                 print(strURL)

        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                 self.counsilStateArr = allServiceDict.value(forKey: "state") as! [NSDictionary]
                            }else{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                                                        
                            Constant.hideLoader(view: self.view)
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Server error", duration: 3.0, position: .center)
                            Constant.hideLoader(view: self.view)
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    
    @available(iOS 11.0, *)
    private func saveUserData(dict:NSDictionary) {
        do {
              let data = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
             UserDefaults.standard.setValue(data, forKey: "userDict")
              UserDefaults.standard.setValue(dict.value(forKey: "token") as! String, forKey: "token")
          } catch {
             
          }
      }

    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UILabel {
    func underlineMyText(range1:String, range2:String) {
        if let textString = self.text {

            let str = NSString(string: textString)
            let firstRange = str.range(of: range1)
            let secRange = str.range(of: range2)
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: firstRange)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: secRange)
            attributedText = attributedString
        }
    }
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.1 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
            locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
