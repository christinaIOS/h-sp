//
//  SettlementViewController.swift
//  h+SP
//
//  Created by mirrorminds on 20/12/19.
//  Copyright © 2019 mirrorminds. All rights reserved.
//

import UIKit
import DatePickerDialog
class SettlementViewController: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var MenuBtn: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblOverall: UILabel!
    @IBOutlet weak var lblToday: UILabel!
    
    
      var fromView = ""
    var docCategory = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddShadow(shadView: view1)
        AddShadow(shadView: view2)
        Constant.addStatusBar(view: self.view)
        bgView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 40)
        if fromView == "dash" {
            MenuBtn.isHidden = true
            backBtn.isHidden = false
           
        }else {
            MenuBtn.isHidden = false
           backBtn.isHidden = true
             gestur()
        }
        
        docCategory =  UserDefaults.standard.value(forKey: "docCat") as! String
              
      if docCategory == "DOC" || docCategory == "DEN" || docCategory == "DYL" || docCategory == "PHY" {
           lblOverall.text = "Today Appointments"
           lblToday.text = "Overall Appointments"
       }else if docCategory == "PHA" {
           lblOverall.text = "Today Orders"
           lblToday.text = "Overall Orders"
       }
        
    }
    
    
    func AddShadow(shadView:UIView){
           shadView.layer.shadowColor = UIColor.lightGray.cgColor
                                                 shadView.layer.shadowOpacity = 2
                                                 shadView.layer.shadowOffset = CGSize.zero
                                                 shadView.layer.shadowRadius = 3
                          shadView.layer.cornerRadius = 10
       }
    func gestur(){
                     let left = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     left.direction = .left
                     self.view.addGestureRecognizer(left)
             
                     let right = UISwipeGestureRecognizer(target : self, action : #selector(Swipe))
                     right.direction = .right
                     self.view.addGestureRecognizer(right)
                 }
             
             
                 @objc func Swipe(){
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
                 }
    // MARK: - btnActions
    
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    
    @IBAction func menuAction(_ sender: UIButton) {
        if sender.tag == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
        }else if sender.tag == 1 {
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func DateShowAction(_ sender: UIButton) {
//        let currentDate = Date()
//        var dateComponents = DateComponents()
//        dateComponents.month = 12
//         let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
               
        DatePickerDialog().show("Date", doneButtonTitle: "OK", cancelButtonTitle: "Cancel",datePickerMode: .date) { (date) in
         if let dt = date {
             let formatter = DateFormatter()
             formatter.dateFormat = "dd-MM-yyyy"
           if sender.tag == 0 {
                sender.setTitle("     \(formatter.string(from: dt))", for: .normal)
            }else if sender.tag == 1 {
               sender.setTitle("     \(formatter.string(from: dt))", for: .normal)
            }
            
            }
          }
           
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
