//
//  SubRegisterViewController.swift
//  h+SP
//
//  Created by mirrorminds on 12/06/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView

class SubRegisterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfassword: UITextField!
    @IBOutlet weak var tfChangeassword: UITextField!
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var hideinnerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var errlbl: UILabel!
    
    var switchStatus = "Y"
    var userArr = [NSDictionary]()
    var isEdit = false
    var selectedId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.addStatusBar(view: self.view)
          hideinnerView.roundCorners(corners: [.layerMinXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 25 : 35))
        // Do any additional setup after loading the view.
          mySwitch.addTarget(self, action: #selector(self.switchIsChanged(mySwitch:)), for: UIControl.Event.valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:  UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
               view.addGestureRecognizer(tap)
        SubUserListingAPICall()
    }
   
    //MARK:- textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if textField == tfMobile {
               let maxLength = 10
               let currentString: NSString = tfMobile.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           
           return true
       }
    
    @objc func keyboardWillShow(notification: NSNotification) {
              if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                  if self.view.frame.origin.y == 0{
                      self.view.frame.origin.y -= keyboardSize.height/1.5
                  }
              }
          }
          
          @objc func keyboardWillHide(notification: NSNotification) {
              if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                  if self.view.frame.origin.y != 0{
                      self.view.frame.origin.y = 0
                  }
              }
          }
    @objc func dismissKeyboard() {
              //Causes the view (or one of its embedded text fields) to resign the first responder status.
              view.endEditing(true)
          }
    //MARK:- btn Actions
    
    @IBAction func notificationAction(_ sender: Any) {
             let controller = storyboard?.instantiateViewController(withIdentifier: "NotoficationViewController") as! NotoficationViewController
               controller.fromview = "O"
             controller.modalPresentationStyle = .fullScreen
             controller.modalTransitionStyle = .coverVertical
             present(controller, animated: true, completion: nil)
       }
    
    @IBAction func addAction (_ sender:UIButton) {
        tfName.isUserInteractionEnabled = true
        tfEmail.isUserInteractionEnabled = true
        tfMobile.isUserInteractionEnabled = true
        tfassword.isUserInteractionEnabled = true
        tfChangeassword.isUserInteractionEnabled = true
        mySwitch.isUserInteractionEnabled = true
        
        tfName.text = ""
        tfEmail.text = ""
        tfMobile.text = ""
        tfassword.text = ""
        tfChangeassword.text = ""
        mySwitch.isOn = true
        isEdit = false
        hideView.isHidden = false
        submitBtn.isEnabled = true
        submitBtn.alpha = 1
    }
    
    @IBAction func submitAction (_ sender:UIButton) {
        view.endEditing(true)
        validation()
    }
    @IBAction func hideAction (_ sender:UIButton) {
        hideView.isHidden = true
    }
    
    @objc func switchIsChanged(mySwitch: UISwitch) {
        if mySwitch.isOn {
           switchStatus = "Y"
         // mySwitch.onTintColor = UIColor.white
        } else {
           switchStatus = "N"
          mySwitch.onTintColor = UIColor.green
        }
       
    }
    @IBAction func menuAction (_ sender:UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
     // MARK: - tblView Delegates
       func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return userArr.count
           
       }
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 80
           
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            var  cell: subregisterTableViewCell? = (tableView.dequeueReusableCell(withIdentifier: "subregisterTableViewCell") as? subregisterTableViewCell)
           let nib: [Any] = Bundle.main.loadNibNamed((UIDevice.current.userInterfaceIdiom == .phone ? "subregisterTableViewCell" : "AbsentTableViewCell_ipad" ), owner: self, options: nil)!
           
                      cell = (nib[0]  as? subregisterTableViewCell)!
                      cell?.viewBg.layer.shadowColor = UIColor.lightGray.cgColor
                      cell?.viewBg.layer.shadowOpacity = 1
                      cell?.viewBg.layer.shadowOffset = CGSize.zero
                      cell?.viewBg.layer.shadowRadius = 2
                      cell?.viewBg.layer.cornerRadius = 5
           
         cell?.mySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        cell?.viewEdit.roundCorners(corners: [.layerMaxXMaxYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 5 : 10))
        cell?.viewEdit.roundCorners(corners: [.layerMaxXMinYCorner], radius: (UIDevice.current.userInterfaceIdiom == .phone ? 5 : 10))
         
        cell?.namelbl.text = userArr[indexPath.row].value(forKey: "name") as? String
        cell?.mailLbl.text = userArr[indexPath.row].value(forKey: "email") as? String
        cell?.mobileLbl.text = userArr[indexPath.row].value(forKey: "mobile_no") as? String
        userArr[indexPath.row].value(forKey: "status") as? String == "Y" ? (mySwitch.isOn = true):(mySwitch.isOn = false)
        cell?.namelbl.text = userArr[indexPath.row].value(forKey: "name") as? String
        
           cell?.btnedit.tag = indexPath.row
           cell?.btnedit.addTarget(self, action: #selector(editAction(sender:)), for: .touchUpInside)
        
        cell?.btnSelect.tag = indexPath.row
                  cell?.btnSelect.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
        
              
           return cell!
           
       }
    
    @objc func didSelect(sender:UIButton){
        tfName.isUserInteractionEnabled = false
        tfEmail.isUserInteractionEnabled = false
        tfMobile.isUserInteractionEnabled = false
        tfassword.isUserInteractionEnabled = false
        tfChangeassword.isUserInteractionEnabled = false
        mySwitch.isUserInteractionEnabled = false
        isEdit = false
        
        tfName.text = userArr[sender.tag].value(forKey: "name") as? String
               tfEmail.text = userArr[sender.tag].value(forKey: "email") as? String
               tfMobile.text = userArr[sender.tag].value(forKey: "mobile_no") as? String
               tfassword.text = ""
               tfChangeassword.text = ""
                userArr[sender.tag].value(forKey: "status") as? String == "Y" ? (mySwitch.isOn = true):(mySwitch.isOn = false)
               hideView.isHidden = false
        submitBtn.isEnabled = false
        submitBtn.alpha = 0.5
    }
   
    
    
    @objc func editAction(sender:UIButton){
        tfName.isUserInteractionEnabled = true
        tfEmail.isUserInteractionEnabled = true
        tfMobile.isUserInteractionEnabled = true
        tfassword.isUserInteractionEnabled = true
        tfChangeassword.isUserInteractionEnabled = true
        mySwitch.isUserInteractionEnabled = true
        isEdit = true
        tfName.text = userArr[sender.tag].value(forKey: "name") as? String
        tfEmail.text = userArr[sender.tag].value(forKey: "email") as? String
        tfMobile.text = userArr[sender.tag].value(forKey: "mobile_no") as? String
        tfassword.text = ""
        tfChangeassword.text = ""
         userArr[sender.tag].value(forKey: "status") as? String == "Y" ? (mySwitch.isOn = true):(mySwitch.isOn = false)
        selectedId = (userArr[sender.tag].value(forKey: "id") as! NSNumber).stringValue
        hideView.isHidden = false
        submitBtn.isEnabled = true
        submitBtn.alpha = 1
     }
       @available(iOS 11.0, *)
          func logout(){
              let controller = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                   controller.modalPresentationStyle = .fullScreen
                   controller.modalTransitionStyle = .coverVertical
              present(controller, animated: true, completion: nil)
          }
    
   func isValidEmail(emailStr:String) -> Bool {
             let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
             
             let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
             return emailPred.evaluate(with: emailStr)
     }
    
    func validation(){
        if tfName.text != "" {
            if tfEmail.text != "" {
                if tfMobile.text != "" {
                   if isEdit == false {
                        if tfassword.text != "" {
                            if tfChangeassword.text != "" {
                                if tfassword.text == tfChangeassword.text {
                                    if isValidEmail(emailStr: tfEmail.text!) == true {
                                       createSubUserAPICall()
                                    }else{
                                        self.view.makeToast("Please enter valid mail id", duration: 2.0, position: .center)
                                    }
                                }else{
                                   self.view.makeToast("Password mismatch", duration: 2.0, position: .center)
                                }
                            }else{
                                self.view.makeToast("Please enter your change password", duration: 2.0, position: .center)
                            }
                        }else{
                            self.view.makeToast("Please enter your password", duration: 2.0, position: .center)
                        }
                    }else {
                                    if isValidEmail(emailStr: tfEmail.text!) == true {
                                        editSubUserAPICall(id:selectedId)
                                    }else{
                                        self.view.makeToast("Please enter valid mail id", duration: 2.0, position: .center)
                        }
                    }
                    
                }else{
                    self.view.makeToast("Please enter your mobile no", duration: 2.0, position: .center)
                }
            }else{
                self.view.makeToast("Please enter your mail ", duration: 2.0, position: .center)
            }
        }else{
            self.view.makeToast("Please enter your name", duration: 2.0, position: .center)
        }
    }
    //MARK:- AI CALL
    func createSubUserAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = subUser
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        print(strURL)
        
        let params = ["name":"\(tfName.text!)","email":"\(tfEmail.text!)","password": "\(tfassword.text!)","mobileno" : "\(tfMobile.text!)","status":"\(switchStatus)"]
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.hideView.isHidden = true
                                self.SubUserListingAPICall()
                           }else{
                               
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                   self.hideView.isHidden = true
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                           }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    func editSubUserAPICall(id:String) {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = editSubUser + selectedId
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken,
        "userid":id]
        print(strURL)
        
        let params = ["name":"\(tfName.text!)","email":"\(tfEmail.text!)","password": "\(tfassword.text!)","mobileno" : "\(tfMobile.text!)","status":"\(switchStatus)"]
        print(headers)
        print(params)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.hideView.isHidden = true
                                self.SubUserListingAPICall()
                           }else{
                               
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                   self.hideView.isHidden = true
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                           }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
    func SubUserListingAPICall() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = subUserListing
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
        print(strURL)
        
       
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                                self.userArr = allServiceDict.value(forKey: "user") as! [NSDictionary]
                                
                                if self.userArr.count == 0 {
                                    self.errlbl.isHidden = false
                                }else{
                                     self.errlbl.isHidden = true
                                }
                                
                                self.tblView.reloadData()
                           }else{
                               
                                if allServiceDict.value(forKey: "Message") as! String == "USER NOT EXISTS!!" || allServiceDict.value(forKey: "Message") as! String == "ACCOUNT LOGGED IN ANOTHER DEVICE" {
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                        UserDefaults.standard.setValue(false, forKey: "loginStatus")
                                        self.logout()
                                    })
                                }else {
                                    self.errlbl.isHidden = false
                                   self.hideView.isHidden = true
                                    self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                }
                           }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.errlbl.isHidden = false
                            self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
        
}
