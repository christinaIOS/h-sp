//
//  Terms&conditionsWebViewController.swift
//  h+SP
//
//  Created by mirrorminds on 01/03/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit
import WebKit
import KVSpinnerView

class Terms_conditionsWebViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {

    @IBOutlet weak var webview: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://www.websitepolicies.com/blog/sample-terms-conditions-template")
        webview.load(URLRequest(url: url!))
    }
    
     func webView(_ webView: WKWebView, didFinish navigation:
        WKNavigation!) {
        KVSpinnerView.dismiss()
    }

       func webView(_ webView: WKWebView, didStartProvisionalNavigation
       navigation: WKNavigation!) {
        KVSpinnerView.show(saying: "")
    }

       func webView(_ webView: WKWebView, didFail navigation:
       WKNavigation!, withError error: Error) {
        KVSpinnerView.dismiss()
    }
    
    @IBAction func backAction(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }

}
