//
//  ZoomViewController.swift
//  h+SP
//
//  Created by mirrorminds on 04/07/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit
import KVSpinnerView
import Alamofire


class ZoomViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var imgPhoto: UIImageView!
    var dict : NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       scrolView.delegate = self
       scrolView.minimumZoomScale = 1.0
       scrolView.maximumZoomScale = 10.0
        zoomAPICall()
        imgPhoto.af_setImage(withURL: URL(string: dict.value(forKey: "image") as! String)!)
        // Do any additional setup after loading the view.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
      return imgPhoto
    }
   
    @IBAction func bckAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func zoomAPICall() {
      if Reachability.isConnectedToNetwork() {
        //KVSpinnerView.show(saying: "")
        let strURL = adscount
      let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
        let headers = ["Content-Type": "application/x-www-form-urlencoded",
                       "Authorization":strAccessToken,"fcmkey" : UserDefaults.standard.value(forKey: "FCMTOKEN") as? String != nil ? (UserDefaults.standard.value(forKey: "FCMTOKEN") as! String):("")]
                 print(strURL)
              var params: [String: Any]
        
        params = [
            "id" : dict.value(forKey: "id") as! Int,
                
             ]
       
      
        print(params)
        print(strURL)
        print(headers)
                  Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                              // self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }else{
                               // self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                            
//                           DispatchQueue.main.async {
//                               KVSpinnerView.dismiss()
//                           }
                        case .failure(let error):
                            print(error)
                          //  self.view.makeToast("Sever error.please try again later", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
}
