//
//  commentsViewController.swift
//  h+SP
//
//  Created by mirrorminds on 18/07/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit

class commentsViewController: UIViewController {

    @IBOutlet weak var lbl : UILabel!
    var commentTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
     // lbl.text = commentTxt
        lbl.attributedText = commentTxt.htmlToAttributedString!
        // Do any additional setup after loading the view.
    }
    
    @IBAction func bckAction(_ sender : UIButton) {
           self.dismiss(animated: true, completion: nil)
    }
       

}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
