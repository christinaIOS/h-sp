//
//  mailUsViewController.swift
//  h+SP
//
//  Created by mirrorminds on 06/06/20.
//  Copyright © 2020 mirrorminds. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView
import DropDown

class mailUsViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var nameTf : UITextField!
    @IBOutlet weak var mailTf : UITextField!
    @IBOutlet weak var subjectTf : UITextField!
    @IBOutlet weak var descriptionTv : UITextView!
    
    var subjectDD  = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

              tap.cancelsTouchesInView = false
         view.addGestureRecognizer(tap)
         
        if UserDefaults.standard.value(forKey: "userName") as? String != nil {
            nameTf.text = UserDefaults.standard.value(forKey: "userName") as? String
        }
        
        if UserDefaults.standard.value(forKey: "userEmail") as? String != nil {
            mailTf.text = UserDefaults.standard.value(forKey: "userEmail") as? String
        }
        
    }
    @objc func dismissKeyboard() {
        self.view.frame.origin.y = 0
        view.endEditing(true)
    }
    
    //MARK:- setting droDowns
    
    func subjectDropDown() {
        let bankNameStrArr = ["App","Appointment Related","Doctor","Other","Payment"]
               subjectDD.anchorView = subjectTf
               subjectDD.topOffset = CGPoint(x: 0, y: subjectTf.bounds.height + 10)
               let joinedString = bankNameStrArr
               subjectDD.dataSource = joinedString
               subjectDD.selectionAction = { [unowned self] (index, item) in
                   self.subjectTf.text = item
                 //  self.bankNameID = "\(se.bankNameArr[index].value(forKey: "id") as! NSNumber)"
                  // self.SpecialityHeightCons2.constant = (UIDevice.current.userInterfaceIdiom == .phone ? 40 : 55)
                  // self.deg13btn.isHidden = false
               }
               subjectDD.show()
    }
    
    //MARK:- textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == subjectTf {
            textField.resignFirstResponder()
             subjectDropDown()
            return false
        }
        return true
    }
    @objc func keyboardWillShow(notification: NSNotification) {
               if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                   if self.view.frame.origin.y == 0{
                       self.view.frame.origin.y -= keyboardSize.height/1.5
                   }
               }
           }
           
           @objc func keyboardWillHide(notification: NSNotification) {
               if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
                   if self.view.frame.origin.y != 0{
                       self.view.frame.origin.y = 0
                   }
               }
           }
    //MARK:- textview delegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
         self.view.frame.origin.y = -150
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
           if(text == "\n") {
            self.view.frame.origin.y = 0
               textView.resignFirstResponder()
               return false
           }
           return true
       }
    @IBAction func submitAction(_ sender: Any) {
        validation()
    }
    @IBAction func menuAction(_ sender: UIButton) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: nil)
    }
    func isValidEmail(emailStr:String) -> Bool {
               let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
               
               let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
               return emailPred.evaluate(with: emailStr)
       }
    func validation() {
        if nameTf.text != "" {
            if mailTf.text != "" {
                if subjectTf.text != "" {
                    if descriptionTv.text != "" {
                        if isValidEmail(emailStr: mailTf.text!) != false {
                             mailUpdate()
                        }
                    else{
                           self.view.makeToast("Please enter valid mailId", duration: 2.0, position: .center)
                       }
                    }else{
                        self.view.makeToast("Please enter your description", duration: 2.0, position: .center)
                    }
                }else{
                  self.view.makeToast("Please enter your subject for enquiry", duration: 2.0, position: .center)
                }
            }else{
                self.view.makeToast("Please enter your mail Id", duration: 2.0, position: .center)
            }
        }else{
            self.view.makeToast("Please enter your name", duration: 2.0, position: .center)
        }
    }
    
    func mailUpdate() {
      if Reachability.isConnectedToNetwork() {
        KVSpinnerView.show(saying: "")
        let strURL = mailUs
        let strAccessToken = "Bearer " + (UserDefaults.standard.value(forKey: "token") as! String)
       let headers = ["Content-Type": "application/x-www-form-urlencoded",
       "Authorization":strAccessToken]
         
        let params = ["name":nameTf.text!,"email":mailTf.text!,"subject": subjectTf.text!,"description":descriptionTv.text!] as [String : Any]
        
        print(strURL)
        print(headers)
        print(params)
        
        Alamofire.request(strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
                      {
                        response in switch response.result {
                        case .success(let JSON):
                            let allServiceDict = (JSON as! NSDictionary)
                            print(allServiceDict)
                            if allServiceDict.value(forKey: "status") as! String == "true" {
                              self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                                self.nameTf.text = ""
                                self.mailTf.text = ""
                                self.subjectTf.text = ""
                                self.descriptionTv.text = ""
                            }else{
                                self.view.makeToast(allServiceDict.value(forKey: "Message") as! String, duration: 2.0, position: .center)
                            }
                           DispatchQueue.main.async {
                               KVSpinnerView.dismiss()
                           }
                        case .failure(let error):
                            print(error)
                            self.view.makeToast("Server error.please try again later.", duration: 2.0, position: .center)
                            DispatchQueue.main.async {
                                KVSpinnerView.dismiss()
                            }
                            
                        }
                  }
              }else {
               self.view.makeToast("NetWork error", duration: 3.0, position: .center)
             }
    }
}
