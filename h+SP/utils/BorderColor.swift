//
//  BorderColor.swift
//  NeuIndustries
//
//  Created by Balakrishnan on 07/07/16.
//  Copyright © 2016 Balakrishnan. All rights reserved.
//
import UIKit

extension UIButton {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
//    override public var highlighted: Bool {
//        didSet {
//            if highlighted {
//                backgroundColor = UIColor(red: 71.0/255.0, green: 168.0/255.0, blue: 242.0/255.0, alpha: 1.0)
//            } else {
//                backgroundColor = UIColor.clearColor()
//            }
//        }
//    }
}
