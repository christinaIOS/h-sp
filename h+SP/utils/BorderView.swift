//
//  BorderView.swift
//  NeuIndustries
//
//  Created by Balakrishnan on 21/07/16.
//  Copyright © 2016 Balakrishnan. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerLineRadious: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderLineWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderLineColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }

}
